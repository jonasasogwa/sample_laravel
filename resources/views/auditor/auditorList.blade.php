@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-default btn-xs" href="{{ route('auditors-excel-file',['type'=>'xls']) }}">Download Excel xls</a>
                        <a class="btn btn-default btn-xs" href="{{ route('auditors-excel-file',['type'=>'xlsx']) }}">Download Excel xlsx</a>
                        <a class="btn btn-default btn-xs" href="{{ route('auditors-excel-file',['type'=>'csv']) }}">Download CSV</a>
                    </div>
                </div>

                    <table class="table table-hover">
                        <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Verification Code</th>
                        <th>phone</th>
                        <th>Settings</th>
                        </thead>
                        <tbody>
                        @foreach($auditors as $auditor)
                            <tr>
                                <td>
                                    {{ $auditor->last_name }}  {{ $auditor->first_name }}
                                </td>
                                <td>
                                    {{ $auditor->email }}
                                </td>
                                <td>
                                    {{ $auditor->verification_code }}
                                </td>
                                <td>
                                    {{ $auditor->phone }}
                                </td>
                                <td>
                                    <a href="{{url('/auditors/edit/')}}/{{$auditor->id}}" class="btn btn-xs btn-warning "> edit </a>
                                    <a type="button" data-info='{{$auditor->id}}}' class="btn btn-xs btn-danger removeAuditor">trash</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            {{ $auditors->links() }}
                        </ul>
                    </div>

                </div>

            </div>

        </div>

    <script>
        $(document).on('click', '.removeAuditor', function(){
            var id = $(this).attr('data-info');
            var r =confirm('Are you sure you want to delete this  Auditor');
            //alert(id);
            if(r){
                $.get('auditors/delete', {'id':id})
                .done(function(e){
                    if(e == '200'){
                        alert('It has been deleted');
                        location.reload();
                    }
                    if(e == '403'){
                        alert('You cannot delete this Auditor because it being used by one or more process');
                    }
                })
                .fail(function(){
                    alert('error');
                });
            }
        });
    </script>
@endsection
