@extends('layouts.app1')

@section('content')

    {{--<div class="col-sm-12">--}}
        {{--<div class="box">--}}
            {{--<!-- /.box-header -->--}}
            {{--<div class="box-body">--}}
                {{--{!! Form::open(array('url' => '/auditors/upload','method'=>'POST','files'=>'true')) !!}--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-4 col-sm-4 col-md-4">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-9">--}}
                                {{--{!! Form::file('sample_file', array('class' => 'form-control' )) !!}--}}
                                {{--{!! $errors->first('sample_file', '<p class="alert alert-danger">:message</p>') !!}--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-3 col-sm-3 col-md-3 text-center">--}}
                        {{--{!! Form::submit('Upload',['class'=>'btn btn-primary']) !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--{!! Form::close() !!}--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-6">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3>Upload Auditors</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="box-group" id="accordion">
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        <div class="panel box box-primary">

                            <div class="box-header with-border">
                                <h4 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" >
                                        Upload Auditors from Excel File
                                        <a  href="{{asset('custom/downloadable/auditors.csv')}}">
                                            <code class="btn btn-xs btn-info">Download Template</code>
                                        </a>

                                    </a>
                                </h4>
                            </div>

                            <div id="" class="panel-collapse  in">
                                <div class="box-body">

                                    {!! Form::open(array('url' => '/auditors/upload','method'=>'POST','files'=>'true')) !!}
                                    <br>
                                    <div class="form-group has-feedback">
                                        {!! Form::file('sample_file', array('class' => '' )) !!}
                                        {!! $errors->first('sample_file', '<p class="alert alert-danger">:message</p>') !!}
                                        <span class="fa fa-upload form-control-feedback"></span>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-4 pull-right">
                                            {!! Form::submit('Upload',['class'=>'btn btn-primary btn-block btn-flat']) !!}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>



@endsection
