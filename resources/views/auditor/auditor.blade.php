<?php
$data = '';
if(isset($auditorObj)){
    $data = $auditorObj;
}

?>

@extends('layouts.app1')

@section('content')

    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                <div class="panel panel-default">

                    <div class="panel-body">

                        @if($data)
                            {!! Form::open(['url' => '/auditors/edit', 'id' => "AuditorForm", 'class'=>'form-horizontal']) !!}
                            {!! Form::input('hidden','auditor_id',encrypt($data->id),['class' => '', 'id' => "auditor_id"]) !!}
                        @else
                            {!! Form::open(['url' => '/auditors/create', 'id' => "AuditorForm", 'class'=>'form-horizontal']) !!}
                        @endif

                        <br>
                            @if($data)
                                <legend>Edit Auditor </legend>
                            @else
                                <legend>Create New Auditor </legend>
                            @endif

                        <!--<div class="messages"></div>-->
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="branch_name" class="col-sm-2 control-label"> Last Name</label>
                                <div class="col-sm-10">
                                    @if($data)
                                        {!! Form::input('text','last_name',$data->user_profile->last_name,["placeholder" => "Enter Last Name  ", "class" => "form-control required", "id" => "last_name"
                                        ]) !!}
                                        @if ($errors->has('last_name')) <p class="help-block ">{{ $errors->first('last_name') }}</p> @endif
                                    @else
                                        {!! Form::input('text','last_name',null,["placeholder" => "Enter Last Name  ", "class" => "form-control required", "id" => "last_name"]) !!}
                                        @if ($errors->has('last_name')) <p class="help-block has-error">{{ $errors->first('last_name') }}</p> @endif
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="branch_name" class="col-sm-2 control-label"> First Name</label>
                                <div class="col-sm-10">
                                    @if($data)
                                        {!! Form::input('text','first_name',$data->user_profile->first_name,["placeholder" => "Enter First Name  ", "class" => "form-control required", "id" => "first_name",
                                        ]) !!}
                                        @if ($errors->has('first_name')) <p class="help-block ">{{ $errors->first('first_name') }}</p> @endif
                                    @else
                                        {!! Form::input('text','first_name',null,["placeholder" => "Enter First Name  ", "class" => "form-control required", "id" => "first_name",
                                        ]) !!}
                                        @if ($errors->has('first_name')) <p class="help-block has-error">{{ $errors->first('first_name') }}</p> @endif
                                    @endif
                                </div>
                            </div>


                            @if( $isAbleToChangeRole)
                                <div class="form-group" >
                                    <label for="country_id" class="col-sm-2 control-label">Role</label>
                                    <div class="col-sm-10">
                                        @if($data)
                                            {!! Form::select('role_id',$roles,$role_id,['class' => 'role_id form-control m-b required', "id" => "role_id"])
                                            !!}
                                        @else
                                            {!! Form::select('role_id',$roles,null,['class' => 'role_id form-control m-b required', "id" => "role_id",])
                                            !!}
                                        @endif
                                    </div>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="branch_name" class="col-sm-2 control-label"> Phone Number</label>
                                <div class="col-sm-10">
                                    @if($data)
                                        {!! Form::input('number','phone',$data->user_profile->phone,["placeholder" => "Enter Phone number  ", "class" => "form-control required", "id" => "phone",
                                        ]) !!}
                                        @if ($errors->has('phone')) <p class="help-block ">{{ $errors->first('phone') }}</p> @endif
                                    @else
                                        {!! Form::input('text','phone',null,["placeholder" => "Enter phone number  ", "class" => "form-control validate[required]", "id" => "phone",
                                        ]) !!}
                                        @if ($errors->has('phone')) <p class="help-block has-error">{{ $errors->first('phone') }}</p> @endif
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="branch_name" class="col-sm-2 control-label"> Email</label>
                                <div class="col-sm-10">
                                    @if($data)
                                        {!! Form::input('email','email',$data['email'],["placeholder" => "Enter Email  ", "class" => "form-control required", "id" => "email",
                                        ]) !!}
                                        @if ($errors->has('email')) <p class="help-block ">{{ $errors->first('email') }}</p> @endif
                                    @else
                                        {!! Form::input('text','email',null,["placeholder" => "Enter Email  ", "class" => "form-control required", "id" => "email",
                                        ]) !!}
                                        @if ($errors->has('email')) <p class="help-block has-error">{{ $errors->first('email') }}</p> @endif
                                    @endif
                                </div>
                            </div>

                            @if(!$data)
                                <div class="form-group">
                                <label for="password" class="col-sm-3 control-label">Password: </label>
                                <label class="col-sm-1 control-label">: </label>
                                <div class="col-sm-8">
                                <input type="password" class="form-control" id="password" placeholder="Employee Password"
                                name="password" autocomplete="off">
                                </div>
                                </div> <!-- /form-group-->

                                <div class="form-group">
                                <label for="confirm_password" class="col-sm-3 control-label">Confirm Password: </label>
                                <label class="col-sm-1 control-label">: </label>
                                <div class="col-sm-8">
                                <input type="password" class="form-control" id="confirm_password"
                                placeholder="Confirm Password" name="confirm_password" autocomplete="off">
                                </div>
                                </div> <!-- /form-group-->
                            @endif
                            <div class="messages"></div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    @if($data)
                                        <button type="submit" class="btn btn-success" data-loading-text="Loading..." id="companyBtn"> <i class="glyphicon glyphicon-ok-sign"></i> Update Record </button>
                                    @else
                                        <button type="submit" class="btn btn-success" data-loading-text="Loading..." id="companyBtn"> <i class="glyphicon glyphicon-ok-sign"></i>  Save Record </button>
                                    @endif
                                </div>
                            </div>

                    </div>
                </div>
            </div>

        </div>

    </div>


@endsection
