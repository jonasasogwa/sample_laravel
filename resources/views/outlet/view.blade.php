@extends('layouts.app1')

@section('content')
	@include('flash::message')
		<h2>Details of {{ $outlets->dealer_name }} Retail Outlet: Outlet Code-{{ $outlets->code }}</h2>
		<div  class="container">
			<div class="table-responsive">
				<table class="table" style="width:auto">
						<tr>
							<td>
								<a href="{{ route('outlet.edit',['outlet'=>$outlets->id])}}">	
									<button class="btn btn-info btn-xs">Edit</button>
								</a>
							</td>
							<td>

								<a href="{{ route('outlet.delete',['outlet'=>$outlets->id])}}">
									<button class="btn btn-danger btn-xs">Trash</button>
								</a>
							</td>

							<td>
								@if($previous)
									<a href="{{ route('outlet.view',['outlets'=>$previous->id]) }}">
										<button class="btn btn-primary btn-xs"> Previous</button>
									</a>
								@endif
							</td>
							<td>

								@if($next)
									<a href="{{ route('outlet.view',['outlets'=>$next->id]) }}">
										<button class="btn btn-primary btn-xs"> Next</button>
									</a>
								@endif
							</td>
						</tr>
						<tr>
							<td>
								<b>Code</b>
							</td>
							<td>
								{{ $outlets->code }}
							</td>
						
							<td>
								<b>Outlet Name</b>
							</td>
							<td>
								{{ $outlets->name }}
							</td>
						<tr>
							<td>
								<b>Address</b>
							</td>
							<td>
								{{ $outlets->address }}
							</td>
						
							<td>
								<b>City</b>
							</td>
							<td>
								<?php
									$cities = Session::get('cities');
								?>	
								@foreach($cities as $key=>$value)
									@if($value['id'] === $outlets->city_id)
										{{ $value['name'] }}
									@endif
								@endforeach
								
								<!--{{ $outlets->city_id }}-->
							</td>
						</tr>

						<tr>
							<td>
								<b>Nearest To Depot</b>
							</td>

							<td>
								{{ $outlets->nearest_to_depot }}
							</td>
						
							<td>
								<b>Distance To Depot</b>
							</td>

							<td>
								{{ $outlets->distant_to_depot }}
							</td>
						</tr>


						<tr>
							<td>
								<b>Tank Number</b>
							</td>

							<td>
								{{ $outlets->tank_no }}
							</td>
						
							<td>
								<b>Tank Capacity</b>
							</td>

							<td>
								{{ $outlets->capacity }}
							</td>
						</tr>

						<tr>
							<td>
								<b>No of pumps</b>
							</td>

							<td>
								{{ $outlets->pump_no }}
							</td>
						
							<td>
								<b>Date of purchase</b>
							</td>

							<td>
								{{ $outlets->date_registered }}
							</td>
						</tr>

						<tr>
							<td>
								<b>Low Demand Season</b>
							</td>

							<td>
								{{ $outlets->low_demand_season }}
							</td>
						
							<td>
								<b>High Demand Season</b>
							</td>

							<td>
								{{ $outlets->high_demand_season }}
							</td>
						</tr>

						<tr>
							<td>
								<b>Status</b>
							</td>

							<td>
								<?php
									$statuses = Session::get('statuses');
								?>	
								@foreach($statuses as $key=>$value)
									@if($value['id'] === $outlets->status_id)
										{{ $value['name'] }}
									@endif
								@endforeach
						
							</td>

							<td>
								<b>Marketter</b>
							</td>

							<td>
								<?php 
									$city = App\Models\City::where('id', '=', $outlets->city_id)->first(); 
									//$userid should be read from the database and not the authenticated user
									// $userid = auth::User()->id;
									$marketer = "N/A";
									$mktrid = $outlets->marketer_id;
									//dd($mktrid);
									$userid = App\Models\Marketer::where('user_id', '=', $mktrid)->first();
									//dd($userid->id);
									$mktr = App\Models\User::find($userid['id']);
									//dd($mktr->last_name);
									if(!is_null($mktr)){
										try {
											$marketer = $mktr->User_profile->last_name . ' '  . $mktr->User_profile->first_name;
										} catch (Exception $e) {
											
										}
										
									}
									

									 //$createdBy = $user->profile->last_name;
								//dd($city);
								?>
						
							</td>

						</tr>

					<tr>
						<td>
							<a href="{{ route('outlet.edit',['outlet'=>$outlets->id])}}">	
								<button class="btn btn-info btn-xs">Edit</button>
							</a>
						</td>
						<td>

							<a href="{{ route('outlet.delete',['outlet'=>$outlets->id])}}">
								<button class="btn btn-danger btn-xs">Trash</button>
							</a>
						</td>

						<td>
							@if($previous)
								<a href="{{ route('outlet.view',['outlets'=>$previous->id]) }}">
									<button class="btn btn-primary btn-xs"> Previous</button>
								</a>
							@endif
						</td>
						<td>

							@if($next)
								<a href="{{ route('outlet.view',['outlets'=>$next->id]) }}">
									<button class="btn btn-primary btn-xs"> Next</button>
								</a>
							@endif
						</td>
					</tr>

				</table>
				
			</div>
		</div>

	
	


@stop