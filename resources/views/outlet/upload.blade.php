@extends('layouts.app1')

@section('content')
    @include('flash::message')
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-6">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3>Upload Retail Outlet</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="box-group" id="accordion">
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        <div class="panel box box-primary">
                            <div class="box-header with-border">
                                <h4 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion">
                                        Upload Marketers Records from Excel File

                                        <a href="{{asset('custom/downloadable/outlet.csv')}}">
                                            <code class="btn btn-xs btn-info">Download Template</code>
                                        </a>

                                    </a>
                                </h4>
                            </div>
                            <div id="" class="panel-collapse  in">
                                <div class="box-body">

                                    {!! Form::open(array('route' => 'upload_Outlet_excel','method'=>'POST','files'=>'true')) !!}
                                    <br>
                                    <div class="form-group has-feedback">
                                        {!! Form::file('sample_file', array('class' => '' )) !!}
                                        {!! $errors->first('sample_file', '<p class="alert alert-danger">:message</p>') !!}
                                        <span class="fa fa-upload form-control-feedback"></span>
                                    </div>


                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <label for="city_id">Select Marketer</label>
                                        <select class="form-control" name="marketer_id">
                                            <option class="list-item" value="nil">
                                                Select Marketer
                                            </option>
                                            @foreach($marketers as $each)
                                                <option class="list-item" value="{{ $each->id }}">
                                                    <?php if(isset($each->user->user_profile)): ?>
                                                    {{ $each->user->user_profile->first_name }}  {{ $each->user->user_profile->last_name }}

                                                    <?php else: ?>
                                                    {{ $each->code }}

                                                    <?php endif; ?>
                                                </option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('marketer_id'))
                                            <span class="help-block">
                                                <strong style="color: red;">{{ $errors->first('marketer_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                    <div class="row">
                                        <div class="col-xs-4 pull-right">
                                            {!! Form::submit('Upload',['class'=>'btn btn-primary btn-block btn-flat']) !!}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>



@stop