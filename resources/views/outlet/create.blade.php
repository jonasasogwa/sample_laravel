@extends('layouts.app1')

@section('content')
    @include('flash::message')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>Create New Retail Outlet</h1>
        </div>
        <div class="panel-body">
            <form action="{{ route('outlet.create')}}" method="post">
            {{ csrf_field() }}

            <!--
				<div class="form-group">
		 			<label for="code">Outlet Identification Code</label>
		 			<input type="text" class="form-control" name="code">
                    @if($errors->has('code'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('code') }}</strong>
						</span>
                    @endif
                    </div>
-->

                <div class="form-group">
                    <label for="content">Outlet Name</label>
                    <input class="form-control" name="name" type="text">
                    @if($errors->has('name'))
                        <span class="help-block">
							<strong style="color: red;">{{ $errors->first('name') }}</strong>
						</span>
                    @endif
                </div>

                {{--@role((['outlet-manager']))--}}
                <div class="form-group">
                    <label for="city_id">Select Marketer</label>

                    <select class="form-control" name="marketer_id">
                        <option class="list-item" value="nil">
                            Select Marketer
                        </option>
                        @foreach($marketers as $each)
                            <option class="list-item" value="{{ $each->id }}">
                                {{ $each->code }}
                            </option>
                        @endforeach


                    </select>
                    @if($errors->has('marketer_id'))
                        <span class="help-block">
							<strong style="color: red;">{{ $errors->first('marketer_id') }}</strong>
						</span>
                    @endif
                </div>
                {{--@endrole--}}


                {{--<div class="form-group">--}}
                    {{--<label for="dealer_name">Dealer Name</label>--}}
                    {{--<input type="text" class="form-control" name="dealer_name">--}}
                    {{--@if($errors->has('dealer_name'))--}}
                        {{--<span class="help-block">--}}
							{{--<strong style="color: red;">{{ $errors->first('dealer_name') }}</strong>--}}
						{{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}

                <div class="form-group">
                    <label for="content">Address</label>
                    <textarea class="form-control" name="address" rows="5" cols="5"></textarea>
                    @if($errors->has('address'))
                        <span class="help-block">
							<strong style="color: red;">{{ $errors->first('address') }}</strong>
						</span>
                    @endif
                </div>





                <div class="form-group">
                    <label for="city_id">Select City</label>

                    <select class="form-control" name="city_id">
                        <option class="list-item" value="nil">
                            Select City
                        </option>
                        @foreach($cities as $city)
                            <option class="list-item" value="{{ $city->id }}">
                                {{ $city->name }}
                            </option>
                        @endforeach


                    </select>
                    @if($errors->has('city_id'))
                        <span class="help-block">
							<strong style="color: red;">{{ $errors->first('city_id') }}</strong>
						</span>
                    @endif
                </div>



                <div class="form-group">
                    <label for="city_id">Nearest Depot</label>

                    <select class="form-control" name="nearest_to_depot">
                        <option class="list-item" value="nil">
                            Select Nearest Depot
                        </option>
                        @foreach($depots as $each)
                            <option class="list-item" value="{{ $each->id }}">
                                {{ $each->name }}
                            </option>
                        @endforeach


                    </select>
                    @if($errors->has('nearest_to_depot'))
                        <span class="help-block">
							<strong style="color: red;">{{ $errors->first('nearest_to_depot') }}</strong>
						</span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="distant_to_depot">Distance from Depot (KM)</label>
                    <input type="number" class="form-control" name="distant_to_depot">
                    @if($errors->has('distant_to_depot'))
                        <span class="help-block">
							<strong style="color: red;">{{ $errors->first('distant_to_depot') }}</strong>
						</span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="tank_no">Tank No</label>
                    <input type="number" class="form-control" name="tank_no">
                    @if($errors->has('tank_no'))
                        <span class="help-block">
							<strong style="color: red;">{{ $errors->first('tank_no') }}</strong>
						</span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="capacity">Capacity (Litres)</label>
                    <input type="number" class="form-control" name="capacity">
                    @if($errors->has('capacity'))
                        <span class="help-block">
							<strong style="color: red;">{{ $errors->first('capacity') }}</strong>
						</span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="pump_no">Pump Number</label>
                    <input type="number" class="form-control" name="pump_no">
                    @if($errors->has('pump_no'))
                        <span class="help-block">
							<strong style="color: red;">{{ $errors->first('pump_no') }}</strong>
						</span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="date_of_purchase">Date of Purchase</label>
                    <input type="date" class="form-control" name="date_of_purchase">
                    @if($errors->has('date_of_purchase'))
                        <span class="help-block">
							<strong style="color: red;">{{ $errors->first('date_of_purchase') }}</strong>
						</span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="low_demand_season">Low Demand Season(Litres)</label>
                    <input type="number" class="form-control" name="low_demand_season">
                    @if($errors->has('low_demand_season'))
                        <span class="help-block">
							<strong style="color: red;">{{ $errors->first('low_demand_season') }}</strong>
						</span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="high_demand_season">High Demand Season(Litres)</label>
                    <input type="number" class="form-control" name="high_demand_season">
                    @if($errors->has('high_demand_season'))
                        <span class="help-block">
							<strong style="color: red;">{{ $errors->first('high_demand_season') }}</strong>
						</span>
                    @endif
                </div>




                <div class="text-center">
                    <button class="btn btn-success" type="submit">Submit</button>
                </div>

            </form>
        </div>
    </div>

@stop