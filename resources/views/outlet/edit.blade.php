@extends('layouts.app1')

@section('content')
<?php
use App\City;
?>

<div  class="panel panel-default">
	@include('flash::message')
 	<div class="panel-heading">
	 	<h1>Edit Retail Outlet</h1>
	 </div>
	 <div class="panel-body">

	 	<form action="{{ route('outlet.update', ['id'=>$outlet->id]) }}" method="post">
			{{ csrf_field()}}
			{{--<div class="form-group">--}}
				{{--<label for="outlet_code">Outlet Code</label>--}}
				{{--<input type="text" class="form-control" name="outlet_code" value="{{ $outlet->code }}"></input>--}}
			{{--</div>--}}

			<div class="form-group">
				<label for="outlet_code">Outlet Name</label>
				<input type="text" class="form-control" name="name" value="{{ $outlet->name }}"></input>
			</div>



			<div class="form-group">
				<label for="address">Address</label>
				<input type="text" class="form-control" name="address" value="{{ $outlet->address }}"></input>
			</div>

			<div class="form-group">
				<label for="city">City</label>
				<!--$cities = Session::get('cities');-->
				<?php
					//$cities = City::hydrate(Session::get('cities'));
					$cities = Session::get('cities');
					//dd($cities);
				?>
				
				<select name="city_id" class="form-control">
					
					@foreach($cities as $city)
						@if($city['id']==$outlet->city_id)
							<option value="{{ $city['id'] }}" selected="selected">{{ $city['name'] }}</option>
						@else
							<option value="{{ $city['id'] }}">{{ $city['name'] }}</option>
						@endif
					@endforeach
				</select>
				
			</div>

			<div class="form-group">
				<label for="nearest_to_depot">Nearest To Depot</label>
				<input type="text" class="form-control" name="nearest_to_depot" value="{{ $outlet->nearest_to_depot }}"></input>
			</div>

			<div class="form-group">
				<label for="distant_to_depot">Distance to Depot(KM)</label>
				<input type="text" class="form-control" name="distant_to_depot" value="{{ $outlet->distant_to_depot }}"></input>
			</div>

			<div class="form-group">
				<label for="tank_no">Tank Number</label>
				<input type="text" class="form-control" name="tank_no" value="{{ $outlet->tank_no }}"></input>
			</div>

			<div class="form-group">
				<label for="dealer_name">Capacity(Litres)</label>
				<input type="text" class="form-control" name="capacity" value="{{ $outlet->capacity }}"></input>
			</div>

			<div class="form-group">
				<label for="pump_no">Pump Number</label>
				<input type="text" class="formcontrol" name="pump_no" value="{{ $outlet->pump_no }}"></input>
			</div>

			<div class="form-group">
				<label for="date_of_purchase">Date of Purchase</label>
				<input type="text" class="form-control" name="date_of_purchase" value="{{ $outlet->date_of_purchase }}"></input>
			</div>

			<div class="form-group">
				<label for="low_demand_season">Low Demand Season(Litres)</label>
				<input type="text" class="form-control" name="low_demand_season" value="{{ $outlet->low_demand_season }}"></input>
			</div>

			<div class="form-group">
				<label for="high_demand_season">High Demand Season(Litres)</label>
				<input type="text" class="form-control" name="high_demand_season" value="{{ $outlet->high_demand_season }}"></input>
			</div>

			<?php
				//$cities = City::hydrate(Session::get('cities'));
				$statuses = Session::get('statuses');
				//dd($cities);
			?>
			<div class="form-group">
				<label for="status_id">Status</label>
				
				<select name="status_id" class="form-control">
				@foreach($statuses as $status)
					@if($status['id']== $outlet->status_id)
						<option value="{{ $status['id'] }}" selected="selected">{{ $status['name'] }}</option>
					@else
						<option value="{{ $status['id'] }}">{{ $status['name'] }}</option>
					@endif
				@endforeach

				</select>
				
			</div>

			<div class="form-group">
				<button class="btn btn-info btn-xs" type="submit">Update</button>
			</div>

		</form>

	 </div>
	</div>




@stop