@extends('layouts.app1')
@section('content')
	@include('flash::message')
<h1>List of Retail Outlets</h1>

		 <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <a class="btn btn-default btn-xs" href="{{ route('outlet-excel-file',['type'=>'xls']) }}">Download Excel xls</a> |
                <a class="btn btn-default btn-xs" href="{{ route('outlet-excel-file',['type'=>'xlsx']) }}">Download Excel xlsx</a> |
                <a class="btn btn-default btn-xs" href="{{ route('outlet-excel-file',['type'=>'csv']) }}">Download CSV</a>
            </div>
        </div>
<div  class="panel panel-default">
	<div class="table-responsive">	 


		<table class="table">
			<thead>
				<th>
					Outlet Code 
				</th>
				<th>
	 				 Name
				</th>
				<th>
					 Outlet Address
				</th>

				<th>
					City
				</th>

				<th>
					 Marketter
				</th>

				<th>
					 Manage Records
				</th>
			</thead>
			
			@if($outlets->count() ===0)
				<tr>
					<td colspan="4"><h4>Retail outlet table is empty!</h4></td>
				</tr>
				
			@else

				@foreach($outlets as $outlet)
					<?php 
						$city = App\Models\City::where('id', '=', $outlet->city_id)->first(); 
						//$userid should be read from the database and not the authenticated user
						// $userid = auth::User()->id;
						$marketer = "N/A";
						$mktrid = $outlet->marketer_id;
						//dd($mktrid);
						$userid = App\Models\Marketer::where('user_id', '=', $mktrid)->first();
						//dd($userid->id);
						$mktr = App\Models\User::find($userid['id']);
						//dd($mktr->last_name);
						if(!is_null($mktr)){
							try {
								$marketer = $mktr->User_profile->last_name . ' '  . $mktr->User_profile->first_name;
							} catch (Exception $e) {
								
							}
							
						}
						

						 //$createdBy = $user->profile->last_name;
					//dd($city);
					?>

					<tr>
							
						<td>
							{{ $outlet->code }}
						</td>
					
						<td>
							{{ $outlet->name }}
						</td>
						
						<td>
							{{ $outlet->address }}
						</td>

						<td>
							@if(!empty($city->name))
								{{ $city->name }}
							@endif
						</td>

						<td>

							{{ $marketer }}
						</td>
						<td>
							<div class="btn-group btn-group-xs" role="group" aria-label="...">
								<a href="{{ route('outlet.view',['outlets'=>$outlet->id])}}">	
									<button class="btn btn-info btn-xs">View</button>
								</a>
								<a href="{{ route('outlet.edit',['outlet'=>$outlet->id])}}">	
									<button class="btn btn-warning btn-xs">Edit</button>
								</a>

								<a href="{{ route('outlet.delete',['outlet'=>$outlet->id])}}">
										<button class="btn btn-danger btn-xs">Trash</button>
								</a>
							</div>
						</td>

					</tr>

				@endforeach
			@endif
		</table>
		<div class="text-center">
			{{ $outlets->links() }}
		</div>
	</div>

</div>




@stop