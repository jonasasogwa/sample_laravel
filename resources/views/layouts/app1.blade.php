<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NHCMB</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{asset('custom/css/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('custom/css/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{asset('custom/css/dist/css/skins/_all-skins.min.css')}}">
    <!-- Styles -->
    <link href="{{ asset('custom/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('custom/datepicker/dcalendar.picker.css') }}" rel="stylesheet" type="text/css">
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="{{ url('/') }}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>nhc</b>mb</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>NHC</b>MB</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">4</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 4 messages</li>
                            <li class="footer"><a href="#">See All Messages</a></li>
                        </ul>
                    </li>
                    <!-- Notifications: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <ul class="dropdown-menu">
                            <li class="header">You have 10 notifications</li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                    <!-- Tasks: style can be found in dropdown.less -->
                    <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-flag-o"></i>
                            <span class="label label-danger">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 9 tasks</li>
                            <li class="footer">
                                <a href="#">View all tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src=" {{ asset('custom/css/dist/img/avatar.png') }} " class="user-image" alt="User Image">
                            <span class="hidden-xs">{{Illuminate\Support\Facades\Auth::user()->email}}</span>
                        </a>
                        @guest
                            <ul class="dropdown-menu">
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{{ route('login') }}" class="btn btn-default btn-flat">Login</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('register') }}" class="btn btn-default btn-flat">Register</a>
                                    </div>
                                </li>
                            </ul>
                        @else
                            <ul class="dropdown-menu">
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                                           class="btn btn-default btn-flat">Sign out</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        @endguest
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>

    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ asset('custom/css/dist/img/avatar.png') }}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>
                        <?php
                        $id = Illuminate\Support\Facades\Auth::user()->email;
                        $name = \App\Models\User_profile::whereRaw('user_id','=',$id);
                        ?>

                        {{$id}}
                    </p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn btn-flat">
						<i class="fa fa-search"></i>
					</button>
				</span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>

                <li>
                    <a href="{{ url('/home') }}"><i class="fa fa-book"></i> <span>Dashboard</span></a>
                </li>

                @role((['admin']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-th"></i> <span>Manage Users</span>
                        <span class="pull-right-container">
                            <span class="label label-primary pull-right">*</span>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('users') }}"><i class="fa fa-circle-o"></i> View Users</a></li>
                        <li><a href="{{route('sort_users_view')}}"><i class="fa fa-circle-o"></i> Sort Users</a></li>
                    </ul>
                </li>
                @endrole

                @role((['admin']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-th"></i> <span>Manage Marketers</span>
                        <span class="pull-right-container">
                            <span class="label label-primary pull-right">*</span>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('marketers') }}"><i class="fa fa-circle-o"></i> View/Upload Marketers</a></li>
                        <li><a href="{{ route('add_marketer') }}"><i class="fa fa-circle-o"></i> Add Marketers</a></li>
                        <li><a href="{{ route('marketers_requests') }}"><i class="fa fa-circle-o"></i> Marketers Request</a></li>
                    </ul>
                </li>
                @endrole

                @role((['admin', 'outlet-admin']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-th"></i> <span>Manage Outlets</span>
                        <span class="pull-right-container">
                            <span class="label label-primary pull-right">*</span>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('outlet.manage')}}"><i class="fa fa-circle-o"></i> View Outlets</a></li>
                        <li><a href="{{ route('outlet.addnew')}}"><i class="fa fa-circle-o"></i> Create Outlet</a></li>
                        <li><a href="{{ route('outlet.upload')}}"><i class="fa fa-circle-o"></i> Upload Outlets</a></li>
                    </ul>
                </li>
                @endrole

                @role((['admin', 'admin-auditor']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-th"></i> <span>Manage Auditors</span>
                        <span class="pull-right-container">
                            <span class="label label-primary pull-right">*</span>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url('auditors') }}"><i class="fa fa-circle-o"></i> View Auditors</a></li>
                        <li><a href="{{ url('auditors/add') }}"><i class="fa fa-circle-o"></i> Add Auditor</a></li>
                        <li><a href="{{ url('auditors/upload') }}"><i class="fa fa-circle-o"></i> Upload Auditors</a></li>
                    </ul>
                </li>
                @endrole

                @role((['admin']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-th"></i> <span>Manage Terminals</span>
                        <span class="pull-right-container">
                            <span class="label label-primary pull-right">*</span>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url('terminals') }}"><i class="fa fa-circle-o"></i> View Terminals</a></li>
                        <li><a href="{{ url('terminals/add') }}"><i class="fa fa-circle-o"></i> Add Terminal</a></li>
                        <li><a href="{{ url('terminals/upload') }}"><i class="fa fa-circle-o"></i> Upload Terminals</a></li>
                    </ul>
                </li>
                @endrole

                <li class="header">REPORT</li>

                @role((['admin']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-bar-chart-o"></i> <span>Product Receives Report</span>
                        <span class="pull-right-container">
                            <span class="label label-primary pull-right"></span>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('mother_vessel_receives') }}"><i class="fa fa-circle-o"></i> Mother Vessel Receive</a></li>
                        <li><a href="{{route('daughter_vessel_receives')}}"><i class="fa fa-circle-o"></i> Daughter Vessel Receive</a></li>
                        <li><a href="{{route('terminals_receives')}}"><i class="fa fa-circle-o"></i> Terminal Receive</a></li>
                        <li><a href="{{route('depots_receives')}}"><i class="fa fa-circle-o"></i> Depot Receive</a></li>
                        <li><a href="{{route('outlets_receives')}}"><i class="fa fa-circle-o"></i> Retail Outlets Receive</a></li>
                    </ul>
                </li>
                @endrole

                @role((['admin']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-bar-chart-o"></i> <span>Product Dispatches Report</span>
                        <span class="pull-right-container">
                            <span class="label label-primary pull-right"></span>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('mother_vessel_dispatches') }}"><i class="fa fa-circle-o"></i> Mother Vessel Dispatched</a></li>
                        <li><a href="{{route('daughter_vessel_dispatches')}}"><i class="fa fa-circle-o"></i> Daughter Vessel Dispatched</a></li>
                        <li><a href="{{route('terminals_dispatches')}}"><i class="fa fa-circle-o"></i> Terminal Dispatched</a></li>
                        <li><a href="{{route('depots_dispatches')}}"><i class="fa fa-circle-o"></i> Depot Dispatched</a></li>
                    </ul>
                </li>
                @endrole

                @role((['admin']))
                <li><a href="{{route('retail-outlet-mass-balance')}}"><i class="fa fa-line-chart"></i> <span>Retail-Outlet Mass Balance</span></a></li>
                @endrole

                @role((['admin']))
                <li>
                    <a href="{{ url('reports/consolidated-report/') }}"><i class="fa fa-line-chart"></i> <span>Consolidated Report</span></a>
                </li>
                @endrole

                @role((['admin']))
                <li>
                    <a href="{{ url('reports/outlet-with-without-product/') }}"><i class="fa fa-line-chart"></i> <span>Outlet with/without Product</span>
                    </a>
                </li>
                @endrole

                @role((['admin']))
                <li>
                    <a href="{{ url('reports/product-dispatched-not-received/') }}"><i class="fa fa-line-chart"></i> <span>Product Dispatched & not Received</span>
                    </a>
                </li>
                @endrole

                <li class="header">SETTINGS</li>
                @role((['admin']))
                <li>
                    <a href="{{ url('logs') }}"><i class="fa fa-book"></i> <span>Logs</span></a>
                </li>
                @endrole

                <li>
                    <a href="{{ url('auditors/edit/') }}/{{\Illuminate\Support\Facades\Auth::user()->id}}"><i class="fa fa-book"></i> <span>Edit Info</span></a>
                </li>

                <li>
                    <a href="{{ url('/change-password') }}"><i class="fa fa-book"></i> <span>Change Password</span></a>
                </li>


            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">

                @yield('content')

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2018 <a href="ikooba.com">ikOOba Technologies</a>.</strong> All rights
        reserved.
    </footer>
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
    </aside>
    <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 2.2.3 -->
<script src="{{asset('custom/js/mplugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 3.3.6 -->
<script src="{{asset('custom/css/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('custom/js/mplugins/slimScroll/jquery.slimscroll.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('custom/css/dist/js/app.min.js')}} "></script>

<script src="{{ asset('custom/datepicker/dcalendar.picker.js') }}"></script>

<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>


@yield('js')

</body>
</html>
