<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('custom/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('custom/datepicker/dcalendar.picker.css') }}" rel="stylesheet" type="text/css">

</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                        <li><a class="nav-link" href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">

            <div class="col-lg-3">
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="#">Home</a>
                    </li>

                    @role((['admin', 'outlet-admin']))

                    <li class="list-group-item">
                        <a href="{{ url('logs') }}">Logs</a>
                    </li>

                    <li class="list-group">
                        <ul class="list-group-item">
                            <a href="{{ route('users') }}">Users</a>
                            <li class="list-group-item">
                                <a href="{{ route('users') }}">View Users</a>
                            </li>
                            <li class="list-group-item">
                                <a href="#">Sort Users</a>
                            </li>
                        </ul>
                    </li>
                    @endrole

                    @role((['admin', 'outlet-admin']))
                    <li class="list-group">
                        <ul class="list-group-item">
                            <a href="{{ route('marketers') }}">Marketer</a>
                            <li class="list-group-item">
                                <a href="{{ route('marketers') }}">View Marketers</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('add_marketer') }}">Add Marketers</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('marketers_requests') }}">Marketers Request</a>
                            </li>
                        </ul>
                    </li>
                    @endrole

                    @role((['admin', 'outlet-admin', 'outlet-manager']))
                    <li class="list-group-item">
                        <a href="{{ route('outlet.manage')}}">Outlet</a>
                        <ul class="list-group-item">
                            <li class="list-group-item">
                                <a href="{{ route('outlet.addnew')}}">Create Outlet</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('outlet.upload')}}">Upload Outlets</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ route('outlet.manage')}}">Manage Outlets</a>
                            </li>

                        </ul>
                    </li>
                    @endrole

                    @role((['admin', 'admin-auditor']))
                    <li class="list-group">
                        <ul class="list-group-item">
                            <a href="#">Auditor</a>
                            <li class="list-group-item">
                                <a href="{{ url('auditors') }}">View Auditor</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ url('auditors/upload') }}">Upload Auditors</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ url('auditors/add') }}">Add Auditor</a>
                            </li>
                        </ul>
                    </li>
                    @endrole

                    @role((['admin']))
                    <li class="list-group">
                        <ul class="list-group-item">
                            <a href="#">Terminal</a>
                            <li class="list-group-item">
                                <a href="{{ url('terminals/upload') }}">Upload Terminals</a>
                            </li>

                            <li class="list-group-item">
                                <a href="{{ url('terminals') }}">View Terminal</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ url('terminals/add') }}">Add Terminal</a>
                            </li>
                        </ul>
                    </li>
                    @endrole

                    @role((['admin']))
                    <li class="list-group">

                        <ul class="list-group-item">
                            <a href="#">Product received Report</a>
                            <li class="list-group-item">
                                <a href="{{ route('mother_vessel_receives') }}">Mother Vessel Receive</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{route('daughter_vessel_receives')}}">Daughter Vessel Receive</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{route('terminals_receives')}}">Terminal Receive</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{route('depots_receives')}}">Depot Receive</a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{route('outlets_receives')}}">Retail Outlets Receive</a>
                            </li>
                        </ul>
                    </li>

                    <li class="list-group">

                        <ul class="list-group-item">
                            <a href="#">Product Dispatched Report</a>

                            <li class="list-group-item">
                                <a href="{{ route('mother_vessel_dispatches') }}">Mother Vessel Dispatched</a>
                            </li>

                            <li class="list-group-item">
                                <a href="{{route('daughter_vessel_dispatches')}}">Daughter Vessel Dispatched</a>
                            </li>

                            <li class="list-group-item">
                                <a href="{{route('terminals_dispatches')}}">Terminal Dispatched</a>
                            </li>

                            <li class="list-group-item">
                                <a href="{{route('depots_dispatches')}}">Depot Dispatched</a>
                            </li>

                        </ul>
                    </li>

                    <li class="list-group-item">
                        <a href="{{ url('reports/product-dispatched-not-received/') }}">Product Dispatched & not
                            Received</a>
                    </li>
                   
                    <li class="list-group-item">
                        <a href="{{route('retail-outlet-mass-balance')}}">Retail-Outlet Mass Balance</a>
                    </li>


                    <li class="list-group-item">
                        <a href="{{ url('reports/consolidated-report/') }}">Consolidated Report</a>
                    </li>

                    <li class="list-group-item">
                        <a href="{{ url('reports/outlet-with-without-product/') }}">Outlet with/without Product</a>
                    </li>
                    @endrole

                    <?php if(\Illuminate\Support\Facades\Auth::user()):?>
                    <li class="list-group-item">
                        <a href="{{ url('auditors/edit/') }}/{{\Illuminate\Support\Facades\Auth::user()->id}}">Edit
                            Info</a>
                    </li>
                    <?php endif; ?>

                    <li class="list-group-item">
                        <a href="{{ url('/change-password') }}">Change Password</a>
                    </li>

                </ul>
            </div>

            <div class="col-lg-9">
                @yield('content')
            </div>

        </div>
    </div>

</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('custom/js/jquery.js')}}"></script>
<script src="{{ asset('custom/datepicker/dcalendar.picker.js') }}"></script>

<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>

@yield('js')
</body>
</html>
