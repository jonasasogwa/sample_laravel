@extends('layouts.app1')

@section('content')

    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">

                        <a class="btn btn-default btn-xs" href="{{ route('terminals-excel-file',['type'=>'xls']) }}">Download Excel xls</a> |
                        <a class="btn btn-default btn-xs" href="{{ route('terminals-excel-file',['type'=>'xlsx']) }}">Download Excel xlsx</a> |
                        <a class="btn btn-default btn-xs" href="{{ route('terminals-excel-file',['type'=>'csv']) }}">Download CSV</a>

                    </div>
                </div>

                <table class="table table-hover">

                    <thead>
                        <th>S/N</th>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Date Installed</th>
                        <th>Capacity</th>
                        <th>Settings</th>
                    </thead>
                        <?php $sn=1;?>
                    <tbody>
                    @foreach($terminals as $terminal)
                        <tr>
                            <td>{{$sn++}}</td>
                            <td>
                                {{ $terminal->name }}
                            </td>
                            <td>
                                {{ $terminal->code }}
                            </td>
                            <td>
                                {{ $terminal->date_installed }}
                            </td>
                            <td>
                                {{ $terminal->capacity }}
                            </td>
                            <td>
                                <a href="#" class="btn btn-xs btn-info "> view </a>
                                <a href="{{url('/terminals/edit/')}}/{{$terminal->id}}" class="btn btn-xs btn-warning "> edit </a>
                                <a type="button" data-info='{{$terminal->id}}}' class="btn btn-xs btn-danger removeTerminal">trash</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        {{ $terminals->links() }}
                    </ul>
                </div>

            </div>

        </div>

    </div>

    <script>

        $(document).on('click', '.removeTerminal', function(){
            var id = $(this).attr('data-info');
            var r =confirm('Are you sure you want to delete this  Auditor');
            //alert(id);
            if(r){
                $.get('terminals/delete', {'id':id})
                    .done(function(e){
                        if(e == '200'){
                            alert('It has been deleted');
                            location.reload();
                        }
                        if(e == '403'){
                            alert('You cannot delete this Auditor because it being used by one or more process');
                        }
                    })
                    .fail(function(){
                        alert('error');
                    });
            }
        });

    </script>
@endsection
