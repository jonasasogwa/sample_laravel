<?php
$data = '';
if(isset($terminalObj)){
    $data = $terminalObj;
}
?>

@extends('layouts.app1')

@section('content')

    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                <div class="panel panel-default">
                    <div class="panel-body">

                        @if($data)
                            {!! Form::open(['url' => '/terminals/edit', 'id' => "TerminalForm", 'class'=>'form-horizontal']) !!}
                            {!! Form::input('hidden','terminal_id',encrypt($data->id),['class' => '', 'id' => "terminal_id"]) !!}
                        @else
                            {!! Form::open(['url' => '/terminals/create', 'id' => "TerminalForm", 'class'=>'form-horizontal']) !!}
                        @endif

                        @if($data)
                            <legend>Edit Terminal </legend>
                        @else
                            <legend>Create New Terminal </legend>
                        @endif

                    <!--<div class="messages"></div>-->
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    <div class="form-group">
                        <label for="branch_name" class="col-sm-2 control-label"> Name</label>
                        <div class="col-sm-10">
                            @if($data)
                                {!! Form::input('text','name',$data->name,["placeholder" => "Enter Name  ", "class" => "form-control required", "id" => "name"
                                ]) !!}
                                @if ($errors->has('name')) <p class="help-block ">{{ $errors->first('name') }}</p> @endif
                            @else
                                {!! Form::input('text','name',null,["placeholder" => "Enter Name  ", "class" => "form-control required", "id" => "name"]) !!}
                                @if ($errors->has('name')) <p class="help-block has-error">{{ $errors->first('name') }}</p> @endif
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="branch_name" class="col-sm-2 control-label"> Code</label>
                        <div class="col-sm-10">
                            @if($data)
                                {!! Form::input('text','code',$data->code,["placeholder" => "Enter Code  ", "class" => "form-control required", "id" => "code",
                                ]) !!}
                                @if ($errors->has('code')) <p class="help-block ">{{ $errors->first('code') }}</p> @endif
                            @else
                                {!! Form::input('text','code',null,["placeholder" => "Enter Code  ", "class" => "form-control required", "id" => "code",
                                ]) !!}
                                @if ($errors->has('code')) <p class="help-block has-error">{{ $errors->first('code') }}</p> @endif
                            @endif
                        </div>
                    </div>


            <div class="form-group">
                <label for="branch_name" class="col-sm-2 control-label"> Capacity</label>
                <div class="col-sm-10">
                    @if($data)
                        {!! Form::input('number','capacity',$data->capacity,["placeholder" => "Enter Capacity  ", "class" => "form-control required", "id" => "capacity",
                        ]) !!}
                                    @if ($errors->has('capacity')) <p class="help-block ">{{ $errors->first('capacity') }}</p> @endif
                                @else
                                    {!! Form::input('text','capacity',null,["placeholder" => "Enter capacity  ", "class" => "form-control", "id" => "capacity",
                                    ]) !!}
                                    @if ($errors->has('capacity')) <p class="help-block has-error">{{ $errors->first('capacity') }}</p> @endif
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="branch_name" class="col-sm-2 control-label"> Date Installed</label>
                            <div class="col-sm-10">
                                @if($data)
                                    {!! Form::input('text','date_installed',$data['date_installed'],["placeholder" => "Enter Date Installed  ", "class" => "form-control required", "id" => "date_installed",
                                    ]) !!}
                                    @if ($errors->has('date_installed')) <p class="help-block ">{{ $errors->first('date_installed') }}</p> @endif
                                @else
                                    {!! Form::input('text','date_installed',null,["placeholder" => "Enter Date Installed  ", "class" => "form-control required", "id" => "date_installed1",
                                    ]) !!}
                                    @if ($errors->has('date_installed')) <p class="help-block has-error">{{ $errors->first('date_installed') }}</p> @endif
                                @endif
                            </div>
                        </div>

                        <div class="messages"></div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                @if($data)
                                    <button type="submit" class="btn btn-success" data-loading-text="Loading..." id="companyBtn"> <i class="glyphicon glyphicon-ok-sign"></i> Update Record </button>
                                @else
                                    <button type="submit" class="btn btn-success" data-loading-text="Loading..." id="companyBtn"> <i class="glyphicon glyphicon-ok-sign"></i>  Save Record </button>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection

@section('js')

    <script>
        $('#date_installed').dcalendarpicker();
        $('#date_installed1').dcalendarpicker();
    </script>

@stop