<div class="container">

    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            text-align: left;
            padding: 8px;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }
        tr:nth-child(even){background-color: #f2f2f2}
    </style>

    <table class="table">
        <tr>
            <th scope="col">Source Type:</th>
            <td scope="col">Terminal</td>
            <th scope="col">Source Name:</th>
            <td scope="col">Terminal Receives</td>
        </tr>
    </table><br>

    <?php $sn=1; ?>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">S/N</th>
            <th scope="col">Product Type</th>
            <th scope="col">Terminal</th>
            <th scope="col">Quantity Dispatched</th>
            <th scope="col">Bill Date</th>
        </tr>
        </thead>
        <tbody>
        @foreach($terminals as $terminal)
            <tr>
                <td  scope="col">{{$sn++}}</td>

                <td scope="row">
                    {{$terminal->type->name}}
                </td>

                <td>
                    {{$terminal->terminal->name}}
                </td>

                <td>
                    {{$terminal->qty_dispatch}}
                </td>

                <td>
                    {{$terminal->bill_date}}
                </td>

            </tr>
        @endforeach

        </tbody>
    </table>

</div>