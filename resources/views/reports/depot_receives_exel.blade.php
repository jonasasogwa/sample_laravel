
    <div class="container">

        <table id="example1" class="table table-hover">
            <thead>
                <th>Terminal</th>
                <th>Depot</th>
                <th>Truck No.</th>
                <th>Metre Ticket No</th>
                <th>Product Qty</th>
                <th>Loading Date</th>
                <th>Arrival Date</th>
            </thead>
            <tbody>
            @foreach($depots as $depot)
                <tr>

                    <td>
                        {{App\Models\Terminal::whereRaw('id=?',[$depot['terminal_id']])->first()->code}}
                    </td>

                    <td>
                        {{$depot->depot->code}}
                    </td>

                    <td>
                        {{$depot->truck_no}}
                    </td>

                    <td>
                        {{$depot->metre_ticket_no}}
                    </td>

                    <td>
                        {{$depot->product_qty}}
                    </td>

                    <td>
                        {{$depot->loading_date}}
                    </td>

                    <td>
                        {{$depot->date_of_arrival}}
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
