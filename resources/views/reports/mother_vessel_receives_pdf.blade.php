
    <div class="container">

        <style>
            table {
                border-collapse: collapse;
                width: 100%;
            }
            th, td {
                text-align: left;
                padding: 8px;
            }
            th {
                background-color: #4CAF50;
                color: white;
            }
            tr:nth-child(even){background-color: #f2f2f2}
        </style>

        <table class="table">
            <tr>
                <th scope="col">Source Type:</th>
                <td scope="col">Mother Vessel</td>
                <th scope="col">Source Name:</th>
                <td scope="col">Mother Vessel Receives</td>
            </tr>
        </table><br>

        <?php $sn=1; ?>
        <table class="table">
            <thead class="thead-light">
            <tr>
                <th scope="col">S/N</th>
                <th scope="col">Product Type</th>
                <th scope="col">Bill of Laden Number</th>
                <th scope="col">Bill of Laden Qty</th>
                <th scope="col">Out Turn Qty</th>
                <th scope="col">Arrival Date</th>
            </tr>
            </thead>
            <tbody>
            @foreach($receives as $receive)
                <tr>
                    <td  scope="col">{{$sn++}}</td>

                    <td>

                        {{$receive->type->name}}
                    </td>
                    <td>
                        {{$receive->bill_of_laden_no}}
                    </td>
                    <td>
                        {{$receive->bill_of_laden_qty}}
                    </td>
                    <td>
                        {{$receive->out_turn_qty}}
                    </td>
                    <td>
                        {{$receive->arrival_date}}
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
