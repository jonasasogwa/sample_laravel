    <div class="container">
        <table class="table table-hover">
            <thead>
            <th>Dispatch No</th>
            <th>Truck No.</th>
            <th>Product Qty.</th>
            <th>Driver Name</th>
            <th>Waybill No.</th>
            <th>Date-Time Dispatch</th>
            <th>Estimated Arrival Date</th>
            </thead>
            <tbody>
            @foreach($outlets as $outlet)
                <tr>

                    <td>
                        {{$outlet->dispatch_no}}
                    </td>

                    <td>
                        {{$outlet->truck_no}}
                    </td>

                    <td>
                        {{$outlet->product_qty}}
                    </td>

                    <td>
                        {{$outlet->driver_name}}
                    </td>

                    <td>
                        {{$outlet->product_qty}}
                    </td>

                    <td>
                        {{$outlet->waybill_no}}
                    </td>

                    <td>
                        {{$outlet->date_time_dispatch}}
                    </td>

                    <td>
                        {{$outlet->estimated_arrival_date}}
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

