@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                <form class="form-inline" method="post" action="{{ url('/') }}" >
                    {{ csrf_field() }}
                    <input type="hidden" name="getSourceTypeNames" id="getSourceTypeNames" value="{{url('/reports/getSourceTypeNames')}}">
                    <input type="hidden" name="getReport" id="getReport" value="{{url('/reports/getProductDispatchNotReceived')}}">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label for="location_id" class="col-sm-2 control-label">Source Type: </label>
                            <div class="col-sm-10 ">
                                {!! Form::select('source_type_id',['0'=> 'Select Source Type', 'mother_vessel'=> 'Mother Vessel', 'daughter_vessel' => 'Daughter Vessel', 'terminal'=>'Terminal', 'depot'=>'Depot'],null,['class' => 'source_type form-control m-b', "id" => "source_type"]) !!}
                            </div>
                        </div> <!-- /form-group-->
                        <div class="form-group row">
                            <label for="location_id" class="col-sm-2 control-label">Source Name: </label>
                            <div class="col-sm-10 sub_div">
                                {!! Form::select('source_name_id',['0'=> 'Select Name'],null,['class' => 'source_name_id form-control m-b', "id" => "source_name_id"]) !!}
                            </div>
                        </div> <!-- /form-group-->
                    </div>

                    <br/>
                    <div class="col-md-10 row" >
                        <label for="from_date">From: </label>
                        <input type="text" class="form-control" id="from_date" name="from_date">
                        <label for="to_date">To: </label>
                        <input type="text" class="form-control" id="to_date" name="to_date">
                        <button type="button" class="btn btn-primary " id="productDispatchNotReceivedBtn"> Submit</button>
                    </div>

                </form>

                {{--<div class="row pull-left">--}}
                    {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}
                        {{--<a class="btn btn-success btn-xs" href="{{ route('mother-receives-excel-file',['type'=>'xls']) }}">Download Excel xls</a>--}}
                        {{--<a class="btn btn-success btn-xs" href="{{ route('mother-receives-excel-file',['type'=>'xlsx']) }}">Download Excel xlsx</a>--}}
                        {{--<a class="btn btn-success btn-xs" href="{{ route('mother-receives-excel-file',['type'=>'csv']) }}">Download CSV</a>--}}
                        {{--<a class="btn btn-primary btn-xs" href="{{ route('mother-receives-pdf-file') }}">Download PDF</a>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <table id="example1" class="table table-hover">



                </table>
            </div>
        </div>
    </div>

    <div class="col-md-12 reports printarea" id="printarea">

    </div>
@endsection

@section('js')
    <script>
        $('#from_date').dcalendarpicker();
        $('#to_date').dcalendarpicker();
    </script>


    <script src="{{ asset('js/reports.js')}}"></script>

@stop
