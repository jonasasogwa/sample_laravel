@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                <form class="form-inline" method="post" action="{{ url('/') }}" >
                    {{ csrf_field() }}
                    <input type="hidden" name="getSourceTypeNames" id="getSourceTypeNames" value="{{url('/reports/getSourceTypeNames')}}">
                    <input type="hidden" name="getReport" id="getReport" value="{{url('/reports/getOutletWithWithoutProductReport')}}">

                    <div class="row">
                        <div class="col-xs-2">
                            <label for="from_date">Type of Report: </label>
                        </div>
                        <div class="col-xs-4">
                            {!! Form::select('report_id',['0'=> 'Select Type of Report', '1'=> 'Outlet With Product', '2' => 'Outlet Without Product'],null,['class' => 'report_id form-control m-b', "id" => "report_id"]) !!}
                        </div>
                        <div class="col-xs-2">
                            <label for="from_date">Product Type: </label>
                        </div>
                        <div class="col-xs-4">
                            {!! Form::select('type_id',$types,null,['class' => 'type_id form-control m-b', "id" => "type_id"]) !!}
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-2">
                            <label for="from_date">From: </label>
                        </div>
                        <div class="col-xs-4">
                            <input type="text" class="form-control" id="from_date" name="from_date">
                        </div>
                        <div class="col-xs-2">
                            <label for="from_date">to: </label>
                        </div>
                        <div class="col-xs-4">
                            <input type="text" class="form-control" id="to_date" name="to_date">
                        </div>
                    </div>
                    <br>
                    <div class="col-xs-2 pull-right">
                        <button type="button" class="btn btn-primary " id="outletWithWithoutProduct"> Search</button>
                    </div>

                    {{--<div class="col-md-10 row" >--}}
                        {{--<label for="from_date">From: </label>--}}
                        {{--<input type="text" class="form-control" id="from_date" name="from_date">--}}
                        {{--<label for="to_date">To: </label>--}}
                        {{--<input type="text" class="form-control" id="to_date" name="to_date">--}}
                        {{--<button type="button" class="btn btn-primary " id="outletWithWithoutProduct"> Submit</button>--}}
                    {{--</div>--}}

                </form>

                {{--<div class="row pull-left">--}}
                    {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}
                        {{--<a class="btn btn-success btn-xs" href="{{ route('mother-receives-excel-file',['type'=>'xls']) }}">Download Excel xls</a>--}}
                        {{--<a class="btn btn-success btn-xs" href="{{ route('mother-receives-excel-file',['type'=>'xlsx']) }}">Download Excel xlsx</a>--}}
                        {{--<a class="btn btn-success btn-xs" href="{{ route('mother-receives-excel-file',['type'=>'csv']) }}">Download CSV</a>--}}
                        {{--<a class="btn btn-primary btn-xs" href="{{ route('mother-receives-pdf-file') }}">Download PDF</a>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <table id="example1" class="table table-hover">



                </table>
            </div>
        </div>
    </div>


    <div class="col-md-12 reports printarea" id="printarea">

    </div>
@endsection

@section('js')
    <script>
        $('#from_date').dcalendarpicker();
        $('#to_date').dcalendarpicker();
    </script>


    <script src="{{ asset('js/reports.js')}}"></script>

@stop
