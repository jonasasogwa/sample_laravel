@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <fieldset><legend>Download Range:</legend> </fieldset>
                <form class="form-inline" method="post" action="{{ route('post_mother_vessel_dispatches') }}">
                    {{ csrf_field() }}
                    <label for="from_date">From: </label>
                    <input type="text" class="form-control" id="from_date" name="from_date">
                    <label for="to_date">To: </label>
                    <input type="text" class="form-control" id="to_date" name="to_date">
                    <label for="to_date">Report Type: </label>
                    <select name="type" id="type" class="form-control">
                        <option value="csv">Download CSV</option>
                        <option value="xls">Download Excel xls</option>
                        <option value="xlsx">Download Excel xlsx</option>
                        <option value="pdf">Download PDF</option>
                    </select>
                    <button type="submit" class="btn btn-primary"> Submit</button>
                </form>
                <fieldset><legend>Download All:</legend> </fieldset>
                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-success btn-xs" href="{{ route('mother-dispatches-excel-file',['type'=>'xls']) }}">Download Excel xls</a>
                        <a class="btn btn-success btn-xs" href="{{ route('mother-dispatches-excel-file',['type'=>'xlsx']) }}">Download Excel xlsx</a>
                        <a class="btn btn-success btn-xs" href="{{ route('mother-dispatches-excel-file',['type'=>'csv']) }}">Download CSV</a>
                        <a class="btn btn-primary btn-xs" href="{{ route('mother-dispatches-pdf-file') }}">Download PDF</a>
                    </div>
                </div>

                <table id="example1" class="table table-hover">

                    <thead>
                        <th>Product Type</th>
                        <th>Bill of Laden Number</th>
                        <th>Bill of Laden Qty</th>
                        <th>Mother Vessel</th>
                        <th>Daughter Vessel</th>
                        <th>Dispatched Date</th>
                    </thead>

                    <tbody>
                    @foreach($dispatches as $dispatch)
                        <tr>
                            <td>
                                <?php if(isset($dispatch->type)): ?>
                                {{$dispatch->type->name}}
                                <?php endif; ?>
                            </td>

                            <td>
                                {{$dispatch->bill_of_laden_no}}
                            </td>

                            <td>
                                {{$dispatch->bill_of_laden_qty}}
                            </td>

                            <td>
                                {{App\Models\MotherVessel::whereRaw('id=?',[$dispatch['mother_vessel_id']])->first()->vessel_no}}
                            </td>

                            <td>
                                {{App\Models\Daughter_vessel::whereRaw('id=?',[$dispatch['daughter_vessel_id']])->first()->number}}
                            </td>

                            <td>
                                {{$dispatch->dispatch_date}}
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#from_date').dcalendarpicker();
        $('#to_date').dcalendarpicker();
    </script>
@stop
