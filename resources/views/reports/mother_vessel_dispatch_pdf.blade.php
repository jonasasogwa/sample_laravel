
<div class="container">

    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            text-align: left;
            padding: 8px;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }
        tr:nth-child(even){background-color: #f2f2f2}
    </style>

    <table class="table">
        <tr>
            <th scope="col">Source Type:</th>
            <td scope="col">Mother Vessel</td>
            <th scope="col">Source Name:</th>
            <td scope="col">Mother Vessel Dispatches</td>
        </tr>
    </table><br>

    <?php $sn=1; ?>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">S/N</th>
                <th scope="col">Product Type</th>
                <th scope="col">Bill of Laden Number</th>
                <th scope="col">Bill of Laden Qty</th>
                <th scope="col">Mother Vessel</th>
                <th scope="col">Daughter Vessel</th>
                <th scope="col">Dispatched Date</th>
            </tr>
        </thead>
        <tbody>
        @foreach($dispatches as $dispatch)
            <tr>
                <td  scope="col">{{$sn++}}</td>

                <td>
                    <?php if(isset($dispatch->type)): ?>
                    {{$dispatch->type->name}}
                    <?php endif; ?>
                </td>

                <td>
                    {{$dispatch->bill_of_laden_no}}
                </td>

                <td>
                    {{$dispatch->bill_of_laden_qty}}
                </td>

                <td>
                    {{App\Models\MotherVessel::whereRaw('id=?',[$dispatch['mother_vessel_id']])->first()->vessel_no}}
                </td>

                <td>
                    {{App\Models\Daughter_vessel::whereRaw('id=?',[$dispatch['daughter_vessel_id']])->first()->number}}
                </td>

                <td>
                    {{$dispatch->dispatch_date}}
                </td>

            </tr>
            @endforeach
        </tbody>
    </table>
</div>
