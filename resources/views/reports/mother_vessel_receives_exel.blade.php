
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <a href="{{ route('mother-receives-excel-file',['type'=>'xls']) }}">Download Excel xls</a> |
            <a href="{{ route('mother-receives-excel-file',['type'=>'xlsx']) }}">Download Excel xlsx</a> |
            <a href="{{ route('mother-receives-excel-file',['type'=>'csv']) }}">Download CSV</a> |
            <a href="{{ route('mother-receives-pdf-file') }}">Download PDF</a>
        </div>
    </div>

    <table id="example1" class="table table-hover">

        <thead>
        <th>Product Type</th>
        <th>Bill of Laden Number</th>
        <th>Bill of Laden Qty</th>
        <th>Out Turn Qty</th>
        <th>Arrival Date</th>
        </thead>

        <tbody>
        @foreach($receives as $receive)
        <tr>
            <td>
                {{$receive->type->name}}
            </td>
            <td>
                {{$receive->bill_of_laden_no}}
            </td>
            <td>
                {{$receive->bill_of_laden_qty}}
            </td>
            <td>
                {{$receive->out_turn_qty}}
            </td>
            <td>
                {{$receive->arrival_date}}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

</div>

