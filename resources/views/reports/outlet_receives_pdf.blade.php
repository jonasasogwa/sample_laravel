<div class="container">

    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            text-align: left;
            padding: 8px;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }
        tr:nth-child(even){background-color: #f2f2f2}
    </style>

    <table class="table">
        <tr>
            <th scope="col">Source Type:</th>
            <td scope="col">OutLet</td>
            <th scope="col">Source Name:</th>
            <td scope="col">Outlet Receives</td>
        </tr>
    </table><br>

    <?php $sn=1; ?>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">S/N</th>
            <th scope="col">Dispatch No</th>
            <th scope="col">Truck No.</th>
            <th scope="col">Product Qty.</th>
            <th scope="col">Driver Name</th>
            <th scope="col">Waybill No.</th>
            <th scope="col">Date-Time Dispatch</th>
            <th scope="col">Estimated Arrival Date</th>
        </tr>
        </thead>
        <tbody>
        @foreach($outlets as $outlet)
            <tr>
                <td  scope="col">{{$sn++}}</td>

                <td>
                    {{$outlet->dispatch_no}}
                </td>

                <td>
                    {{$outlet->truck_no}}
                </td>

                <td>
                    {{$outlet->product_qty}}
                </td>

                <td>
                    {{$outlet->driver_name}}
                </td>

                <td>
                    {{$outlet->product_qty}}
                </td>

                <td>
                    {{$outlet->waybill_no}}
                </td>

                <td>
                    {{$outlet->date_time_dispatch}}
                </td>

                <td>
                    {{$outlet->estimated_arrival_date}}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

</div>