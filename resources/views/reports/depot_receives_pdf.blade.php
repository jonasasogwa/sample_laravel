
<div class="container">

    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            text-align: left;
            padding: 8px;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }
        tr:nth-child(even){background-color: #f2f2f2}
    </style>

    <table class="table">
        <tr>
            <th scope="col">Source Type:</th>
            <td scope="col">Depot</td>
            <th scope="col">Source Name:</th>
            <td scope="col">Depot Receives</td>
        </tr>
    </table><br>

    <?php $sn=1; ?>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">S/N</th>
                <th scope="col">Terminal</th>
                <th scope="col">Depot</th>
                <th scope="col">Truck No.</th>
                <th scope="col">Metre Ticket No</th>
                <th scope="col">Product Qty</th>
                <th scope="col">Loading Date</th>
                <th scope="col">Arrival Date</th>
            </tr>
        </thead>
        <tbody>
            @foreach($depots as $depot)
                <tr>
                    <td  scope="col">{{$sn++}}</td>

                    <td scope="row">
                        {{App\Models\Terminal::whereRaw('id=?',[$depot['terminal_id']])->first()->code}}
                    </td>

                    <td>
                        {{$depot->depot->code}}
                    </td>

                    <td>
                        {{$depot->truck_no}}
                    </td>

                    <td>
                        {{$depot->metre_ticket_no}}
                    </td>

                    <td>
                        {{$depot->product_qty}}
                    </td>

                    <td>
                        {{$depot->loading_date}}
                    </td>

                    <td>
                        {{$depot->date_of_arrival}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

</div>
