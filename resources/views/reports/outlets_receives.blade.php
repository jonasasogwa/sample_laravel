@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <fieldset><legend>Download Range:</legend> </fieldset>
                <form class="form-inline" method="post" action="{{ route('post_outlet_receives') }}" >
                    {{ csrf_field() }}
                    <label for="from_date">From: </label>
                    <input type="text" class="form-control" id="from_date" name="from_date">
                    <label for="to_date">To: </label>
                    <input type="text" class="form-control" id="to_date" name="to_date">
                    {{--<label for="to_date">Report Type: </label>--}}
                    {{--<select name="type" id="type" class="form-control">--}}
                        {{--<option value="csv">Download CSV</option>--}}
                        {{--<option value="xls">Download Excel xls</option>--}}
                        {{--<option value="xlsx">Download Excel xlsx</option>--}}
                        {{--<option value="pdf">Download PDF</option>--}}
                    {{--</select>--}}
                    <button type="submit" class="btn btn-primary"> Submit</button>
                </form>
                <fieldset><legend>Download All:</legend> </fieldset>
                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-success btn-xs" href="{{ route('outlet-receives-excel-file',['type'=>'xls']) }}">Download Excel xls</a>
                        <a class="btn btn-success btn-xs" href="{{ route('outlet-receives-excel-file',['type'=>'xlsx']) }}">Download Excel xlsx</a>
                        <a class="btn btn-success btn-xs" href="{{ route('outlet-receives-excel-file',['type'=>'csv']) }}">Download CSV</a>
                        <a class="btn btn-primary btn-xs" href="{{ route('outlet-receives-pdf-file') }}">Download PDF</a>
                    </div>
                </div>

                <table class="table table-hover">
                    <thead>
                    <th>S/N</th>
                    <th>Dispatch No</th>
                    <th>Truck No.</th>
                    <th>Product Qty.</th>
                    <th>Driver Name</th>
                    <th>Waybill No.</th>
                    <th>Date-Time Dispatch</th>
                    <th>Estimated Arrival Date</th>
                    </thead>
                    <?php $sn=1;?>
                    <tbody>
                    @foreach($outlets as $outlet)
                        <tr>
                            <td>{{$sn++}}</td>
                            <td>
                                {{$outlet->dispatch_no}}
                            </td>

                            <td>
                                {{$outlet->truck_no}}
                            </td>

                            <td>
                                {{$outlet->product_qty}}
                            </td>

                            <td>
                                {{$outlet->driver_name}}
                            </td>

                            <td>
                                {{$outlet->product_qty}}
                            </td>

                            <td>
                                {{$outlet->waybill_no}}
                            </td>

                            <td>
                                {{$outlet->date_time_dispatch}}
                            </td>

                            <td>
                                {{$outlet->estimated_arrival_date}}
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        {{ $outlets->links() }}
                    </ul>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#from_date').dcalendarpicker();
        $('#to_date').dcalendarpicker();
    </script>
@stop
