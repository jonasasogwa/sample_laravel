@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <fieldset><legend>Download Range:</legend> </fieldset>
                <form class="form-inline" method="post" action="{{ route('post_depot_receives') }}" >
                    {{ csrf_field() }}
                    <label for="from_date">From: </label>
                    <input type="text" class="form-control" id="from_date" name="from_date">
                    <label for="to_date">To: </label>
                    <input type="text" class="form-control" id="to_date" name="to_date">
                    {{--<label for="to_date">Report Type: </label>--}}
                    {{--<select name="type" id="type" class="form-control">--}}
                        {{--<option value="csv">Download CSV</option>--}}
                        {{--<option value="xls">Download Excel xls</option>--}}
                        {{--<option value="xlsx">Download Excel xlsx</option>--}}
                        {{--<option value="pdf">Download PDF</option>--}}
                    {{--</select>--}}
                    <button type="submit" class="btn btn-primary"> Submit</button>
                </form>
                <fieldset><legend>Download Range:</legend> </fieldset>
                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-success btn-xs" href="{{ route('depot-receives-excel-file',['type'=>'xls']) }}">Download Excel xls</a> 
                        <a class="btn btn-success btn-xs" href="{{ route('depot-receives-excel-file',['type'=>'xlsx']) }}">Download Excel xlsx</a>
                        <a class="btn btn-success btn-xs" href="{{ route('depot-receives-excel-file',['type'=>'csv']) }}">Download CSV</a>
                        <a class="btn btn-primary btn-xs" href="{{ route('depot-receives-pdf-file') }}">Download PDF</a>
                    </div>
                </div>

                <table id="example1" class="table table-hover">
                    <thead>
                    <th>S/N</th>
                    <th>Terminal Code</th>
                    <th>Depot Code</th>
                    <th>Truck No.</th>
                    <th>Metre Ticket No</th>
                    <th>Product Qty</th>
                    <th>Loading Date</th>
                    <th>Arrival Date</th>
                    </thead>
                    <?php $sn=1;?>
                    <tbody>
                    @foreach($depots as $depot)
                        <tr>
                            <td>{{$sn++}}</td>
                            <td>
                                {{App\Models\Terminal::whereRaw('id=?',[$depot['terminal_id']])->first()->code}}
                            </td>

                            <td>
                                {{$depot->depot->code}}
                            </td>

                            <td>
                                {{$depot->truck_no}}
                            </td>

                            <td>
                                {{$depot->metre_ticket_no}}
                            </td>

                            <td>
                                {{$depot->product_qty}}
                            </td>

                            <td>
                                {{$depot->loading_date}}
                            </td>

                            <td>
                                {{$depot->date_of_arrival}}
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        {{ $depots->links() }}
                    </ul>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#from_date').dcalendarpicker();
        $('#to_date').dcalendarpicker();
    </script>
@stop
