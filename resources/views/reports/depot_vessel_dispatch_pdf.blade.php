
<div class="container">

    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            text-align: left;
            padding: 8px;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }
        tr:nth-child(even){background-color: #f2f2f2}
    </style>

    <table class="table">
        <tr>
            <th scope="col">Source Type:</th>
            <td scope="col">Depot</td>
            <th scope="col">Source Name:</th>
            <td scope="col">Depot Dispatches</td>
        </tr>
    </table><br>

    <?php $sn=1; ?>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">S/N</th>
            <th scope="col">Depot</th>
            <th scope="col">OutLet</th>
            <th scope="col">Truck No.</th>
            <th scope="col">Metre Ticket No</th>
            <th scope="col">Product Qty</th>
            <th scope="col">WayBill Qty</th>
            <th scope="col">Loading Date</th>
        </tr>
        </thead>
        <tbody>
        @foreach($depots as $depot)
            <tr>
                <td  scope="col">{{$sn++}}</td>

                <td scope="row">
                    {{$depot->depot_id}}
                </td>

                <td>
                    {{$depot->retail_outlet_id}}
                </td>

                <td>
                    {{$depot->truck_number}}
                </td>

                <td>
                    {{$depot->meter_ticket_number}}
                </td>

                <td>
                    {{$depot->product_qty}}
                </td>

                <td>
                    {{$depot->waybill_qty}}
                </td>

                <td>
                    {{$depot->loading_date}}
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>

</div>

