
    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <a href="{{ route('excel-file',['type'=>'xls']) }}">Download Excel xls</a> |
                <a href="{{ route('excel-file',['type'=>'xlsx']) }}">Download Excel xlsx</a> |
                <a href="{{ route('excel-file',['type'=>'csv']) }}">Download CSV</a>
            </div>
        </div>

        <table id="example1" class="table table-hover">
            <thead>
            <th>Mother Vessel </th>
            <th>Product Type</th>
            <th>Bill of Laden Qty</th>
            <th>Loading Date</th>
            </thead>
            <tbody>
            @foreach($daughters as $daughter)
                <tr>
                    <td>
                        {{ $daughter->mother_vessel->vessel_no }}
                    </td>
                    <td>
                    {{ $daughter->type->name }}
                    </td>
                    <td>
                        {{ $daughter->bill_of_laden_qty }}
                    </td>
                    <td>
                        {{ $daughter->loading_date }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

