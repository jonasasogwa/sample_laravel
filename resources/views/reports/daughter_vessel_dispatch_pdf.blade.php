<div class="container">

    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            text-align: left;
            padding: 8px;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }
        tr:nth-child(even){background-color: #f2f2f2}
    </style>
    <table class="table">
        <tr>
            <th scope="col">Source Type:</th>
            <td scope="col">Daughter Vessel</td>
            <th scope="col">Source Name:</th>
            <td scope="col">Daughter Vessel Dispatch</td>
        </tr>
    </table><br>


    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">Terminal</th>
                <th scope="col">Product Type</th>
                <th scope="col">Bill of Laden Qty</th>
                <th scope="col">Qty Dispatched</th>
                <th scope="col">Daughter Vessel</th>
                <th scope="col">Dispatched Date</th>
            </tr>
        </thead>
        <tbody>
        @foreach($dispatches as $dispatch)
            <tr>
                <td scope="row">
                    <?php if(isset($dispatch->terminal)): ?>
                    {{$dispatch->terminal->name}}
                    <?php endif; ?>
                </td>
                <td>
                    <?php if(isset($dispatch->type)): ?>
                    {{$dispatch->type->name}}
                    <?php endif; ?>
                </td>
                <td>
                    {{$dispatch->bill_of_laden_qty}}
                </td>
                <td>
                    {{$dispatch->qty_dispatched}}
                </td>
                <td>
                    {{$dispatch->daughter_vessel_id}}
                </td>
                <td>
                    {{$dispatch->dispatch_date}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>

