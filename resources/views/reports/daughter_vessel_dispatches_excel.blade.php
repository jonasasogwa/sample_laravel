    <div class="container">

        <table id="example1" class="table table-hover">

            <thead>
            <th>Terminal</th>
            <th>Product Type</th>
            <th>Bill of Laden Qty</th>
            <th>Qty Dispatched</th>
            <th>Daughter Vessel</th>
            <th>Dispatched Date</th>
            </thead>

            <tbody>
            @foreach($dispatches as $dispatch)
                <tr>

                    <td>
                        <?php if(isset($dispatch->terminal)): ?>
                        {{$dispatch->terminal->name}}
                        <?php endif; ?>
                    </td>

                    <td>
                        <?php if(isset($dispatch->type)): ?>
                        {{$dispatch->type->name}}
                        <?php endif; ?>
                    </td>

                    <td>
                        {{$dispatch->bill_of_laden_qty}}
                    </td>

                    <td>
                        {{$dispatch->qty_dispatched}}
                    </td>

                    <td>
                        {{$dispatch->daughter_vessel_id}}
                    </td>

                    <td>
                        {{$dispatch->dispatch_date}}
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

