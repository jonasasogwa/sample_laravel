
    <div class="container">

        <table id="example1" class="table table-hover">
            <thead>
            <th>Depot</th>
            <th>OutLet</th>
            <th>Truck No.</th>
            <th>Metre Ticket No</th>
            <th>Product Qty</th>
            <th>WayBill Qty</th>
            <th>Loading Date</th>
            </thead>
            <tbody>
            @foreach($depots as $depot)
                <tr>

                    <td>
                        {{$depot->depot_id}}
                    </td>

                    <td>
                        {{$depot->retail_outlet_id}}
                    </td>

                    <td>
                        {{$depot->truck_number}}
                    </td>

                    <td>
                        {{$depot->meter_ticket_number}}
                    </td>

                    <td>
                        {{$depot->product_qty}}
                    </td>

                    <td>
                        {{$depot->waybill_qty}}
                    </td>

                    <td>
                        {{$depot->loading_date}}
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

