
    <div class="container">

        <table id="example1" class="table table-hover">
            <thead>
            <th>Product Type</th>
            <th>Terminal</th>
            <th>Quantity Dispatched</th>
            <th>Bill Date</th>
            </thead>
            <tbody>
            @foreach($terminals as $terminal)
                <tr>

                    <td>
                        {{$terminal->type->name}}
                    </td>

                    <td>
                        {{$terminal->terminal->name}}
                    </td>

                    <td>
                        {{$terminal->qty_dispatch}}
                    </td>

                    <td>
                        {{$terminal->bill_date}}
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
