@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                <fieldset><legend>Download Range:</legend> </fieldset>
                <form class="form-inline" method="post" action="{{ route('post_terminal_receives') }}" >
                    {{ csrf_field() }}
                    <label for="from_date">From: </label>
                    <input type="text" class="form-control" id="from_date" name="from_date">
                    <label for="to_date">To: </label>
                    <input type="text" class="form-control" id="to_date" name="to_date">
                    {{--<label for="to_date">Report Type: </label>--}}
                    {{--<select name="type" id="type" class="form-control">--}}
                        {{--<option value="csv">Download CSV</option>--}}
                        {{--<option value="xls">Download Excel xls</option>--}}
                        {{--<option value="xlsx">Download Excel xlsx</option>--}}
                        {{--<option value="pdf">Download PDF</option>--}}
                    {{--</select>--}}
                    <button type="submit" class="btn btn-primary"> Submit</button>
                </form>
                <fieldset><legend>Download All:</legend> </fieldset>
                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-success btn-xs" href="{{ route('terminal-receives-excel-file',['type'=>'xls']) }}">Download Excel xls</a>
                        <a class="btn btn-success btn-xs" href="{{ route('terminal-receives-excel-file',['type'=>'xlsx']) }}">Download Excel xlsx</a>
                        <a class="btn btn-success btn-xs" href="{{ route('terminal-receives-excel-file',['type'=>'csv']) }}">Download CSV</a>
                        <a class="btn btn-primary btn-xs" href="{{ route('terminal-receives-pdf-file') }}">Download PDF</a>
                    </div>
                </div>

                <table id="example1" class="table table-hover">
                    <thead>
                        <th>S/N</th>
                        <th>Product Type</th>
                        <th>Terminal</th>
                        <th>Quantity Dispatched</th>
                        <th>Bill Date</th>
                    </thead>
                    <?php $sn=1;?>
                    <tbody>
                        @foreach($terminals as $terminal)
                            <tr>

                                <td>{{$sn++}}</td>

                                <td>
                                    {{$terminal->type->name}}
                                </td>

                                <td>
                                    {{$terminal->terminal->name}}
                                </td>

                                <td>
                                    {{$terminal->qty_dispatch}}
                                </td>

                                <td>
                                    {{$terminal->bill_date}}
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        {{ $terminals->links() }}
                    </ul>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#from_date').dcalendarpicker();
        $('#to_date').dcalendarpicker();
    </script>
@stop
