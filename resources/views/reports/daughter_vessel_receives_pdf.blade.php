
<div class="container">

    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            text-align: left;
            padding: 8px;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }
        tr:nth-child(even){background-color: #f2f2f2}
    </style>

    <table class="table">
        <tr>
            <th scope="col">Source Type:</th>
            <td scope="col">Daughter Vessel</td>
            <th scope="col">Source Name:</th>
            <td scope="col">Daughter Vessel Receives</td>
        </tr>
    </table><br>

    <?php $sn=1; ?>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">S/N</th>
            <th scope="col">Mother Vessel No</th>
            <th scope="col">Product Type</th>
            <th scope="col">Bill of Laden Qty</th>
            <th scope="col">Loading Date</th>
        </tr>
        </thead>
        <tbody>
            @foreach($daughters as $daughter)
                <tr>
                    <td  scope="col">{{$sn++}}</td>
                    <td scope="row">
                        {{ $daughter->mother_vessel->vessel_no }}
                    </td>
                    <td>
                        {{ $daughter->type->name }}
                    </td>
                    <td>
                        {{ $daughter->bill_of_laden_qty }}
                    </td>
                    <td>
                        {{ $daughter->loading_date }}
                    </td>

                </tr>
            @endforeach
        </tbody>
    </table>

</div>

