
    <div class="container">
        <table id="example1" class="table table-hover">
            <thead>
            <th>Mother Vessel No</th>
            <th>Product Type</th>
            <th>Bill of Laden Qty</th>
            <th>Loading Date</th>
            </thead>
            <tbody>
            @foreach($daughters as $daughter)
                <tr>
                    <td>
                        {{ $daughter->mother_vessel->vessel_no }}
                    </td>
                    <td>
                    {{ $daughter->type->name }}
                    </td>
                    <td>
                        {{ $daughter->bill_of_laden_qty }}
                    </td>
                    <td>
                        {{ $daughter->loading_date }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

