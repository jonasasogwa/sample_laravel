@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                @include('flash::message')
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>Add Marketer</h3>

                        </div>
                        {{--@include('includes.errors')--}}
                        <div class="panel-body">
                            <form action="{{ route('add_marketer') }}" id="marketer_add" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" class="form-control">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong style="color: red;">{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <input type="text" name="first_name" class="form-control">
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                            <strong style="color: red;">{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" name="last_name" class="form-control">
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong style="color: red;">{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input type="text" name="phone" class="form-control">
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong style="color: red;">{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" class="form-control">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong style="color: red;">{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <textarea name="address" id="address" cols="3" rows="3" class="form-control"></textarea>
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong style="color: red;">{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="status_id">Status</label>
                                    <select name="status_id" id="status_id" class="form-control" required>
                                        <option value="1">Active</option>
                                        <option value="0">Not Active</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <div class="pull-right">
                                        <button class="btn btn-success" id="marketer" type="submit" >Save</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
            </div>

        </div>

    </div>

@endsection
@section('js')
    <script src="{{ asset('custom/js/marketer.js')}}"></script>
@stop

