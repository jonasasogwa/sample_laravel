@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                @include('flash::message')
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>Update Marketer</h3>
                        </div>
                        {{--@include('includes.errors')--}}
                        <div class="panel-body">
                            <form action="{{ route('marketer.update_marketer', ['id' => $marketer->id]) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="hidden" name="id" value="{{ $marketer->id }}" class="form-control">
                                    <input type="email" name="email" value="{{ $marketer->email }}" class="form-control">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong style="color: red;">{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <input type="text" name="first_name" value="{{ $marketer->first_name }}" class="form-control">
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                            <strong style="color: red;">{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" name="last_name" value="{{ $marketer->last_name }}" class="form-control">
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong style="color: red;">{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input type="text" name="phone" value="{{ $marketer->phone }}" class="form-control">
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong style="color: red;">{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <textarea name="address" id="address" cols="3" rows="3" class="form-control">
                                        {{$marketer->address }}
                                    </textarea>
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong style="color: red;">{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="status_id">Status</label>
                                    <select name="status_id" id="status_id" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Not Active</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <div class="pull-right">
                                        <button class="btn btn-success" type="submit">Save</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
            </div>

        </div>

    </div>

@endsection
