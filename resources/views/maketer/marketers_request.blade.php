@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-default btn-xs" href="{{ route('excel-file_req',['type'=>'xls']) }}">Download Excel xls</a>
                        <a class="btn btn-default btn-xs" href="{{ route('excel-file_req',['type'=>'xlsx']) }}">Download Excel xlsx</a>
                        <a class="btn btn-default btn-xs" href="{{ route('excel-file_req',['type'=>'csv']) }}">Download CSV</a>
                    </div>
                </div>

                <table class="table table-hover">
                    <thead>
                    <th>CODE</th>
                    <th>NAME</th>
                    <th>PRODUCT NAME</th>
                    <th>DESCRIPTION</th>
                    <th>SETTINGS</th>
                    </thead>
                    <tbody>
                    @foreach($maketers as $maketer)
                        <tr>
                            <td>
                                {{$maketer->code}}
                            </td>
                            <td>
                                {{$maketer->first_name}} {{$maketer->last_name}}
                            </td>
                            <td>
                                {{$maketer->name}}
                            </td>
                            <td>
                                {{$maketer->description}}
                            </td>
                            <td>
                                <a href="#" class="btn btn-xs btn-info "> view </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>

        </div>

    </div>
@endsection

@section('js')
    <script src="{{ asset('custom/js/sales.js')}}"></script>
@stop
