@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                @include('flash::message')
                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-default btn-xs" href="{{ route('excel-file-m',['type'=>'xls']) }}">Download Excel xls</a>
                        <a class="btn btn-default btn-xs" href="{{ route('excel-file-m',['type'=>'xlsx']) }}">Download Excel xlsx</a>
                        <a class="btn btn-default btn-xs" href="{{ route('excel-file-m',['type'=>'csv']) }}">Download CSV</a>
                    </div>
                </div>

                <table class="table table-hover">
                    <thead>
                    <th>S/N</th>
                    <th>CODE</th>
                    <th>NAME</th>
                    <th>PHONE</th>
                    <th>EMAIL</th>
                    <th>SETTINGS</th>
                    </thead>
                    <tbody>
                    <?php $sn = 1 ?>
                    @foreach($maketers as $maketer)
                        <tr>

                            <td>
                                {{$sn++}}
                            </td>

                            <td>
                                <?php ?>
                                <?php if(isset($maketer->marketers->code)): ?>
                                {{ $maketer->marketers->code }}
                                <?php endif; ?>
                            </td>

                            <td>
                                <?php if(isset($maketer->user_profile)): ?>
                                {{ $maketer->user_profile->last_name }} {{ $maketer->user_profile->first_name }}
                                <?php endif; ?>
                            </td>

                            <td>
                                <?php if(isset($maketer->user_profile)): ?>
                                {{ $maketer->user_profile->phone }}
                                    <?php endif; ?>
                            </td>

                            <td>
                                {{ $maketer->email }}
                            </td>

                            <td>
                                {{--<a href="#" class="btn btn-xs btn-info "> view </a>--}}
                                <a href="{{ route('marketer.edit_marketer',['id' => $maketer->id]) }}"
                                   class="btn btn-xs btn-warning "> edit </a>

                                <a href="{{ route('marketer.delete',['id' => $maketer->id]) }}" class="btn btn-xs btn-danger ">
                                    trash </a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        {{ $maketers->links() }}
                    </ul>
                </div>

                {{--<h1>Upload Marketers Records from Excel File</h1>--}}
                {{--{!! Form::open(array('route' => 'upload_excel','method'=>'POST','files'=>'true')) !!}--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-4 col-sm-4 col-md-4">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-9">--}}
                                {{--{!! Form::file('sample_file', array('class' => 'form-control' )) !!}--}}
                                {{--{!! $errors->first('sample_file', '<p class="alert alert-danger">:message</p>') !!}--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-3 col-sm-3 col-md-3 text-center">--}}
                        {{--{!! Form::submit('Upload',['class'=>'btn btn-primary']) !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--{!! Form::close() !!}--}}

            </div>

            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-7">
                    <div class="box box-solid">
                        <div class="box-header with-border">

                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="box-group" id="accordion">
                                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                <div class="panel box box-primary">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" >
                                                Upload Marketers Records from Excel File

                                                    <a  href="{{asset('custom/downloadable/marketers.csv')}}">
                                                        <code class="btn btn-xs btn-info">Download Template</code>
                                                    </a>

                                            </a>
                                        </h4>
                                    </div>
                                    <div id="" class="panel-collapse  in">
                                        <div class="box-body">

                                            {!! Form::open(array('route' => 'upload_excel','method'=>'POST','files'=>'true')) !!}
                                                <br>
                                                <div class="form-group has-feedback">
                                                    {!! Form::file('sample_file', array('class' => '' )) !!}
                                                    {!! $errors->first('sample_file', '<p class="alert alert-danger">:message</p>') !!}
                                                    <span class="fa fa-upload form-control-feedback"></span>
                                                </div>

                                                <div class="row">
                                                    <div class="col-xs-4 pull-right">
                                                        {!! Form::submit('Upload',['class'=>'btn btn-primary btn-block btn-flat']) !!}

                                                    </div>
                                                </div>
                                            {!! Form::close() !!}

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>


        </div>

    </div>
@endsection

@section('js')
    <script src="{{ asset('custom/js/sales.js')}}"></script>
@stop
