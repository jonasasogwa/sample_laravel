@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <span>Old values</span>
                <section class="panel panel-default">
                    {{--<header class="panel-heading">
                        <span class="label bg-danger pull-right">4 left</span> Tasks
                    </header>--}}

                    <table class="table table-responsive">
                        
                        <thead>
                            <tr>
                                <th>Key</th>
                                <th>Value</th>
                                
                            </tr>
                        </thead>
                        <tbody id="terms">
                           
                        {{--  <tr>
                            
                            @foreach($old as $key => $value)
                            <td>{{$key}}</td>
                            </td><td>{{ $value}}</td>
                            
                        </tr>

                           @endforeach   --}}
                          
                        </tbody>
                    </table>
                    
                </section>
                <span>New values</span>
                <section class="panel panel-default">
                    {{--<header class="panel-heading">
                        <span class="label bg-danger pull-right">4 left</span> Tasks
                    </header>--}}
                    <table class="table table-responsive">
                        
                        <thead>
                            <tr>
                                <th>Key</th>
                                <th>Value</th>
                                
                            </tr>
                        </thead>
                        <tbody id="terms">
                           
                        <tr>
                            
                            @foreach($arr as $key => $value)
                            <td>{{$key}}</td>
                            </td><td>{{ $value}}</td>
                            
                        </tr>

                           @endforeach 
                          
                        </tbody>
                    </table>
                    
                </section>
                <section class="panel panel-default">
                    {{--<header class="panel-heading">
                        <span class="label bg-danger pull-right">4 left</span> Tasks
                    </header>--}}

                    <table class="table table-responsive">
                        
                        <thead>
                            <tr>
                                
                                <th></th>
                                
                            </tr>
                        </thead>
                        <tbody id="terms">
                           
                        <tr>
                            
                           
                            <td><strong>Url</strong></td><td>{{ $log->url}}</td>
                            
                            
                        </tr>

                           
                          
                        </tbody>
                    </table>
                    
                </section>
            </div>
   
@endsection
