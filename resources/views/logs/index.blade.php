@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12"></div>
                </div>
                <table class="table table-hover">
                    <thead>
                        <th></th>
                        <th>Name</th>
                        <th>Event</th>
                        <th>Action</th>
                        <th></th>
                        <th>Model</th>
                        <th></th>
                        <th>Date</th>
                    </thead>
                    <tbody>
                    @foreach($logs as $log)
                        <tr>
                            <td>{{$log->id}}</td>
                            <td><?php echo ($log->users) ? $log->users->email : $log->user_id   ?></td>
                            <td>
                                {{ $log->event}}
                            </td>
                            <td>Data</td>
                            <td>On</td>
                            <td>{{$log->auditable_type}}</td>
                            <td>On</td>
                            <td>{{ date('M j, Y h:ia', strtotime($log->created_at)) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    {{$logs->links()}}
                    <div class="text-center">
                        Showing {{$logs->currentPage()}} of {{ $logs->lastPage()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
   
@endsection
