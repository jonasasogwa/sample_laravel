@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="panel panel-default">

                    <div class="panel-body">
                        {!! Form::open(['url' => '/change-password', 'id' => "AuditorForm", 'class'=>'form-horizontal']) !!}

                        <legend>Change Password </legend>

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
    


                            <div class="form-group">
                                <label for="password" class="col-sm-3 control-label">Old Password: </label>
                                <label class="col-sm-1 control-label">: </label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="password" placeholder="Employee Password"
                                           name="old_password" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password" class="col-sm-3 control-label">New Password: </label>
                                <label class="col-sm-1 control-label">: </label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="password" placeholder="Employee Password"
                                           name="new_password" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="confirm_password" class="col-sm-3 control-label">Confirm New Password: </label>
                                <label class="col-sm-1 control-label">: </label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="confirm_password"
                                           placeholder="Confirm Password" name="confirm_new_password" autocomplete="off">
                                </div>
                            </div> <!-- /form-group-->

                            <div class="messages"></div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-success" data-loading-text="Loading..." id="companyBtn"> <i class="glyphicon glyphicon-ok-sign"></i>  Save Record </button>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

@endsection
