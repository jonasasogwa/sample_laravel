@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">


                <fieldset><legend>Download Range:</legend> </fieldset>
                <form class="form-inline" method="post" action="{{ route('sort_users_post') }}">
                    {{ csrf_field() }}
                    <label for="to_date">Report Type: </label>
                    <select name="role" id="role" class="form-control">
                        @foreach($roles as $role)
                        <option value="{{$role->name}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                    <button type="submit" name="sort" class="btn btn-primary"> Sort</button>
                </form>
                <fieldset><legend></legend> </fieldset>


                <table class="table table-hover">
                    <thead>
                        <th>S/N</th>
                        <th>V-Code</th>
                        <th>Email</th>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th>Setting</th>
                    </thead>

                    <tbody>

                    <?php $sn = 1; ?>
                    <?php if(isset($_POST['sort'])){?>
                    @foreach($users as $user)
                        <tr>

                            <td>
                                {{$sn++}}
                            </td>

                            <td>
                                {{$user->verification_code}}
                            </td>

                            <td>
                                {{$user->email}}
                            </td>

                            <td>
                                <?php if(isset($user->user_profile)): ?>
                                {{$user->user_profile->first_name}} {{$user->user_profile->last_name}}
                                <?php endif; ?>
                            </td>
                            <td>
                                @foreach($user->roles as $use)
                                    {{$use->name}}
                                @endforeach
                            </td>
                            <td>
                                <?php if(isset($user->status)): ?>
                                {{$user->status->name}}
                                <?php endif; ?>
                            </td>
                            <td>
                                <a href="{{ route('user_status',['id' => $user->id]) }}"
                                   class="btn btn-xs btn-warning ">status</a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        {{ $users->links() }}
                    </ul>
                </div>
                <?php }?>
            </div>

        </div>

    </div>

@endsection
