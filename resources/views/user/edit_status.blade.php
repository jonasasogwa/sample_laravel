@extends('layouts.app1')

@section('content')
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                @include('flash::message')

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>Update Status For {{ $user->first_name}} {{ $user->last_name }}</h3>
                        </div>
                        {{--@include('includes.errors')--}}
                        <div class="panel-body">

                            <form action="{{ route('update_status', ['id' => $user->id]) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group">

                                    <label for="status_id">Status</label>
                                    <select name="status_id" id="status_id" class="form-control">
                                        <option value="">Select Status</option>
                                        @foreach($status as $statu)
                                            <option value="{{$statu->id}}">{{$statu->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <div class="pull-right">
                                        <button class="btn btn-success" type="submit">Save</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>

            </div>

        </div>

    </div>
@endsection
