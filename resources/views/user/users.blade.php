@extends('layouts.app1')

@section('content')
    <div class="col-sm-12 yscroll-container">

        <div class="box yscroll-left">
            <!-- /.box-header -->
            <div class="box-body">

                <div class="row">
                    {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}
                        {{--<a href="{{ route('excel-file',['type'=>'xls']) }}">Download Excel xls</a> |--}}
                        {{--<a href="{{ route('excel-file',['type'=>'xlsx']) }}">Download Excel xlsx</a> |--}}
                        {{--<a href="{{ route('excel-file',['type'=>'csv']) }}">Download CSV</a>--}}
                    {{--</div>--}}
                </div>

                <table class="table table-hover">
                    <thead>
                    <th>S/N</th>
                    <th>V-Code</th>
                    <th>Email</th>
                    <th>Name</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th>Setting</th>
                    </thead>
                    <tbody>

                    <?php $sn = 1; ?>

                    @foreach($users as $user)
                        <tr>

                            <td>
                                {{$sn++}}
                            </td>

                            <td>
                                {{$user->verification_code}}
                            </td>

                            <td>
                                {{$user->email}}
                            </td>

                            <td>
                                <?php if(isset($user->user_profile)): ?>
                                {{$user->user_profile->first_name}} {{$user->user_profile->last_name}}
                                <?php endif; ?>
                            </td>
                            <td>
                                @foreach($user->roles as $use)
                                    {{$use->name}}
                                @endforeach
                            </td>
                            <td>
                                <?php if(isset($user->status)): ?>
                                {{$user->status->name}}
                                <?php endif; ?>
                            </td>
                            <td>
                                {{--<a href="" class="btn btn-xs btn-info "> view </a>--}}
                                <a href="{{ route('user_status',['id' => $user->id]) }}"
                                   class="btn btn-xs btn-warning ">status</a>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>

                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        {{ $users->links() }}
                    </ul>
                </div>
            </div>

        </div>

    </div>

@endsection
