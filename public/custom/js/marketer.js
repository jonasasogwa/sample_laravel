$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#marketer').click('submit',function(e){
    e.preventDefault();
     var email = $("input[name=email]").val();
     var first_name = $("input[name=first_name]").val();
     var last_name = $("input[name=last_name]").val();
     var phone = $("input[name=phone]").val();
     var password = $("input[name=password]").val();
     var address = $("input[name=address]").val();
     //var type_id = $("input[name=type_id]").val();
     var status_id = $("input[name=status_id]").val();

    // Set up the AJAX indicator
    $('body').append('<div id="ajaxBusy"><p id="ajaxBusyMsg">Please wait...</p></div>');
    $('#ajaxBusy').show();

    $.ajax({
        type:'POST',
        url:'/marketer/add_marketer',
        data:{
            email:email,
            first_name:first_name,
            last_name:last_name,
            phone:phone,
            password:password,
            address:address,
            status_id:status_id
        },
        success:function(data){
            $('#ajaxBusy').hide();
            if (data.errors.email) {
                $('input[name="email"]').css("border","2px solid red");
            }else{  $('input[name="email"]').css("border","2px solid #ebebeb"); }
            if (data.errors.first_name) {
                $('input[name="first_name"]').css("border","2px solid red");
            }else{ $('input[name="first_name"]').css("border","2px solid #ebebeb"); }
            if (data.errors.last_name) {
                $('input[name="last_name"]').css("border","2px solid red");
            }else{ $('input[name="last_name"]').css("border","2px solid #ebebeb"); }
            if (data.errors.phone) {
                $('input[name="phone"]').css("border","2px solid red");
            }else { $('input[name="phone"]').css("border","2px solid #ebebeb");}
            if (data.errors.password) {
                $('input[name="password"]').css("border","2px solid red");
            }else { $('input[name="password"]').css("border","2px solid #ebebeb"); }
            if (document.getElementById("address").value == "") {
                $("#address").css("border","2px solid red");
            }else { $("#address").css("border","2px solid #ebebeb"); }
            if (document.getElementById("status_id").value == "") {
                $("#status_id").css("border","2px solid red");
            }else { $("#status_id").css("border","2px solid #ebebeb"); }

            if(status_id !=="" && address !=="" && password !=="" && last_name !=="" && phone !=="" && first_name !=="" && email !==""){
                if (data.errors.email =='The email has already been taken.') {
                    alert("The email has already been taken.");
                }else {
                    $('body').append('<div id="ajaxBusy"><p id="ajaxBusyMsg">Marketer Added Successfully!!!</p></div>');
                    $('#ajaxBusy').show();
                    $('#marketer_add').submit();
                    $('#ajaxBusy').hide();
                }
            }
        }
    });
});
