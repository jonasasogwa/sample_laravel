$(document).ready(function(){

    $('#source_type').change(function () {
        var source_type_id = $('#source_type').val();
        if(source_type_id == 0 || source_type_id == ''){
            return;
        }

        var url = $('#getSourceTypeNames').val();
       // console.log(source_type_id);
      //  return;
        $.get(url, {'source_type':source_type_id}, function (data) {
            console.log(data);
            var HTML = "<select name = 'source_name_id' class='form-control m-b source_name_id' id='source_name_id'><option value='0'>All Outlets</option>";
            $.each(data, function(i,v){
                HTML+="<option value='"+ i+"'>"+ v+"</option>";
            });
            HTML+="</select>";
            $(".sub_div").html(HTML);
        });
    });



    $('#productDispatchNotReceivedBtn').click(function(){
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        var source_type = $('#source_type').val();
        var source_name_id = $('#source_name_id').val();
        if(from_date == ''){
            alert('Date From cannot be empty');
            return;
        }
        if(to_date == ''){
            alert('Date To cannot be empty');
            return;
        }
        if(source_type == ''){
            alert('Source Type cannot be empty');
            return;
        }
        if(source_name_id == ''){
            alert('Source Name To cannot be empty');
            return;
        }
        var url = $('#getReport').val();

        $.ajax({
            url: url,
            type: 'get',
            data: {'from_date':from_date, 'to_date':to_date, 'source_type':source_type, 'source_name_id':source_name_id},
            dataType: 'json',
            success:function(data) {
                var HTML = '';
                //data = data.parseJSON();
                console.log(data.length);
                // HTML += '<table class="" style="width: 99%">';
                // HTML += ' <tr><td style="text-align: center;font-size: 20px; color: #ff6666">Product Dispatch & Not Received  :' +
                //
                //     'From Date ' + from_date + ' <br/>' +
                //     'To Date: ' + to_date + '<br/> </td> </tr> <tr> <td>';
                if (source_type == "mother_vessel"){
                    HTML += '<table class="table" ><thead><tr>'+
                        '<th>Sn</th>'+
                        '<th>Mother Vessel</th>'+
                        '<th>Daughter Vessel</th>'+
                        '<th>Product </th>'+
                        '<th>Bill of Laden No</th>'+
                        '<th>Dispatch Date</th>'+
                        '</tr></thead>';
                    if(data.length < 1){
                        HTML += '<tbody><tr><td colspan="7" style="font-size: 2em">No record found</td> </tr></tbody>';
                    }else {

                        $(data).each(function (i, v) {

                            HTML += '<table class="table" ><thead><tr>' +
                                '<th>' + i+1 + '</th>' +
                                '<th>'+ v.name+ '</th>'+
                                '<th>'+ v.daughterVessel+ '</th>'+
                                '<th>'+ v.product + '</th>'+
                                '<th>' + v.billOfLadenNo+'</th>'+
                                '<th>' +v.dispatchDate +'</th>'+
                                '</tr></thead>';
                        });
                    }
                }

                if (source_type == "daughter_vessel"){
                    HTML += '<table class="table" ><thead><tr>'+
                        '<th>Sn</th>'+
                        '<th>Daughter Vessel</th>'+
                        '<th>Terminal</th>'+
                        '<th>Product </th>'+
                        '<th>Bill of Laden No</th>'+
                        '<th>Dispatch Date</th>'+
                        '</tr></thead>';
                    if(data.length < 1){
                        HTML += '<tbody><tr><td colspan="7" style="font-size: 2em">No record found</td> </tr></tbody>';
                    }else {

                        $(data).each(function (i, v) {

                            HTML += '<table class="table" ><thead><tr>' +
                                '<th>' + i+1 + '</th>' +
                                '<th>'+ v.name+ '</th>'+
                                '<th>'+ v.terminal+ '</th>'+
                                '<th>'+ v.product + '</th>'+
                                '<th>' + v.billOfLadenNo+'</th>'+
                                '<th>' +v.dispatchDate +'</th>'+
                                '</tr></thead>';
                        });
                    }
                }
                if (source_type == "terminal"){
                    HTML += '<table class="table" ><thead><tr>'+
                        '<th width="5">Sn</th>'+
                        '<th width="15">Terminal</th>'+
                        '<th width="20">Depot</th>'+
                        '<th width="20">Outlet</th>'+
                        '<th width="15">Product Quantity</th>'+
                        '<th width="15">Dispatch Type</th>'+
                        '<th width="10">Estimated Time of Arrival</th>'+
                        '</tr></thead>';
                    if(data.length < 1){
                        HTML += '<tbody><tr><td colspan="7" style="font-size: 2em">No record found</td> </tr></tbody>';
                    }else {

                        $(data).each(function (i, v) {

                            HTML += '<table class="table" ><thead><tr>' +
                                '<th>' + i+1 + '</th>' +
                                '<th>'+ v.terminal+ '</th>'+
                                '<th>'+ v.depot + '</th>'+
                                '<th>' + v.outlet+'</th>'+
                                '<th>' +v.productQty +'</th>'+
                                '<th>' +v.dispatchType+ '</th>'+
                                '<th>' +v.estDateOfArrival+ '</th>'+
                                '</tr></thead>';
                        });
                    }

                }
                if (source_type == "depot"){
                    HTML += '<table class="table" ><thead><tr>'+
                        '<th>Sn</th>'+
                        '<th>Depot</th>'+
                        '<th>Outlet</th>'+
                        '<th>Product Quantity</th>'+
                        '<th>Waybill No</th>'+
                        '<th>Meter ticket no</th>'+
                        '</tr></thead>';
                    if(data.length < 1){
                        HTML += '<tbody><tr><td colspan="7" style="font-size: 2em">No record found</td> </tr></tbody>';
                    }else {

                        $(data).each(function (i, v) {

                            HTML += '<table class="table" ><thead><tr>' +
                                '<th>' + i+1 + '</th>' +
                                '<th>'+ v.depot + '</th>'+
                                '<th>' + v.retail_outlet+'</th>'+
                                '<th>' +v.productQty +'</th>'+
                                '<th>' +v.waybill_no+ '</th>'+
                                '<th>' +v.meter_ticket_number+ '</th>'+
                                '</tr></thead>';
                        });
                    }

                }
                $('.reports').html(HTML);

            }

        });
    });


    $('#consolidatedReport').click(function(){
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        var source_type = $('#source_type').val();
        var source_name_id = $('#source_name_id').val();
        if(from_date == ''){
            alert('Date From cannot be empty');
            return;
        }
        if(to_date == ''){
            alert('Date To cannot be empty');
            return;
        }
        if(source_type == ''){
            alert('Source Type cannot be empty');
            return;
        }
        if(source_name_id == ''){
            alert('Source Name To cannot be empty');
            return;
        }
        var url = $('#getReport').val();

        $.ajax({
            url: url,
            type: 'get',
            data: {'from_date':from_date, 'to_date':to_date, 'source_type':source_type, 'source_name_id':source_name_id},
            dataType: 'json',
            success:function(data) {
                var HTML = '';
                //data = data.parseJSON();
                console.log(data.length);
                // HTML += '<table class="" style="width: 99%">';
                // HTML += ' <tr><td style="text-align: center;font-size: 20px; color: #ff6666">Product Dispatch & Not Received  :' +
                //
                //     'From Date ' + from_date + ' <br/>' +
                //     'To Date: ' + to_date + '<br/> </td> </tr> <tr> <td>';
                HTML += '<table class="table" ><thead><tr>'+
                    '<th>Sn</th>'+
                    '<th>Product Type</th>'+
                    '<th>Quantity Received</th>'+
                    '<th>Quantity Dispatched </th>'+

                    '</tr></thead>';
                if(data.length < 1){
                    HTML += '<tbody><tr><td colspan="4" style="font-size: 2em">No record found</td> </tr></tbody>';
                }else {

                    $(data).each(function (i, v) {

                        HTML += '<table class="table" ><thead><tr>' +
                            '<th>' + i+1 + '</th>' +
                            '<th>'+ v.product+ '</th>'+
                            '<th>'+ v.dispatchQty+ '</th>'+
                            '<th>'+ v.receiveQty + '</th>'+
                            '</tr></thead>';
                    });
                }



                $('.reports').html(HTML);

            }

        });
    });



    $('#outletWithWithoutProduct').click(function(){
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        var type_id = $('#type_id').val();
        var report_id = $('#report_id').val();
        if(from_date == ''){
            alert('Date From cannot be empty');
            return;
        }
        if(to_date == ''){
            alert('Date To cannot be empty');
            return;
        }
        if(report_id == ''){
            alert('Choose Report type');
            return;
        }
        if(type_id == ''){
            alert('Choose Product Type');
            return;
        }

        // if(source_type_id == ''){
        //     alert('Choose Product Source');
        //     return;
        // }
        var url = $('#getReport').val();

        $.ajax({
            url: url,
            type: 'get',
            data: {'from_date':from_date, 'to_date':to_date, 'type_id':type_id, 'report_id':report_id},
            dataType: 'json',
            success:function(data) {
                var HTML = '';
                //data = data.parseJSON();
                console.log(data.length);
                // HTML += '<table class="" style="width: 99%">';
                // HTML += ' <tr><td style="text-align: center;font-size: 20px; color: #ff6666">Product Dispatch & Not Received  :' +
                //
                //     'From Date ' + from_date + ' <br/>' +
                //     'To Date: ' + to_date + '<br/> </td> </tr> <tr> <td>';
                if(report_id == 1){
                    HTML += '<table class="table" ><thead><tr>'+
                        '<th>Sn</th>'+
                        '<th>Outlet</th>'+
                        '<th>Address </th>'+
                        '<th>Balance(as at today)</th>'+
                        '</tr></thead>';
                }else{
                    HTML += '<table class="table" ><thead><tr>'+
                        '<th>Sn</th>'+
                        '<th>Outlet</th>'+
                        '<th>Address </th>'+
                        '</tr></thead>';
                }

                if(data.length < 1){
                    if(report_id == 1){
                        HTML += '<tbody><tr><td colspan="4" style="font-size: 2em">No record found</td> </tr></tbody>';
                    }else{
                        HTML += '<tbody><tr><td colspan="3" style="font-size: 2em">No record found</td> </tr></tbody>';
                    }
                  }else {
                     if (report_id == 1) {
                        $(data).each(function (i, v) {

                            HTML += '<table class="table" ><thead><tr>' +
                                '<th>' + i + 1 + '</th>' +
                                '<th>' + v.name + '</th>' +
                                '<th>' + v.address + '</th>' +
                                '<th>' + v.balance + '</th>' +
                                '</tr></thead>';
                        });
                    }
                    if (report_id == 2) {
                        $(data).each(function (i, v) {
                            HTML += '<table class="table" ><thead><tr>' +
                                '<th>' + i + 1 + '</th>' +
                                '<th>' + v.name + '</th>' +
                                '<th>' + v.address + '</th>' +
                                '</tr></thead>';
                        });
                    }
                }
                $('.reports').html(HTML);

            }

        });
    });
});


function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}