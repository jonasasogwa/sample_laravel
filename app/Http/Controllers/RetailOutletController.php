<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Depot;
use App\Models\Marketer;
use App\Models\State;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Retail_outlet;
use App\Models\City;
use App\Models\Status;
use Validator;
use Session;

class RetailOutletController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:web');
    }


    private function generateOutletCode($length = 6)
    {
        $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $digits = '0123456789';
        $randomString = '';
        for ($i = 0; $i < 3; $i++) {
            $randomString .= $letters[rand(0, strlen($letters) - 1)];
        }
        for ($i = 0; $i < 3; $i++) {
            $randomString .= $digits[rand(0, strlen($digits) - 1)];
        }
        return $randomString;
    }

    public function addnew()
    {
        $state = new State();
        $nig = $state->nigerian();
        $cities = City::whereIn('state_id', $nig)->get();

        $marketers = Marketer::all();
        $depots = Depot::all();
        return view('outlet.create')->
        with('cities', $cities)->
        with('marketers', $marketers)->
        with('depots', $depots)->
        with('statuses', Status::all());
    }

    public function createoutlet(Request $request)
    {
        $user = Auth::user();
        if ($user->hasRole(['outlet-manager'])) {
            $this->validate($request, [
                'address' => 'required',
                'city_id' => 'required|integer',
                'nearest_to_depot' => 'required|integer',
                'distant_to_depot' => 'required|integer',
                'tank_no' => 'required',
                'capacity' => 'required|integer',
                'pump_no' => 'required|integer',
                'date_of_purchase' => 'required',
                'low_demand_season' => 'required',
                'high_demand_season' => 'required',
                'name' => 'required'
            ]);
            $marketer_id = Marketer::whereUser_id($user->id)->first()->id;
        } else {
            $this->validate($request, [
                'address' => 'required',
                'city_id' => 'required|integer',
                'marketer_id' => 'required|integer',
                'nearest_to_depot' => 'required|integer',
                'distant_to_depot' => 'required|integer',
                'tank_no' => 'required',
                'capacity' => 'required|integer',
                'pump_no' => 'required|integer',
                'date_of_purchase' => 'required',
                'low_demand_season' => 'required',
                'high_demand_season' => 'required',
                'name' => 'required'
            ]);
            $marketer_id = $request->marketer_id;
        }

        $code = $this->generateOutletCode();
        //  $verification_code = $this->generate_verification_code();
        $check_if_exist = Retail_outlet::where('code', '=', $code)->first();
        //  $date_registered = date('Y-m-d');
        //  $last_login = date('Y-m-d 00:00:00');

        if (Retail_outlet::where('code', '=', $code)->count() > 0) {
            // code found, generate another
            $code = $this->generateOutletCode();
        }


        if ($check_if_exist === null) {
            $date_registered = date('Y-m-d');
            $last_login = date('Y-m-d 00:00:00');
            $password = bcrypt("password");
            $user = User::create([
                'verification_code' => $code,
                'email' => $request->email,
                'password' => $password,
                'date_registered' => $date_registered,
                'last_login' => $last_login

            ]);

            $user_id = $user->id;
            $user->attachRole(7);


            $outlet = new Retail_outlet;
            $outlet->code = $code;
//            $outlet->dealer_name = $request->dealer_name;
            $outlet->address = $request->address;
            $outlet->city_id = $request->city_id;
            $outlet->nearest_to_depot = $request->nearest_to_depot;
            $outlet->distant_to_depot = $request->distant_to_depot;
            $outlet->tank_no = $request->tank_no;
            $outlet->capacity = $request->capacity;
            $outlet->pump_no = $request->pump_no;
            $outlet->date_of_purchase = $request->date_of_purchase;
            $outlet->low_demand_season = $request->low_demand_season;
            $outlet->high_demand_season = $request->high_demand_season;
            //$outlet->status_id = $request->status_id;
            $outlet->name = $request->name;
            $outlet->user_id = $user_id;
            $outlet->marketer_id = $marketer_id;

            $outlet->save();
            if ($user->hasRole(['outlet-admin'])) {
                return redirect()->route('marketers');
            } else {
                return redirect()->route('marketers');
            }
//            } else{
//                return redirect()->route('marketers/user');
//            }


        } else {
            echo "Unable to create record";
        }
    }


    public function manage()
    {
        $user = Auth::user();
        $statuses = Status::all()->map(function ($statuses) {
            return $statuses->getAttributes();
        })->all();

        $cities = City::all()->map(function ($cities) {
            return $cities->getAttributes();
        })->all();

        Session::put('statuses', $statuses);
        Session::put('cities', $cities);
        //$cities = City::hydrate(Session::get('cities'));
        //echo($cities);

        if ($user->hasRole(['outlet-admin'])) {
            $marketer_id = Marketer::whereUser_id($user->id)->first()->id;
            $outlets = Retail_outlet::whereRaw("marketer_id = ?", [$marketer_id])->paginate(10);
        } else {
            $outlets = Retail_outlet::paginate(10);
        }
        return view('outlet.manage')->with('outlets', $outlets);


    }


    public function edit($id)
    {
       /* $rec = Retail_outlet::find($id);
        return view('outlet.edit')->with('outlet', $rec);
        */
        $user = Auth::user();
        if ($user->hasRole(['admin'])) {
            $rec = Retail_outlet::find($id);
        } else if ($user->hasRole(['outlet-admin'])) {
            $marketer_id = Marketer::whereUser_id($user->id)->first()->id;
            $rec = Retail_outlet::find($id);
            if ($rec->marketer_id != $marketer_id) {
                return redirect("outlet/manage");
            }
        } else {
            return redirect("/home");
        }

        return view('outlet.edit')->with('outlet', $rec);
    }

    public function view($id)
    {

        $user = Auth::user();
        $outlet = Retail_outlet::find($id);
        $next_id = Retail_outlet::where('id','>',$outlet->id)->min('id');
        $prev_id = Retail_outlet::where('id','<',$outlet->id)->max('id');
        if (!$user->hasRole(['admin'])) {
            if ($user->hasRole(['outlet-admin'])) {
                $marketer_id = Marketer::whereUser_id($user->id)->first()->id;
                if ($outlet->marketer_id != $marketer_id) {
                    return redirect("outlet/manage");
                }
            } else {
                return redirect("/outlet/manage");
            }
        }
        return view('outlet.view')->with('outlets', $outlet)
                                  ->with('next',$outlet->find($next_id))
                                  ->with('previous',$outlet->find($prev_id));
    }


    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin'])) {
            if ($user->hasRole(['outlet-admin'])) {
                $marketer_id = Marketer::whereUser_id($user->id)->first()->id;
                $rec = Retail_outlet::find($id);
                if ($rec->marketer_id != $marketer_id) {
                    return redirect("outlet/manage");
                }
            } else {
                return redirect("/home");
            }
        }
        $outlet = Retail_outlet::find($id);
        $outlet->code = $request->outlet_code;
//        $outlet->dealer_name = $request->dealer_name;
        $outlet->address = $request->address;
        $outlet->city_id = $request->city_id;
        $outlet->nearest_to_depot = $request->nearest_to_depot;
        $outlet->distant_to_depot = $request->distant_to_depot;
        $outlet->tank_no = $request->tank_no;
        $outlet->capacity = $request->capacity;
        $outlet->pump_no = $request->pump_no;
        $outlet->date_of_purchase = $request->date_of_purchase;
        $outlet->low_demand_season = $request->low_demand_season;
        $outlet->high_demand_season = $request->high_demand_season;
        $outlet->status_id = $request->status_id;
        $outlet->name = $request->name;


        $outlet->save();
        //  Session::flash('success', 'Your category was  succesfully updated');

        return redirect()->route('users');

    }

    public function destroy($id)
    {
        $user = Auth::user();
        if (!$user->hasRole(['admin'])) {
            if ($user->hasRole(['outlet-admin'])) {
                $marketer_id = Marketer::whereUser_id($user->id)->first()->id;
                $rec = Retail_outlet::find($id);
                if ($rec->marketer_id == $marketer_id) {
                    $rec = Retail_outlet::find($id);
                    $rec->delete();
                    return redirect()->route('outlet.manage');
                } else {
                    return redirect("outlet/manage");
                }
            } else {
                return redirect("outlet/manage");
            }
        } else {
            $rec = Retail_outlet::find($id);
            $rec->delete();
            return redirect()->route('outlet.manage');
        }

    }

    public function upload()
    {
        $marketers = Marketer::with('user.user_profile')->get();
        //dd($marketers);
        return view('outlet.upload')->with(compact('marketers'));
    }

    public function upload_Outlet_excel(Request $request)
    {
//        $this->validate($request, [
//            'marketer_id' => 'required'
//        ]);
        $input = $request->all();
        $user = Auth::user();
        if (!$user->hasRole(['outlet-admin'])) {
            $rules = [
                'marketer_id' => 'required',
            ];
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return back()->withInput()
                    ->withErrors($validator);
            }
            $marketer_id = $input['marketer_id'];
        } else {
            $marketer_id = Marketer::whereUser_id($user->id)->first()->id;
        }


        if ($request->hasFile('sample_file')) {
            $path = $request->file('sample_file')->getRealPath();
            $data = Excel::load($path)->get();
            //dd($data);
            if ($data->count()) {
                foreach ($data as $key => $value) {
                    //if (User::where('email', $value->email)->count() == 0){
                        $code = $this->generateOutletCode();
                        $date_registered = date('Y-m-d');
                        $last_login = date('Y-m-d 00:00:00');
                        $password = bcrypt("password");
                        $user = User::create([
                            'verification_code' => $code,
                            'email' => $request->email,
                            'password' => $password,
                            'date_registered' => $date_registered,
                            'last_login' => $last_login

                        ]);

                        $user_id = $user->id;
                        $user->attachRole(7);


                        $arr = [
                            'code' => $code,
                            'name' => $value->name,
                            'address' => $value->address,
                            //'city_id' => $value->city_id,
                            'nearest_to_depot' => $value->nearest_to_depot,
                            'distant_to_depot' => $value->distant_to_depot,
                            'tank_no' => $value->tank_no,
                            'capacity' => $value->capacity,
                            'pump_no' => $value->pump_no,
                            'date_of_purchase' => $value->date_of_purchase,
                            'low_demand_season' => $value->low_demand_season,
                            'high_demand_season' => $value->high_demand_season,
                            'status_id' => 1,
                            'user_id' => $user_id,
                            'marketer_id' => $marketer_id,
                        ];

                        Retail_outlet::create($arr);
                   // }
                }
                flash('Upload Successful!');
                return redirect()->back();
            }

        }else {
            flash('Request data does not have any files to import.');
            return redirect()->back();
        }


    }


    public function downloadExcelFile($type)
    {
        $user = Auth::user();
        if ($user->hasRole(['admin'])) {
            $outlets = Retail_outlet::get()->toArray();
        }
        else if ($user->hasRole(['outlet-admin'])) {
            $marketer_id = Marketer::whereUser_id($user->id)->first()->id;
            $outlets = Retail_outlet::whereRaw("marketer_id = ?", [$marketer_id])->toArray();
        }else{
            return redirect()->back();
        }

        return Excel::create('outlets_list', function ($excel) use ($outlets) {
            $excel->sheet('outlets List', function ($sheet) use ($outlets) {
                $sheet->fromArray($outlets);
            });
        })->download($type);
    }


}
