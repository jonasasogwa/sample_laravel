<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Marketer;
use App\Models\Retail_outlet;
use App\Models\Terminal;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usersCount = User::all()->count();
        $marketersCount = Marketer::all()->count();
        $retailOutlet = Retail_outlet::all()->count();
        $terminalCount = Terminal::all()->count();

        $data = [
            'usersCount' => $usersCount,
            'marketersCount' => $marketersCount,
            'retailOutlet' => $retailOutlet,
            'terminalCount' => $terminalCount
        ];


        return view('home')->with('data',$data);
    }
}
