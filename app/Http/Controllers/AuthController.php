<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function login(Request $request){
        if (Auth::attempt(["verification_code" => request("verification_code"), "password" => request("password")])) {
            $user = Auth::user();
            if($user->hasRole(['outlet-admin'])){
                return redirect('/home');
           }
        //    dd("meeee");
        } else {
            return redirect()->back()
                ->withInput($request->only('verification_code'))
                ->withErrors(['messages'=> "Username and password incorrect"]);
        }
    }
}
