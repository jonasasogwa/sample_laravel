<?php

namespace App\Http\Controllers;

use App\Models\DepotOutlet;
use Illuminate\Http\Request;
use App\Models\DepotReceives;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;

class DepotController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function ReceiveIndex(){
        $depots = DepotReceives::paginate(15);
        return view('reports.depots_receives',compact('depots'));
    }

    public function GetRangeReceived(){
        $input = Input::all();
        $date_from = date('Y-m-d',strtotime($input['from_date']));
        $date_to = date('Y-m-d',strtotime($input['to_date']));
        $depots  = DepotReceives::whereRaw('loading_date >= ? and loading_date <= ?',[$date_from,$date_to])->paginate(15);
        return view('reports.depots_receives',compact('depots'));
        if($input['type']=='csv' || $input['type']=='xls' || $input['type']=='xlsx'){
            //download for spreadsheet
            return Excel::create('Depot Receives Report', function($excel) use ($depots) {
                $excel->sheet('Depot Receives Report', function($sheet) use ($depots) {
                    $sheet->loadView('reports.depot_receives_exel')->with('depots',$depots)
                        ->row(1, array('Terminal', 'Depot', 'Truck No.', 'Metre Ticket No', 'Product Qty','Loading Date','Loading Date'));
                });
            })->download($input['type']);
        }else{
            //download for pdf
            $pdf = PDF::loadView('reports.depot_receives_pdf', ['depots'=>$depots]);
            return $pdf->download('depot_receives_report.pdf');
        }
    }

    public function downloadExcelFile($type){

        $depots = DepotReceives::all();

        return Excel::create('Depot Receives Report', function($excel) use ($depots) {
            $excel->sheet('Depot Receives Report', function($sheet) use ($depots) {
                $sheet->loadView('reports.depot_receives_exel')->with('depots',$depots)
                    ->row(1, array('Terminal', 'Depot', 'Truck No.', 'Metre Ticket No', 'Product Qty','Loading Date','Loading Date'));
            });
        })->download($type);

    }

    public function downloadPdf(){
        $depots = DepotReceives::all();
        $pdf = PDF::loadView('reports.depot_receives_pdf', ['depots'=>$depots]);
        return $pdf->download('depot_receives_report.pdf');
    }

    public function DispatchIndex(){
        $depots = DepotOutlet::all();
        return view('reports.depots_dispatches',compact('depots'));
    }

    public function GetRangeDispatched(){
        $input = Input::all();
        $date_from = date('Y-m-d',strtotime($input['from_date']));
        $date_to = date('Y-m-d',strtotime($input['to_date']));
        $depots = DepotOutlet::whereRaw('loading_date >= ? and loading_date <= ?',[$date_from,$date_to])->get();
        //specify the type of download
        if($input['type']=='csv' || $input['type']=='xls' || $input['type']=='xlsx'){
            //download for spreadsheet
            return Excel::create('DepotOutlet Excel', function($excel) use ($depots) {
                $excel->sheet('DepotOutlet sheet', function($sheet) use ($depots) {
                    $sheet->loadView('reports.depot_dispatches_excel')->with('depots',$depots)
                        ->row(1, array('Terminal', 'Depot', 'Truck No.', 'Metre Ticket No', 'Product Qty','Loading Date','Loading Date'));
                });
            })->download($input['type']);
        }else{
            //download for pdf
            $pdf = PDF::loadView('reports.depot_vessel_dispatch_pdf', ['depots'=>$depots]);
            return $pdf->download('depot_dispatch_report.pdf');
        }
    }


    public function downloadDispatchedExcelFile($type){

        $depots = DepotOutlet::all();
        return Excel::create('DepotOutlet Report', function($excel) use ($depots) {
            $excel->sheet('DepotOutlet Report', function($sheet) use ($depots) {
                $sheet->loadView('reports.depot_dispatches_excel')->with('depots',$depots);
                $sheet->row(1, array('Terminal', 'Depot', 'Truck No.', 'Metre Ticket No', 'Product Qty','Loading Date','Loading Date'));
            });
        })->download($type);

    }

    public function downloadDispatchedPdf(){
        $depots = DepotOutlet::all();
        $pdf = PDF::loadView('reports.depot_vessel_dispatch_pdf', ['depots'=>$depots]);
        return $pdf->download('depot_dispatch_report.pdf');
    }


}
