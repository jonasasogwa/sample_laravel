<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DaughterVesselReceive;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;
use App\Models\DaughterVesselDispatch;


class DaughterVesselReceiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index(){
        $daughters = DaughterVesselReceive::paginate(15);
        return view('reports.daughter_vessel_receives',compact('daughters'));
    }

    public function ViewIndex(){
        $input = Input::all();
        $date_from = date('Y-m-d',strtotime($input['from_date']));
        $date_to = date('Y-m-d',strtotime($input['to_date']));
        $daughters  = DaughterVesselReceive::whereRaw('loading_date >= ? and loading_date <= ?',[$date_from,$date_to])->paginate(15);

        return view('reports.daughter_vessel_receives',compact('daughters'));
        //specify the type of download
        if($input['type']=='csv' || $input['type']=='xls' || $input['type']=='xlsx'){
            //download for spreadsheet
            return Excel::create('Daughter Vessel Report', function($excel) use ($daughters) {
                $excel->sheet('Daughter Vessel Report', function($sheet) use ($daughters) {
                    $sheet->loadView('reports.daughter_vessel_receives_exel')->with('daughters',$daughters)
                        ->row(1, array('Product Type', 'Bill of Laden Number', 'Bill of Laden Qty', 'Out Turn Qty','Loading Date'));
                });
            })->download($input['type']);
        }else{
            //download for pdf
            $pdf = PDF::loadView('reports.daughter_vessel_receives_pdf', ['daughters'=>$daughters]);
            return $pdf->download('daughter_vessel_receives_report.pdf');
        }
    }

    public function downloadExcelFile($type){

        $daughters = DaughterVesselReceive::all();

        return Excel::create('Daughter Vessel Report', function($excel) use ($daughters) {
            $excel->sheet('Daughter Vessel Report', function($sheet) use ($daughters) {
                $sheet->loadView('reports.daughter_vessel_receives_exel')->with('daughters',$daughters)
                    ->row(1, array('Product Type', 'Bill of Laden Number', 'Bill of Laden Qty', 'Out Turn Qty','Loading Date'));
            });
        })->download($type);

    }

    public function downloadPdf(){
        $daughters = DaughterVesselReceive::all();
        $pdf = PDF::loadView('reports.daughter_vessel_receives_pdf', ['daughters'=>$daughters]);
        return $pdf->download('daughter_vessel_receives_report.pdf');
    }

    public function DaughterDispatchesIndex(){
        $dispatches = DaughterVesselDispatch::all();
        return view('reports.daughter_vessel_dispatches',compact('dispatches'));
    }



    public function GetRangeDispatched(){
        $input = Input::all();
        $date_from = date('Y-m-d',strtotime($input['from_date']));
        $date_to = date('Y-m-d',strtotime($input['to_date']));
        $dispatches = DaughterVesselDispatch::whereRaw('dispatch_date >= ? and dispatch_date <= ?',[$date_from,$date_to])->get();
        //specify the type of download
        if($input['type']=='csv' || $input['type']=='xls' || $input['type']=='xlsx'){
            //download for spreadsheet
            return Excel::create('DaughterVesselDispatch Excel', function($excel) use ($dispatches) {
                $excel->sheet('DaughterVesselDispatch sheet', function($sheet) use ($dispatches) {
                    $sheet->loadView('reports.daughter_vessel_dispatches_excel')->with('dispatches',$dispatches)
                        ->row(1, array('Terminal', 'Product Type', 'Bill of Laden Qty', 'Qty Dispatched','Daughter Vessel','Dispatched Date'));
                });
            })->download($input['type']);
        }else{
            //download for pdf
            $pdf = PDF::loadView('reports.daughter_vessel_dispatch_pdf', ['dispatches'=>$dispatches]);
            return $pdf->download('daughter_vessel_dispatch_report.pdf');
        }
    }

    public function downloadDispatchExcelFile($type){

        $dispatches = MotherVesselDispatch::all();
        return Excel::create('Mother Vessel Dispatches Report', function($excel) use ($dispatches) {
            $excel->sheet('Mother Vessel Dispatches Report', function($sheet) use ($dispatches) {
                $sheet->loadView('reports.mother_vessel_dispatches_excel')->with('dispatches',$dispatches);
                $sheet->row(1, ['Product Type', 'Bill of Laden Number', 'Bill of Laden Qty', 'Mother Vessel','Daughter Vessel','Dispatched Date']);
            });
        })->download($type);
    }

    public function downloadDispatchPdf(){
        $dispatches = MotherVesselDispatch::all();
        $pdf = PDF::loadView('reports.mother_vessel_dispatch_pdf', ['dispatches'=>$dispatches]);
        return $pdf->download('mother_vessel_dispatch_report.pdf');
    }


}
