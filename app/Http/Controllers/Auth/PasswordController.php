<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web');
    }


    public function getChangePasswordView(){
        return view("changePassword");
    }

    public function savePassword(Request $request){
        $input = $request->all();
        $rules = [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_new_password' => 'required|same:new_password',
        ];


        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator);
        }
        $old_password = request('old_password');
        $new_password = request('new_password');


        $userObj = Auth::user();
        if (!Hash::check($old_password, $userObj->password)) {
            return back()->withInput()
                ->withErrors("Password Incorrect!!!");
        }

        $user = User::find($userObj->id);
        $user->password = bcrypt($new_password);
        $user->save();
        return back()->with('status','password successfully changes');
    }
}