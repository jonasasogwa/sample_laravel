<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Retail_outlet;
use App\Models\Outlet_receive;
use Illuminate\Support\Facades\Input;
use Barryvdh\DomPDF\Facade as PDF;

class RetailOutletReportController extends Controller
{
    public function __construct(){
        $this->middleware('auth:web');
    }

    public function ReceiveIndex(){
        $outlets = Outlet_receive::paginate(15);
        return view('reports.outlets_receives',compact('outlets'));
    }

    public function GetRangeReceived(){
        $input = Input::all();
        $date_from = date('Y-m-d',strtotime($input['from_date']));
        $date_to = date('Y-m-d',strtotime($input['to_date']));
        $outlets  = Outlet_receive::whereRaw('date_time_dispatch >= ? and date_time_dispatch <= ?',[$date_from,$date_to])->paginate(15);
        //specify the type of download
        return view('reports.outlets_receives',compact('outlets'));
        if($input['type']=='csv' || $input['type']=='xls' || $input['type']=='xlsx'){
            //download for spreadsheet
            return Excel::create('Outlets Receives Report', function($excel) use ($outlets) {
                $excel->sheet('Outlets Receives Report', function($sheet) use ($outlets) {
                    $sheet->loadView('reports.outlet_receives_exel')->with('outlets',$outlets)
                        ->row(1, array('Dispatch No', 'Truck No.', 'Product Qty.', 'Driver Name',
                            'Waybill No.','Date-Time Dispatch','Estimated Arrival Date'));
                });
            })->download($input['type']);
        }else{
            //download for pdf
            $pdf = PDF::loadView('reports.outlet_receives_pdf', ['outlets'=>$outlets]);
            return $pdf->download('outlet_receives_report.pdf');
        }
    }

    public function downloadExcelFile($type){
        $outlets = Outlet_receive::all();

        return Excel::create('Outlet Receives Report', function($excel) use ($outlets) {
            $excel->sheet('Outlet Receives Report', function($sheet) use ($outlets) {
                $sheet->loadView('reports.outlet_receives_exel')->with('outlets',$outlets)
                    ->row(1, array('Dispatch No', 'Truck No.', 'Product Qty.', 'Driver Name',
                        'Waybill No.','Date-Time Dispatch','Estimated Arrival Date'));
            });
        })->download($type);

    }

    public function downloadPdf(){
        $outlets = Outlet_receive::all();
        $pdf = PDF::loadView('reports.outlet_receives_pdf', ['outlets'=>$outlets]);
        return $pdf->download('outlet_receives_report.pdf');
    }

    public function OutLetMassIndex(){
        return view('reports.retail_outlets_mass_balance');
    }

}
