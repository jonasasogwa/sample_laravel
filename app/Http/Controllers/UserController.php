<?php

namespace App\Http\Controllers;


use App\Models\Status;
use App\Models\User_profile;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index()
    {
        $users = User::paginate(10);

        return view('user.users')->with('users',$users);
    }

    public function changeStatusView($id)
    {
        $user = User::find($id);
        $status = Status::all();
        $data = [
            "user" => $user,
            "status" => $status
        ];
        return view('user.edit_status', $data);
    }

    public function updateStatus(Request $request, $id){
        $user = User::find($id);
        $user->status_id = $request->status_id;
        $user->save();
        flash('Status Changed!');
        return redirect()->back();
    }

    public function SortView(){
        $users = User::paginate(10);
        $roles = Role::all();
        $data = [
            "users" => $users,
            "roles" => $roles
        ];
        return view('user.sort_user',$data);
    }

    public function SortAll(){
        $input = Input::all();
        $user_role = $input['role'];
        $roles = Role::all();

        $role1 = Role::whereRaw("name = ?", [$user_role])->get()->pluck("id")->toArray();
        $users = User::whereHas('roles', function ($q) use ($role1) {
            $q->whereIn("id", $role1);
        })->with(['user_profile'])->paginate(10);

        $data = [
            "users" => $users,
            "roles" => $roles
        ];
        return view('user.sort_user',$data);
    }
}
