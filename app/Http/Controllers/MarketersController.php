<?php

namespace App\Http\Controllers;

//use Dotenv\Validator;
use App\Traits\Mailer;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Models\Marketer;
use App\Models\User;
use App\Models\Role;
use App\Models\Role_user;
use App\Models\Type;
use App\Models\User_profile;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;


class MarketersController extends Controller
{
    use Mailer;
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index()
    {
        $roles = Role::whereRaw("name = ?", ["outlet-admin"])->get()->pluck("id")->toArray();
        $marketers = User::whereHas('roles', function ($q) use ($roles) {
                       $q->whereIn("id", $roles);
                })->with(['user_profile', 'marketers'])->paginate(15);

        //dd($marketers->all());

        $data = [
            "maketers" => $marketers
        ];

        return view("maketer.maketers", $data);
    }

    public function add_marketer()
    {
        $types = Type::all();
        return view('maketer.add_marketer')->with('types', $types);
    }

    protected $rules = [
        'email' => 'required|unique:users',
        'first_name' => 'required',
        'last_name' => 'required',
        'phone' => 'required',
        'password' => 'required',
        'address' => 'required',
        'status_id' => 'required'
    ];
    public function store_marketer(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
            }

            $code = $this->generateCouponCode();
            $verification_code = $this->generate_verification_code();
            $check_if_exist = Marketer::where('code', '=', $code)->first();
            $date_registered = date('Y-m-d');
            $last_login = date('Y-m-d 00:00:00');

            if (Marketer::where('code', '=', $code)->count() > 0) {
                // code found, gen again
                $code = $this->generateCouponCode();
            }

            if ($check_if_exist === null) {

                $password = bcrypt($request->password);

                $user1 = [
                    'verification_code' => $verification_code,
                    'email' => $request->email,
                    'password' => $password,
                    'password1' => $request->password,
                    'date_registered' => $date_registered,
                    'status_id' => $request->status_id,
                    'last_login' => $last_login,
                ];
                $user = User::create($user1);

                $user_id = $user->id;
                $user->attachRole(8);

                //add the created users profile to user_profile tbl
                User_profile::create([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'phone' => $request->phone,
                    'user_id' => $user_id
                ]);
                $user1['first_name'] = $request->first_name;
                $user1['last_name'] = $request->last_name;
                $user1['email'] = $request->email;

                 //marketer with code doesn't exist
                $query = Marketer::create([
                    'code' => $code,
                    'address' => $request->address,
                    'status_id' => $request->status_id,
                    'user_id' => $user_id
                ]);

                try {
                    $this->sendWelcomeEmail($user1);
                } catch (\Exception $e) {
                    return response()->json([
                        'type' => 'email_fails',
                        'message' => $e->getMessage(),
                    ]);
                }

                if($query == true){
                    flash('New Marketer Added Successfully!');
                    return redirect()->back();
               }

            }
    }

    public function generate_verification_code()
    {
        $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $digits = '1234567890';
        $randomString = '';
        for ($i = 0; $i < 3; $i++) {
            $randomString .= $letters[rand(0, strlen($letters) - 1)];
        }
        for ($i = 0; $i < 3; $i++) {
            $randomString .= $digits[rand(0, strlen($digits) - 1)];
        }

        $token = str_shuffle($randomString);
        $obj = User::where("verification_code", $token)->first();
        //dd($obj);
        if ($obj > 0) {
            $this->generate_verification_code();
        }
        return $token;
    }

    public function upload_excel(Request $request)
    {
        if ($request->hasFile('sample_file')) {
            $path = $request->file('sample_file')->getRealPath();
            $data = Excel::load($path)->get();

                $password = bcrypt('password');
                $date_registered = date('Y-m-d H:i:s');

                foreach ($data as $key => $value) {
                    if (User::where('email', $value->email)->count() == 0){
                        $code = $this->generateCouponCode();
                        $verification_code = $this->generate_verification_code();

                        $arr1 = ['verification_code' => $verification_code, 'email' => $value->email,
                                'password' => $password, 'date_registered' => $date_registered, 'status_id' => 1];

                        $arr1['password1'] = 'password';
                        $new_user = User::create($arr1);
                        $user_id = $new_user->id;

                        $new_user->attachRole(8);
                        $arr = ['code' => $code, 'address' => $value->address, 'status_id' => 1, 'user_id' => $user_id];

                        Marketer::create($arr);

                        $arr1['first_name'] = $value->first_name;
                        $arr1['last_name'] = $value->last_name;
                        try {
                            $this->sendWelcomeEmail($arr1);
                        } catch (\Exception $e) {
                            return response()->json([
                                'type' => 'email_fails',
                                'message' => $e->getMessage(),
                            ]);
                        }

                        $arr2 = ['first_name' => $value->first_name, 'last_name' => $value->last_name,
                                'phone' => $value->phone, 'user_id' => $new_user->id];
                        User_profile::create($arr2);
                    }
                }

                flash('Upload Successful!');
                return redirect()->route('marketers');

            }
        else {
            flash('Request data does not have any files to import.');
            return redirect()->route('marketers');

        }
    }

    public function downloadExcelFile($type)
    {
        //marketers are given role of outlet-managers
        $roles = Role::whereRaw("name = ?", ["outlet-admin"])->get()->pluck("id")->toArray();
        $marketers = User::select("user_profiles.last_name", "user_profiles.first_name", "user_profiles.phone",
            "users.id", "users.verification_code", "users.email","marketers.code","marketers.address")
            ->join("user_profiles", "user_profiles.user_id", "=", "users.id")
            ->join("marketers", "marketers.user_id","=", "users.id")
            ->join("role_user", "role_user.user_id", "=", "users.id")
            ->whereIn("role_user.role_id", $roles)
            ->get()->toArray();

        return Excel::create('marketers_list', function ($excel) use ($marketers) {
            $excel->sheet('Marketers List', function ($sheet) use ($marketers) {
                $sheet->fromArray($marketers);
            });
        })->download($type);
    }

    private function generateCouponCode($length = 6)
    {
        $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $digits = '1234567890';
        $randomString = '';
        for ($i = 0; $i < 3; $i++) {
            $randomString .= $letters[rand(0, strlen($letters) - 1)];
        }
        for ($i = 0; $i < 3; $i++) {
            $randomString .= $digits[rand(0, strlen($digits) - 1)];
        }
        return $randomString;
    }

    public function getEditableMarketerView($id)
    {
        $roles = Role::whereRaw("name = ?", ["outlet-admin"])->get()->pluck("id")->toArray();
        $marketer = User::select("user_profiles.last_name", "user_profiles.first_name", "user_profiles.phone",
            "users.id", "users.password", "users.email","marketers.code","marketers.address")
            ->join("user_profiles", "user_profiles.user_id", "=", "users.id")
            ->join("role_user", "role_user.user_id", "=", "users.id")
            ->join("marketers", "marketers.user_id", "=", "users.id")
            ->whereIn("role_user.role_id", $roles)
            ->whereRaw("users.id",$id)
            ->get()->first();
        return view("maketer.edit_marketer", compact('marketer'));
    }

    public function UpdateMarketer(Request $request){
        $input = $request->all();
        $id = $input['id'];
        $this->validate($request, [
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'status_id' => 'required'
        ]);

        $userinfo  = array(
            'email' => $input['email'],
            'status_id' => $input['status_id']
        );

        $profileinfo  = array(
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'phone' => $input['phone']
        );

        $marketerinfo = array(
            'address' => $input['address'],
            'status_id' => $input['status_id']
        );

        User::where('id', $id)->update($userinfo);
        User_profile::where('user_id', $id)->update($profileinfo);
        Marketer::where('user_id', $id)->update($marketerinfo);

        flash('Update Successful!');
        return back()->withInput();
    }

    public function DestroyMarketer($id)
    {

        Role_user::whereRaw("user_id = ?", [$id])->delete();
        User_Profile::whereRaw("user_id = ?", [$id])->delete();
        Marketer::whereRaw("user_id = ?", [$id])->delete();
        User::find($id)->delete();
        flash('Query Ok, User Removed!');
        return redirect()->back();

    }

    public function marketers_requests(){
        $marketers = User_profile::select("marketers.user_id","marketers.id","marketers.code","user_profiles.last_name",
                "user_profiles.first_name","user_profiles.user_id","marketer_requests.retail_outlet_id",
                "marketer_requests.type_id","types.id","types.name","types.description")
                ->join("marketer_requests", "marketer_requests.retail_outlet_id", "=", "user_profiles.user_id")
                ->join("marketers", "marketers.user_id", "=", "marketer_requests.retail_outlet_id")
                ->join("types","types.id", "=", "marketer_requests.type_id")
                ->get();
        $data = [
            "maketers" => $marketers
        ];
        return view("maketer.marketers_request", $data);
    }

    public function downloadExcelFileReq($type){

        $marketers = User_profile::select("marketers.code","user_profiles.last_name",
                            "user_profiles.first_name","types.name","types.description")
            ->join("marketer_requests", "marketer_requests.retail_outlet_id", "=", "user_profiles.user_id")
            ->join("marketers", "marketers.user_id", "=", "marketer_requests.retail_outlet_id")
            ->join("types","types.id", "=", "marketer_requests.type_id")
            ->get()->toArray();

        return Excel::create('marketers_list', function ($excel) use ($marketers) {
            $excel->sheet('Marketers List', function ($sheet) use ($marketers) {
                $sheet->fromArray($marketers);
            });
        })->download($type);

    }

}

