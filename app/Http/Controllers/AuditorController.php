<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use App\Models\User_profile;
use App\Traits\Mailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class AuditorController extends Controller
{
    use Mailer;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index()
    {

        $roles = Role::whereRaw("category = ?", ["auditor"])->get()->pluck("id")->toArray();
        $auditors = User::select("user_profiles.last_name", "user_profiles.first_name", "user_profiles.phone",
            "users.id", "users.verification_code", "users.email")
            ->join("user_profiles", "user_profiles.user_id", "=", "users.id")
            ->join("role_user", "role_user.user_id", "=", "users.id")
            ->whereIn("role_user.role_id", $roles)
            ->paginate(15);

        $data = [
            "auditors" => $auditors
        ];
        return view("auditor.auditorList", $data);
    }

    public function getAuditorView()
    {
        $user = Auth::user();
        if(!$user->hasRole(['admin', 'admin-auditor'])){
            return back()->withInput()
                ->withErrors("You are not authorize to perform this operation");
        }

        $roleList1[''] = 'Select Role';
        $roleList = [];
        $roles = $roles = Role::whereRaw("category = ?", ["auditor"])->get();
        foreach ($roles as $role) {
            $roleList[$role->id] = $role->display_name;
        }
        //   $roleList = appendArray($roleList1,$roleList);
        $isAbleToChangeRole = true;

        $data = [
            "roles" => $roleList,
            'isAbleToChangeRole' => $isAbleToChangeRole
        ];
        return view("auditor.auditor", $data);
    }

    public function createAuditor(Request $request)
    {
        $user = Auth::user();
        if(!$user->hasRole(['admin', 'admin-auditor'])){
            return back()->withInput()
                ->withErrors("You are not authorize to perform this operation");
        }
        $input = $request->all();
        $rules = [
            'last_name' => 'required',
            'first_name' => 'required',
            'role_id' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ];


        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator);
        } else {
            $input['verification_code'] = $this->generate_verification_code();
            $input['date_registered'] = date("Y-m-d");
            $input['password1'] = $input['password'];
            $input['password'] = bcrypt($input['password']);

            $user = User::create($input);
            $input['user_id'] = $user->id;
            $user->attachRole($input['role_id']);
            User_profile::create($input);
            try {
                $this->sendWelcomeEmail($input);
            } catch (\Exception $e) {
                return response()->json([
                    'type' => 'email_fails',
                    'message' => $e->getMessage(),
                ]);
            }

        }
        return redirect('auditors/');

    }

    public function getEditedAuditorView($id)
    {
        $user = Auth::user();
        if(!$user->hasRole(['admin', 'admin-auditor'])){
            if($user->id != $id){
                return back()->withInput()
                    ->withErrors("You are not authorize to perform this operation");
            }
        }

        if(!$user->hasRole(['admin', 'admin-auditor'])){
            if($user->id != $id){
            }
        }

        $user = Auth::user();
        if(!$user->hasRole(['admin', 'admin-auditor'])){
            if($user->id != $id){
                return back()->withInput()
                    ->withErrors("You are not authorize to perform this operation");
            }
        }
        $auditor = User::with(['user_profile'])->whereId($id)->first();
        $roleList1[''] = 'Select Role';
        $roleList = [];
        $roles = $roles = Role::whereRaw("category = ?", ["auditor"])->get();
        foreach ($roles as $role) {
            $roleList[$role->id] = $role->display_name;
        }
        $isAbleToChangeRole = true;
        if($user->hasRole(['admin', 'admin-auditor'])){
           if($user->id == $id){
               $isAbleToChangeRole = false;
           }
        }
        $droleid = $auditor->roles()->first()->id;

        $data = [
            "roles" => $roleList,
            'auditorObj' => $auditor,
            'isAbleToChangeRole' => $isAbleToChangeRole,
            'role_id' =>$droleid
        ];
        
        return view("auditor.auditor", $data);
    }

    public function updateAuditor()
    {
        $input = request()->all(); 
        $id = decrypt($input['auditor_id']);
        $user = Auth::user();
        if(!$user->hasRole(['admin', 'admin-auditor'])){
            if($user->id != $id){
                return back()->withInput()
                    ->withErrors("You are not authorize to perform this operation");
            }
        }
       
        $user2change = User::find($id);
        $user2change->update(["email" => $input["email"]]);
        $up = User_profile::find($user2change->user_profile->id)->update(["last_name" => $input['last_name'], "first_name" => $input['first_name'], "phone" => $input['phone']]);
        
        if($user->hasRole(['admin', 'admin-auditor'])){
           
            if($user->id != $id){
                
                DB::table('role_user')->whereRaw("user_id = ?", [$id])->delete();
                
                $user2change->attachRole($input['role_id']);
            }
        }

        return redirect('auditors/');
    }

    public function delete()
    {
        $id = Input::get('id');
        DB::table('role_user')->whereRaw("user_id = ?", [$id])->delete();
        User_Profile::whereRaw("user_id = ?", [$id])->delete();
        User::find($id)->delete();
        return '200';
    }

    public function getUploadView()
    {
        $user = Auth::user();
        if(!$user->hasRole(['admin', 'admin-auditor'])){
            return back()->withInput()
                ->withErrors("You are not authorize to perform this operation");
        }
        return view("auditor.uploadAuditor");
    }

    public function saveUpload(Request $request)
    {

        if ($request->hasFile('sample_file')) {
            $path = $request->file('sample_file')->getRealPath();
            $data = Excel::load($path)->get();

            //if ($data->count()) {
                foreach ($data as $key => $value) {
                    if (User::where('email', $value->email)->count() == 0) {
                        $user = ['email' => $value->email, 'password' => bcrypt($value->last_name), 'phone' => $value->phone,
                            'verification_code' => $this->generate_verification_code(), 'date_registered' => date("Y-m-d")];
                        $user = User::create($user);
                        $user_id = $user->id;
                        $profile = ['user_id' => $user_id, 'last_name' => $value->last_name, 'first_name' => $value->first_name, 'phone' => $value->phone];
                        User_profile::create($profile);
                        $user->attachRole(5);
                        $user['password1'] = $value->last_name;
                        $user['last_name'] = $value->last_name;
                        $user['first_name'] = $value->first_name;
                        try {
                            $this->sendWelcomeEmail($user);
                        } catch (\Exception $e) {
                            return response()->json([
                                'type' => 'email_fails',
                                'message' => $e->getMessage(),
                            ]);
                        }
                    }
                }
                return redirect('/auditors');

           // }
        } else {
            flash('Request data does not have any files to import.');
            return redirect('/auditors');
        }


    }

    /**
     * generate authentication code
     * @return string
     */
    public function generate_verification_code()
    {
        $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $digits = '1234567890';
        $randomString = '';
        for ($i = 0; $i < 3; $i++) {
            $randomString .= $letters[rand(0, strlen($letters) - 1)];
        }
        for ($i = 0; $i < 3; $i++) {
            $randomString .= $digits[rand(0, strlen($digits) - 1)];
        }

        $token = str_shuffle($randomString);
        $obj = User::whereRaw("verification_code  =  ?", [$token])->count();
        if ($obj > 0) {
            $this->generate_verification_code();
        }
        return $token;
    }

    public function downloadExcelFile($type)
    {
        $roles = Role::whereRaw("category = ?", ["auditor"])->get()->pluck("id")->toArray();
        $auditors = User::select("user_profiles.last_name", "user_profiles.first_name", "user_profiles.phone",
            "users.id", "users.verification_code", "users.email")
            ->join("user_profiles", "user_profiles.user_id", "=", "users.id")
            ->join("role_user", "role_user.user_id", "=", "users.id")
            ->whereIn("role_user.role_id", $roles)
            ->get()->toArray();
        return Excel::create('auditors_list', function ($excel) use ($auditors) {
            $excel->sheet('Auditor List', function ($sheet) use ($auditors) {
                $sheet->fromArray($auditors);
            });
        })->download($type);

    }



}
