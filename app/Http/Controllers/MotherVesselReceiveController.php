<?php

namespace App\Http\Controllers;

use App\Models\MotherVesselDispatch;
use Illuminate\Http\Request;
use App\Models\MotherVesselReceive;
use App\Models\Type;
use App\Models\MotherVesselDispatches;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;

class MotherVesselReceiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index()
    {
        $receives = MotherVesselReceive::paginate(15);
        return view('reports.mother_vessel_receives', compact('receives'));
    }

    public function ViewIndex()
    {
        $input = Input::all();
        $date_from = date('Y-m-d', strtotime($input['from_date']));
        $date_to = date('Y-m-d', strtotime($input['to_date']));
        $receives = MotherVesselReceive::whereRaw('arrival_date >= ? and arrival_date <= ?', [$date_from, $date_to])->paginate(15);
        return view('reports.mother_vessel_receives', compact('receives'));
        if ($input['type'] == 'csv' || $input['type'] == 'xls' || $input['type'] == 'xlsx') {
            //download for spreadsheet
            return Excel::create('Laravel Excel', function ($excel) use ($receives) {
                $excel->sheet('Excel sheet', function ($sheet) use ($receives) {
                    $sheet->loadView('reports.mother_vessel_receives_exel')->with('receives', $receives)
                        ->row(1, array('Product Type', 'Bill of Laden Number', 'Bill of Laden Qty', 'Out Turn Qty', 'Arrival Date'));
                });
            })->download($input['type']);
        } else {
            //download for pdf
            $pdf = PDF::loadView('reports.mother_vessel_receives_pdf', ['receives' => $receives]);
            return $pdf->download('mother_vessel_receives_report.pdf');
        }
    }

    public function downloadExcelFile($type)
    {

        $receives = MotherVesselReceive::all();

        return Excel::create('mother_vessel_receives', function ($excel) use ($receives) {
            $excel->sheet('Mother Vessel Receives List', function ($sheet) use ($receives) {
                $sheet->fromArray($receives);
            });
        })->download($type);
    }

    public function downloadPdf()
    {
        $receives = MotherVesselReceive::all();
        $pdf = PDF::loadView('reports.mother_vessel_receives_pdf', ['receives' => $receives]);
        return $pdf->download('mother_vessel_receives_report.pdf');
    }

    public function MotherDispatchesIndex()
    {
        $dispatches = MotherVesselDispatches::all();
        return view('reports.mother_vessel_dispatches', compact('dispatches'));
    }

    public function GetRangeDispatched()
    {
        $input = Input::all();
        $date_from = date('Y-m-d', strtotime($input['from_date']));
        $date_to = date('Y-m-d', strtotime($input['to_date']));
        $dispatches = MotherVesselDispatch::whereRaw('dispatch_date >= ? and dispatch_date <= ?', [$date_from, $date_to])->get();
        //specify the type of download
        if ($input['type'] == 'csv' || $input['type'] == 'xls' || $input['type'] == 'xlsx') {
            //download for spreadsheet
            return Excel::create('MotherVesselDispatch Excel', function ($excel) use ($dispatches) {
                $excel->sheet('MotherVesselDispatch sheet', function ($sheet) use ($dispatches) {
                    $sheet->loadView('reports.mother_vessel_dispatches_excel')->with('dispatches', $dispatches)
                        ->row(1, array('Product Type', 'Bill of Laden Number', 'Bill of Laden Qty', 'Mother Vessel', 'Daughter Vessel', 'Dispatched Date'));
                });
            })->download($input['type']);
        } else {
            //download for pdf
            $pdf = PDF::loadView('reports.mother_vessel_dispatch_pdf', ['dispatches' => $dispatches]);
            return $pdf->download('mother_vessel_dispatch_report.pdf');
        }
    }

    public function downloadDispatchExcelFile($type)
    {

        $dispatches = MotherVesselDispatch::all();
        return Excel::create('Mother Vessel Dispatches Report', function ($excel) use ($dispatches) {
            $excel->sheet('Mother Vessel Dispatches Report', function ($sheet) use ($dispatches) {
                $sheet->loadView('reports.mother_vessel_dispatches_excel')->with('dispatches', $dispatches);
                $sheet->row(1, ['Product Type', 'Bill of Laden Number', 'Bill of Laden Qty', 'Mother Vessel', 'Daughter Vessel', 'Dispatched Date']);
            });
        })->download($type);
    }

    public function downloadDispatchPdf()
    {
        $dispatches = MotherVesselDispatch::all();
        $pdf = PDF::loadView('reports.mother_vessel_dispatch_pdf', ['dispatches' => $dispatches]);
        return $pdf->download('mother_vessel_dispatch_report.pdf');
    }
}
