<?php

namespace App\Http\Controllers;

use App\Models\Terminal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TerminalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index()
    {
       $terminals = Terminal::paginate(15);
        $data = [
            "terminals" => $terminals
        ];
        return view("terminal.terminalList", $data);
    }

    public function getTerminalView()
    {
        $user = Auth::user();
        if(!$user->hasRole(['admin'])){
            return back()->withInput()
                ->withErrors("You are not authorize to perform this operation");
        }

        return view("terminal.terminal");
    }


    public function getUploadView()
    {
        $user = Auth::user();
        if(!$user->hasRole(['admin'])){
            return back()->withInput()
                ->withErrors("You are not authorize to perform this operation");
        }
        return view("terminal.uploadTerminal");
    }

    public function createTerminal(Request $request)
    {
        $user = Auth::user();
        if(!$user->hasRole(['admin'])){
            return back()->withInput()
                ->withErrors("You are not authorize to perform this operation");
        }

        $input = $request->all();
       // dd($input);
        $rules = [
            'code' => 'required',
            'name' => 'required',
            'date_installed' => 'required',
            'capacity' => 'required',

        ];
        $input['date_installed'] = date('Y-m-d', strtotime(Input::get('date_installed')));

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator);
        } else {
            $input['status_id'] = 1;
            Terminal::create($input);
        }
        return redirect('terminals/');


    }

    public function getEditedTerminalView($id)
    {
        $user = Auth::user();
        if(!$user->hasRole(['admin'])){
            if($user->id != $id){
                return back()->withInput()
                    ->withErrors("You are not authorize to perform this operation");
            }
        }

        $terminals = Terminal::find($id);
        $terminals->date_installed = date('m/d/Y', strtotime($terminals->date_installed));


        $data = [
            'terminalObj' => $terminals,
        ];
        return view("terminal.terminal", $data);
    }

    public function updateTerminal()
    {
        $input = request()->all();
        $id = decrypt($input['terminal_id']);
        $user = Auth::user();
        if(!$user->hasRole(['admin'])){
            if($user->id != $id){
                return back()->withInput()
                    ->withErrors("You are not authorize to perform this operation");
            }
        }
        $input['date_installed'] = date('Y-m-d', strtotime(Input::get('date_installed')));
        Terminal::find($id)->update(["code" => $input["code"], "name" => $input["name"], "capacity" => $input["capacity"],
            "date_installed" => $input["date_installed"]]);

        return redirect('terminals/');
    }

    public function delete()
    {
        $id = Input::get('id');
        Terminal::find($id)->delete();
        return '200';
    }

    public function generate_verification_code()
    {
        $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $digits = '1234567890';
        $randomString = '';
        for ($i = 0; $i < 3; $i++) {
            $randomString .= $letters[rand(0, strlen($letters) - 1)];
        }
        for ($i = 0; $i < 3; $i++) {
            $randomString .= $digits[rand(0, strlen($digits) - 1)];
        }

        $token = str_shuffle($randomString);
        $obj = Terminal::where("code", $token)->first();
        //dd($obj);
        if ($obj > 0) {
            $this->generate_verification_code();
        }
        return $token;
    }

    public function saveUpload(Request $request)
    {

        if ($request->hasFile('sample_file')) {
            $path = $request->file('sample_file')->getRealPath();
            $data = Excel::load($path)->get();

            if ($data->count()) {
                foreach ($data as $key => $value) {
                    $code = $this->generate_verification_code();
                    $terminal = ['code' => $code, 'name' => $value->name, 'capacity' => $value->capacity,
                        'date_installed' => $value->date_installed, 'status_id' => '1'];
                   Terminal::create($terminal);
                }
                return redirect('/terminals');

            }
        } else {
            dd('Request data does not have any files to import.');
        }

    }

    public function downloadExcelFile($type)
    {
        $terminals = Terminal::all()->toArray();
        return Excel::create('terminal_list', function ($excel) use ($terminals) {
            $excel->sheet('Terminal List', function ($sheet) use ($terminals) {
                $sheet->fromArray($terminals);
            });
        })->download($type);

    }
}