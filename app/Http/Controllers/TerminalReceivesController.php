<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Terminal_receive;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;
use App\Models\Terminal_dispatch;

class TerminalReceivesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index()
    {
        $terminals = Terminal_receive::paginate(15);
        return view('reports.terminal_receives', compact('terminals'));
    }

    public function ViewIndex()
    {
        $input = Input::all();
        $date_from = date('Y-m-d', strtotime($input['from_date']));
        $date_to = date('Y-m-d', strtotime($input['to_date']));
        $terminals = Terminal_receive::whereRaw('bill_date >= ? and bill_date <= ?', [$date_from, $date_to])->paginate(15);
        return view('reports.terminal_dispatches', compact('terminals'));
        if ($input['type'] == 'csv' || $input['type'] == 'xls' || $input['type'] == 'xlsx') {
            //download for spreadsheet
            return Excel::create('Terminal Receives Report', function ($excel) use ($terminals) {
                $excel->sheet('Terminal Receives Report', function ($sheet) use ($terminals) {
                    $sheet->loadView('reports.terminal_receives_exel')->with('terminals', $terminals)
                        ->row(1, array('Product Type', 'Terminal', 'Quantity Dispatched', 'Bill Date'));
                });
            })->download($input['type']);
        } else {
            //download for pdf
            $pdf = PDF::loadView('reports.terminal_receives_pdf', ['terminals' => $terminals]);
            return $pdf->download('terminal_receives_report.pdf');
        }
    }

    public function downloadExcelFile($type)
    {

        $terminals = Terminal_receive::all();

        return Excel::create('Terminal Receives Report', function ($excel) use ($terminals) {
            $excel->sheet('Terminal Receives Report', function ($sheet) use ($terminals) {
                $sheet->loadView('reports.terminal_receives_exel')->with('terminals', $terminals)
                    ->row(1, array('Product Type', 'Terminal', 'Quantity Dispatched', 'Bill Date'));
            });
        })->download($type);

    }

    public function downloadPdf()
    {
        $terminals = Terminal_receive::all();
        $pdf = PDF::loadView('reports.terminal_receives_pdf', ['terminals' => $terminals]);
        return $pdf->download('terminal_receives_report.pdf');
    }

    public function DispatchIndex()
    {
        $terminals = Terminal_dispatch::with('type')->get();
        return view('reports.terminal_dispatches', compact('terminals'));
    }

    public function GetRangeDispatched()
    {
        $input = Input::all();
        $date_from = date('Y-m-d', strtotime($input['from_date']));
        $date_to = date('Y-m-d', strtotime($input['to_date']));
        $terminals = Terminal_dispatch::whereRaw('loading_date >= ? and loading_date <= ?', [$date_from, $date_to])->get();

        if ($input['type'] == 'csv' || $input['type'] == 'xls' || $input['type'] == 'xlsx') {
            //download for spreadsheet
            return Excel::create('Terminal Dispatch Excel', function ($excel) use ($terminals) {
                $excel->sheet('Terminal Dispatch sheet', function ($sheet) use ($terminals) {
                    $sheet->loadView('reports.terminal_dispatches_excel')->with('terminals', $terminals);
                    $sheet->row(1, ['Product Type', 'Bill of Laden Number', 'Bill of Laden Qty', 'Mother Vessel', 'Daughter Vessel', 'Dispatched Date']);
                });
            })->download($input['type']);
        } else {
            //download for pdf
            $pdf = PDF::loadView('reports.terminal_dispatch_pdf', ['terminals' => $terminals]);
            return $pdf->download('terminal_dispatch_report.pdf');
        }
    }

    public function downloadDispatchExcelFile($type)
    {

        $terminals = Terminal_dispatch::all();
        return Excel::create('Terminal Dispatch Report', function ($excel) use ($terminals) {
            $excel->sheet('Terminal Dispatch Report', function ($sheet) use ($terminals) {
                $sheet->loadView('reports.terminal_dispatches_excel')->with('terminals', $terminals);
                $sheet->row(1, ['Product Type', 'Bill of Laden Number', 'Bill of Laden Qty', 'Mother Vessel', 'Daughter Vessel', 'Dispatched Date']);
            });
        })->download($type);
    }

    public function downloadDispatchPdf()
    {
        $terminals = Terminal_dispatch::all();
        $pdf = PDF::loadView('reports.terminal_dispatch_pdf', ['terminals' => $terminals]);
        return $pdf->download('terminal_dispatch_report.pdf');
    }
}
