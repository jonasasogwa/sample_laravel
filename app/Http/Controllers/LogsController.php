<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Logs;

class LogsController extends Controller
{
    public function index(){
        $logs = Logs::with('users')->paginate(10);
        return view('logs.index', compact('logs'));
       
    }

    public function data_log($id)
    {
        $log = Logs::find($id); 
        //return $log;
        $old = $log->old_values;  
        $arr = $log->new_values;
        //return ($log->new_values);
        return view('logs.data-log', compact('arr', 'log', 'old'));
    }
    
}
