<?php

namespace App\Http\Controllers;


use App\Classes\Depot\Depot;
use App\Classes\DaughterVessel\DaughterVessel;
use App\Classes\MotherVessel\MotherVessel;
use App\Classes\Outlet\RetailOutlet;
use App\Models\Daughter_vessel;
use App\Models\DaughterVesselReceive;
use App\Models\Depot as dbDepot;
use App\Models\DepotReceives as dbDepotReceived;
use App\Models\MotherVesselReceive;
use App\Models\Outlet_receive as dbOutletReceive;
use App\Models\DepotOutlet as dbDepotOutlet;
use App\Models\Outlet_tank;
use App\Models\Terminal_receive as dbTerminalReceives;

use App\Models\MotherVessel as dbMotherVessel;
use App\Models\MotherVesselDispatch as dbMotherVesselDispatch;
use App\Models\DaughterVesselDispatch as dbDaughterVesselDispatch;
use App\Models\DaughterVessel as dbDaughterVessel;
use App\Models\DaughterVesselReceive as dbDaughterVesselReceive;
use App\Models\Outlet_hcmb;
use App\Models\Retail_outlet;
use App\Models\Terminal as dbTerminal;
use App\Models\Terminal_dispatch as dbTerminalDispatch;
use App\Models\Terminal_receive;
use App\Models\Type;
use Illuminate\Support\Facades\Input;
use App\Classes\Terminal\Terminal;

class ReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:web');
    }


    public function getOutletWithWithoutProductView()
    {
        $list["0"] = 'Choose Types';
        $types = [];
        $objs = Type::all();
        foreach ($objs as $obj) {
            $types[$obj->id] = $obj->name;
        }
        $types = $this->appendArray($list, $types);
        $data = ['types' => $types];

        return view("reports.outlet_with_without_product", $data);
    }

    public function getOutletWithWithoutProduct()
    {
        $date_from = date('Y-m-d', strtotime(Input::get('from_date')));
        $to_date = date('Y-m-d', strtotime(Input::get('to_date')));
        $type_id = Input::get('type_id');
        $report_id = Input::get('report_id');
   //     $source_type_id = Input::get('source_type_id');
        $list = [];
        $retails = Retail_outlet::all();
        if(count($retails) > 0){
            foreach ($retails as $retail){

                if($report_id == 1){
                   //with product
                    $query = Outlet_hcmb::whereBetween('date', [$to_date, $date_from])
                        ->join('outlet_tanks', 'outlet_tanks.id', '=', 'outlet_hcmbs.outlet_tank_id')
                        ->join('retail_outlets', 'retail_outlets.id', '=', 'outlet_tanks.outlet_id')
                        ->where('outlet_tanks.outlet_id', '=', $retail->id)
                        ->where('outlet_tanks.type_id', '=', $type_id)
                        ->where('outlet_hcmbs.vol', '>', 0)
                        ->get(['retail_outlets.name', 'retail_outlets.id']);
                    if(count($query) > 0){
                        $outlet = new RetailOutlet();
                        $outlet->setName($retail->name);
                        $outlet->setBalance($this->getBalance($retail->id));
                        $outlet->setAddress($retail->address);
                        $outlet->setCapacity($retail->capacity);
                        $list[] = $outlet->morphToJSON();
                    }
                }

                if($report_id == 2){
                   //without product
                    $query = Outlet_hcmb::whereBetween('date', [$to_date, $date_from])
                        ->join('outlet_tanks', 'outlet_tanks.id', '=', 'outlet_hcmbs.outlet_tank_id')
                        ->join('retail_outlets', 'retail_outlets.id', '=', 'outlet_tanks.outlet_id')
                        ->where('outlet_tanks.outlet_id', '=', $retail->id)
                        ->where('outlet_tanks.type_id', '=', $type_id)
                        ->where('outlet_hcmbs.vol', '<', 1)
                        ->get();
                    if(count($query) > 0){
                        $outlet = new RetailOutlet();
                        $outlet->setName($retail->name);
                        $outlet->setBalance(0);
                        $outlet->setAddress($retail->address);
                        $outlet->setCapacity($retail->capacity);
                        $list[] = $outlet->morphToJSON();
                    }
                }
                             //get the balance
            }
        }
        return $list;
    }

    public function productDispatchedNotReceivedView()
    {
        return view("reports.product_dispatched_not_received");
    }


    public function getSourceTypeNames()
    {
        $source_type = Input::get('source_type');
        $list = [];
        // dd($source_type);
        switch ($source_type) {
            case 'mother_vessel':
                $objs = dbMotherVessel::all();
                foreach ($objs as $obj) {
                    $list[$obj->id] = $obj->vessel_name;
                }
                break;
            case 'daughter_vessel':
                $objs = dbDaughterVessel::all();
                foreach ($objs as $obj) {
                    $list[$obj->id] = $obj->name;
                }
                break;
            case 'terminal':
                $objs = dbTerminal::all();
                foreach ($objs as $obj) {
                    $list[$obj->id] = $obj->name;
                }
                break;
            case 'depot':
                $objs = dbDepot::all();
                foreach ($objs as $obj) {
                    $list[$obj->id] = $obj->name;
                }
                break;
            default:
                $objs = Retail_outlet::all();
                foreach ($objs as $obj) {
                    $list[$obj->id] = $obj->name;
                }
                break;
        }


        return $list;
    }

    public function getProductDispatchNotReceivedReport()
    {
        $date_from = date('Y-m-d', strtotime(Input::get('from_date')));
        $to_date = date('Y-m-d', strtotime(Input::get('to_date')));
        $source_type = Input::get('source_type');
        $source_name_id = Input::get('source_name_id');
        $list = [];

        if ($source_type == 'mother_vessel') {
            if ($source_name_id == 0) {
                $vessels = dbMotherVesselDispatch::with(['daughter_vessel', 'mother_vessel'])->whereRaw('dispatch_date >= ? and dispatch_date <= ?', [$date_from, $to_date])->get();

            } else {
                $vessels = dbMotherVesselDispatch::with(['daughter_vessel', 'mother_vessel', 'type'])->whereRaw('dispatch_date >= ? and dispatch_date <= ? and mother_vessel_id = ?', [$date_from, $to_date, $source_name_id])->get();
            }

            if (count($vessels) > 0) {
                foreach ($vessels as $vessel) {
                    $vesselReceive = dbDaughterVesselReceive::whereRaw("bill_of_laden_no = ?", [$vessel->bill_of_laden_no])->first();
                    if (count($vesselReceive) < 1) {
                        $obj = new MotherVessel();
                        $obj->setDaughterVessel($vessel->daughter_vessel->name);
                        $obj->setName($vessel->mother_vessel->vessel_name);
                        $obj->setProduct($vessel->type->name);
                        $obj->setBillOfLadenNo($vessel->bill_of_laden_no);
                        $obj->setDispatchDate($vessel->dispatch_date);
                        $list[] = $obj->morphToJSON();
                    }
                }
            }
        }

        if ($source_type == 'daughter_vessel') {
            if ($source_name_id == 0) {
                $vessels = dbDaughterVesselDispatch::with(['daughter_vessel', 'terminal', 'type'])->whereRaw('dispatch_date >= ? and dispatch_date <= ?', [$date_from, $to_date])->get();

            } else {
                $vessels = dbDaughterVesselDispatch::with(['daughter_vessel', 'terminal', 'type'])->whereRaw('dispatch_date >= ? and dispatch_date <= ? and daughter_vessel_id = ?', [$date_from, $to_date, $source_name_id])->get();
            }

            if (count($vessels) > 0) {
                foreach ($vessels as $vessel) {
                    $terminalReceive = dbTerminalReceives::whereRaw("bill_of_laden_no = ?", [$vessel->bill_of_laden_no])->first();
                    if (count($terminalReceive) < 1) {
                        $obj = new DaughterVessel();
                        $obj->setName($vessel->daughter_vessel->name);
                        $obj->setTerminal($vessel->terminal->name);
                        $obj->setProduct($vessel->type->name);
                        $obj->setBillOfLadenNo($vessel->bill_of_laden_no);
                        $obj->setDispatchDate($vessel->dispatch_date);
                        $list[] = $obj->morphToJSON();
                    }
                }
            }
        }
        if ($source_type == 'terminal') {
            if ($source_name_id == 0) {
                $terminals = dbTerminalDispatch::with(['terminal', 'outlet', 'depot', 'dispatch_type'])->whereRaw('loading_date >= ? and loading_date <= ?', [$date_from, $to_date])->get();
            } else {
                $terminals = dbTerminalDispatch::with(['terminal', 'outlet', 'depot', 'dispatch_type'])->whereRaw('loading_date >= ? and loading_date <= ? and terminal_id = ?', [$date_from, $to_date, $source_name_id])->get();
            }

            if (count($terminals) > 0) {
                foreach ($terminals as $terminal) {
                    //depot is 1 outlet is 2
                    if ($terminal->dispatch_type_id == 1) {
                        $depot = dbDepotReceived::whereRaw("waybill_no = ?", [$terminal->waybill_no])->first();
                        if (count($depot) < 1) {
                            $terminalObj = new Terminal();
                            $terminalObj->setDepot($terminal->depot->name);
                            $terminalObj->setTerminal($terminal->terminal->name);
                            $terminalObj->setWayBillNo($terminal->waybill_no);
                            $terminalObj->setMeterTicketNo($terminal->meter_ticket_no);
                            $terminalObj->setEstDateOfArrival($terminal->est_date_of_arrival);
                            $terminalObj->setProductQty($terminal->product_qty);
                            $terminalObj->setDispatchType($terminal->dispatch_type->name);
                            $list[] = $terminalObj->morphToJSON();
                        }
                    }
                    if ($terminal->dispatch_type_id == 2) {
                        $outletReceive = dbOutletReceive::whereRaw("waybill_no = ?", [$terminal->waybill_no])->first();
                        if (count($outletReceive) < 1) {
                            $terminalObj = new Terminal();
                            $terminalObj->setOutlet($terminal->outlet->name);
                            $terminalObj->setTerminal($terminal->terminal->name);
                            $terminalObj->setWayBillNo($terminal->waybill_no);
                            $terminalObj->setMeterTicketNo($terminal->meter_ticket_no);
                            $terminalObj->setEstDateOfArrival($terminal->est_date_of_arrival);
                            $terminalObj->setProductQty($terminal->product_qty);
                            $terminalObj->setDispatchType($terminal->dispatch_type->name);
                            $list[] = $terminalObj->morphToJSON();
                        }
                    }
                }
            } else {

            }
        }

        if ($source_type == 'depot') {
            if ($source_name_id == 0) {
                $dpOutlets = dbDepotOutlet::with(['outlet', 'depot'])->whereRaw('loading_date >= ? and loading_date <= ?', [$date_from, $to_date])->get();

            } else {
                $dpOutlets = dbDepotOutlet::with(['outlet', 'depot'])->whereRaw('loading_date >= ? and loading_date <= ? and retail_outlet_id = ?', [$date_from, $to_date, $source_name_id])->get();
            }

            if (count($dpOutlets) > 0) {
                foreach ($dpOutlets as $dpOutlet) {
                    $depot = dbOutletReceive::whereRaw("waybill_no = ?", [$dpOutlet->waybill_no])->first();
                    if (count($depot) < 1) {
                        $obj = new Depot();
                        $obj->setDepot($dpOutlet->depot->name);
                        $obj->setRetailOutlet($dpOutlet->outlet->name);
                        $obj->setWayBillNo($dpOutlet->waybill_no);
                        $obj->setMeterTicketNumber($dpOutlet->meter_ticket_number);
                        $obj->setProductQty($dpOutlet->product_qty);
                        $list[] = $obj->morphToJSON();
                    }
                }
            }
        }
        return $list;
    }


    public function getConsolidatedReportView()
    {
        return view("reports.consolidated_report");
    }

    public function getConsolidatedReport()
    {
        $date_from = date('Y-m-d', strtotime(Input::get('from_date')));
        $to_date = date('Y-m-d', strtotime(Input::get('to_date')));
        $source_type = Input::get('source_type');
        $source_name_id = Input::get('source_name_id');
        $list = [];
        $types = Type::all();
        if ($source_type == 'mother_vessel') {
            foreach ($types as $type) {
                if ($source_name_id == 0) {
                    $receives = MotherVesselReceive::whereRaw('arrival_date >= ? and arrival_date <= ? and type_id = ?', [$date_from, $to_date, $type->id])->get();
                    $dispatches = dbMotherVesselDispatch::with(['daughter_vessel', 'mother_vessel'])->whereRaw('dispatch_date >= ? and dispatch_date <= ? and type_id = ?', [$date_from, $to_date, $type->id])->get();
                } else {
                    $receives = MotherVesselReceive::whereRaw('arrival_date >= ? and arrival_date <= ? and type_id = ? and mother_vessel_id = ?', [$date_from, $to_date, $type->id, $source_name_id])->get();
                    $dispatches = dbMotherVesselDispatch::with(['daughter_vessel', 'mother_vessel'])->whereRaw('dispatch_date >= ? and dispatch_date <= ? and mother_vessel_id = ? and type_id = ?', [$date_from, $to_date, $source_name_id, $type->id])->get();
                }
                $totalReceives = 0;

                if (count($receives) > 0) {
                    foreach ($receives as $receive) {
                        $totalReceives += $receive->out_turn_qty;
                    }
                }

                $totalDispatches = 0;

                if (count($dispatches) > 0) {
                    foreach ($dispatches as $dispatch) {
                        $totalDispatches += $dispatch->bill_of_laden_qty;
                    }
                }

                $motherVessel = new MotherVessel();
                $motherVessel->setProduct($type->name);
                $motherVessel->setDispatchQty($totalDispatches);
                $motherVessel->setReceiveQty($totalReceives);
                $list[] = $motherVessel->morphToJSON();
            }
        }
        if ($source_type == 'daughter_vessel') {
            foreach ($types as $type) {
                if ($source_name_id == 0) {
                    $receives = DaughterVesselReceive::whereRaw('loading_date >= ? and loading_date <= ? and type_id = ?', [$date_from, $to_date, $type->id])->get();
                    $dispatches = dbDaughterVesselDispatch::whereRaw('dispatch_date >= ? and dispatch_date <= ? and type_id = ?', [$date_from, $to_date, $type->id])->get();
                } else {
                    $receives = DaughterVesselReceive::whereRaw('loading_date >= ? and loading_date <= ? and type_id = ? and daughter_vessel_id = ?', [$date_from, $to_date, $type->id, $source_name_id])->get();
                    $dispatches = dbDaughterVesselDispatch::whereRaw('dispatch_date >= ? and dispatch_date <= ? and daughter_vessel_id = ? and type_id = ?', [$date_from, $to_date, $source_name_id, $type->id])->get();
                }
                $totalReceives = 0;

                if (count($receives) > 0) {
                    foreach ($receives as $receive) {
                        $totalReceives += $receive->bill_of_laden_qty;
                    }
                }

                $totalDispatches = 0;

                if (count($dispatches) > 0) {
                    foreach ($dispatches as $dispatch) {
                        $totalDispatches += $dispatch->qty_dispatched;
                    }
                }

                $vessel = new DaughterVessel();
                $vessel->setProduct($type->name);
                $vessel->setDispatchQty($totalDispatches);
                $vessel->setReceiveQty($totalReceives);
                $list[] = $vessel->morphToJSON();
            }
        }
        if ($source_type == 'terminal') {
            foreach ($types as $type) {
                if ($source_name_id == 0) {
                    $receives = dbTerminalReceives::whereRaw('bill_date >= ? and bill_date <= ? and type_id = ?', [$date_from, $to_date, $type->id])->get();
                    $dispatches = dbTerminalDispatch::whereRaw('loading_date >= ? and loading_date <= ? and type_id = ?', [$date_from, $to_date, $type->id])->get();
                } else {
                    $receives = dbTerminalReceives::whereRaw('bill_date >= ? and bill_date <= ? and type_id = ? and terminal_id = ?', [$date_from, $to_date, $type->id, $source_name_id])->get();
                    $dispatches = dbTerminalDispatch::whereRaw('loading_date >= ? and loading_date <= ? and terminal_id = ? and type_id = ?', [$date_from, $to_date, $source_name_id, $type->id])->get();
                }
                $totalReceives = 0;

                if (count($receives) > 0) {
                    foreach ($receives as $receive) {
                        $totalReceives += $receive->qty_dispatch;
                    }
                }

                $totalDispatches = 0;

                if (count($dispatches) > 0) {
                    foreach ($dispatches as $dispatch) {
                        $totalDispatches += $dispatch->product_qty;
                    }
                }

                $vessel = new Terminal();
                $vessel->setProduct($type->name);
                $vessel->setDispatchQty($totalDispatches);
                $vessel->setReceiveQty($totalReceives);
                $list[] = $vessel->morphToJSON();
            }
        }

        if ($source_type == 'depot') {
            foreach ($types as $type) {
                if ($source_name_id == 0) {
                    $receives = dbDepotReceived::whereRaw('date_of_arrival >= ? and date_of_arrival <= ? and type_id = ?', [$date_from, $to_date, $type->id])->get();
                    $dispatches = dbDepotOutlet::whereRaw('loading_date >= ? and loading_date <= ? and type_id = ?', [$date_from, $to_date, $type->id])->get();
                } else {
                    $receives = dbDepotReceived::whereRaw('date_of_arrival >= ? and date_of_arrival <= ? and type_id = ? and depot_id = ?', [$date_from, $to_date, $type->id, $source_name_id])->get();
                    $dispatches = dbDepotOutlet::whereRaw('loading_date >= ? and loading_date <= ? and depot_id = ? and type_id = ?', [$date_from, $to_date, $source_name_id, $type->id])->get();
                }
                $totalReceives = 0;

                if (count($receives) > 0) {
                    foreach ($receives as $receive) {
                        $totalReceives += $receive->product_qty;
                    }
                }

                $totalDispatches = 0;

                if (count($dispatches) > 0) {
                    foreach ($dispatches as $dispatch) {
                        $totalDispatches += $dispatch->product_qty;
                    }
                }

                $vessel = new Depot();
                $vessel->setProduct($type->name);
                $vessel->setDispatchQty($totalDispatches);
                $vessel->setReceiveQty($totalReceives);
                $list[] = $vessel->morphToJSON();
            }
        }
        return $list;
    }


    public function appendArray($base_array, ...$arrays_to_join)
    {
        if (!is_array($base_array)) {
            return $base_array;
        }
        foreach ($arrays_to_join as $sec_array) {
            if (!is_array($sec_array))
                continue;
            foreach ($sec_array as $key => $value) {
                $base_array[$key] = $value;
            }
        }
        return $base_array;


    }

    public function getBalance($outlet_id){
       $outlet_tank_ids= Outlet_tank::whereRaw('outlet_id', [$outlet_id])->get()->pluck("id")->toArray();
        $obj = Outlet_hcmb::whereIn('outlet_tank_id', $outlet_tank_ids)
            ->orderBy('date', 'desc')
            ->orderBy('time', 'desc')
            ->first();
        return $obj->vol;
    }
}