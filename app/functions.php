<?php


/**
 * Appends arrays together
 * @param $base_array
 * @param ...$arrays_to_join
 * @return mixed - joined array
 */
function appendArray($base_array, ...$arrays_to_join)
{
    if (!is_array($base_array)) {
        return $base_array;
    }
    foreach ($arrays_to_join as $sec_array) {
        if (!is_array($sec_array))
            continue;
        foreach ($sec_array as $key => $value) {
            $base_array[$key] = $value;
        }
    }
    return $base_array;
}


