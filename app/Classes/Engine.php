<?php

namespace App\Classes;



class Engine {


    /**
     * Convert model to Json String
     * @param $objList
     * @return string
     */
    public function jsonfy($objList){
        foreach($objList as $i=>$obj){
            if(is_object($obj)){
                $o = $obj->__toString();
                if(self::isJson($o)){
                    $o = json_decode($o);
                }
                $objList[$i] = $o;
            }
            elseif(is_array($obj)){
                $o = $this->jsonfy($obj);
                if(self::isJson($o)){
                    $o = json_decode($o);
                }
                $objList[$i] = $o;
            }

        }
        return json_encode($objList, JSON_PRETTY_PRINT);
    }

    /**
     * Check if an object is json
     * @param $string
     * @return bool return true if json , false otherwise
     */
    public static function  isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

}