<?php

namespace App\Classes\Terminal;

use App\Classes\Engine;


class Terminal Extends Engine
{
    private $id;
    private $dispatchType;
    private $wayBillNo;
    private $loadingDate;
    private $driverName;
    private $truckNo;
    private $productQty;
    private $depot;
    private $meterTicketNo;
    private $acquilaCode;
    private $estDateOfArrival;
    private $terminal;
    private $outlet;
    private $product;
    private $receiveQty;
    private $dispatchQty;

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getReceiveQty()
    {
        return $this->receiveQty;
    }

    /**
     * @param mixed $receiveQty
     */
    public function setReceiveQty($receiveQty)
    {
        $this->receiveQty = $receiveQty;
    }

    /**
     * @return mixed
     */
    public function getDispatchQty()
    {
        return $this->dispatchQty;
    }

    /**
     * @param mixed $dispatchQty
     */
    public function setDispatchQty($dispatchQty)
    {
        $this->dispatchQty = $dispatchQty;
    }

    /**
     * @return mixed
     */
    public function getOutlet()
    {
        return $this->outlet;
    }

    /**
     * @param mixed $outlet
     */
    public function setOutlet($outlet)
    {
        $this->outlet = $outlet;
    }

    /**
     * @return mixed
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param mixed $terminal
     */
    public function setTerminal($terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDispatchType()
    {
        return $this->dispatchType;
    }

    /**
     * @param mixed $dispatchType
     */
    public function setDispatchType($dispatchType)
    {
        $this->dispatchType = $dispatchType;
    }

    /**
     * @return mixed
     */
    public function getWayBillNo()
    {
        return $this->wayBillNo;
    }

    /**
     * @param mixed $wayBillNo
     */
    public function setWayBillNo($wayBillNo)
    {
        $this->wayBillNo = $wayBillNo;
    }

    /**
     * @return mixed
     */
    public function getLoadingDate()
    {
        return $this->loadingDate;
    }

    /**
     * @param mixed $loadingDate
     */
    public function setLoadingDate($loadingDate)
    {
        $this->loadingDate = $loadingDate;
    }

    /**
     * @return mixed
     */
    public function getDriverName()
    {
        return $this->driverName;
    }

    /**
     * @param mixed $driverName
     */
    public function setDriverName($driverName)
    {
        $this->driverName = $driverName;
    }

    /**
     * @return mixed
     */
    public function getTruckNo()
    {
        return $this->truckNo;
    }

    /**
     * @param mixed $truckNo
     */
    public function setTruckNo($truckNo)
    {
        $this->truckNo = $truckNo;
    }

    /**
     * @return mixed
     */
    public function getProductQty()
    {
        return $this->productQty;
    }

    /**
     * @param mixed $productQty
     */
    public function setProductQty($productQty)
    {
        $this->productQty = $productQty;
    }

    /**
     * @return mixed
     */
    public function getDepot()
    {
        return $this->depot;
    }

    /**
     * @param mixed $depot
     */
    public function setDepot($depot)
    {
        $this->depot = $depot;
    }

    /**
     * @return mixed
     */
    public function getMeterTicketNo()
    {
        return $this->meterTicketNo;
    }

    /**
     * @param mixed $meterTicketNo
     */
    public function setMeterTicketNo($meterTicketNo)
    {
        $this->meterTicketNo = $meterTicketNo;
    }

    /**
     * @return mixed
     */
    public function getAcquilaCode()
    {
        return $this->acquilaCode;
    }

    /**
     * @param mixed $acquilaCode
     */
    public function setAcquilaCode($acquilaCode)
    {
        $this->acquilaCode = $acquilaCode;
    }

    /**
     * @return mixed
     */
    public function getEstDateOfArrival()
    {
        return $this->estDateOfArrival;
    }

    /**
     * @param mixed $estDateOfArrival
     */
    public function setEstDateOfArrival($estDateOfArrival)
    {
        $this->estDateOfArrival = $estDateOfArrival;
    }





    public function morphToJSON(){
        $var = get_object_vars($this);
        foreach($var as &$value){
            if(is_object($value) && method_exists($value,'morphToJSON')){
                $value = $value->morphToJSON();
            }
        }
        return $var;
    }




    function __toString()
    {
        return $this->jsonfy(get_object_vars($this));
    }
}