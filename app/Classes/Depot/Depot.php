<?php

namespace App\Classes\Depot;

use App\Classes\Engine;


class Depot Extends Engine
{
    private $id;
    private $depot;
    private $retail_outlet;
    private $transporter_name;
    private $meter_ticket_number;
    private $waybill_no;
    private $productQty;
    private $date_of_arrival;
    private $product;
    private $receiveQty;
    private $dispatchQty;

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getReceiveQty()
    {
        return $this->receiveQty;
    }

    /**
     * @param mixed $receiveQty
     */
    public function setReceiveQty($receiveQty)
    {
        $this->receiveQty = $receiveQty;
    }

    /**
     * @return mixed
     */
    public function getDispatchQty()
    {
        return $this->dispatchQty;
    }

    /**
     * @param mixed $dispatchQty
     */
    public function setDispatchQty($dispatchQty)
    {
        $this->dispatchQty = $dispatchQty;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDepot()
    {
        return $this->depot;
    }

    /**
     * @param mixed $depot
     */
    public function setDepot($depot)
    {
        $this->depot = $depot;
    }

    /**
     * @return mixed
     */
    public function getRetailOutlet()
    {
        return $this->retail_outlet;
    }

    /**
     * @param mixed $retail_outlet
     */
    public function setRetailOutlet($retail_outlet)
    {
        $this->retail_outlet = $retail_outlet;
    }

    /**
     * @return mixed
     */
    public function getTransporterName()
    {
        return $this->transporter_name;
    }

    /**
     * @param mixed $transporter_name
     */
    public function setTransporterName($transporter_name)
    {
        $this->transporter_name = $transporter_name;
    }

    /**
     * @return mixed
     */
    public function getMeterTicketNumber()
    {
        return $this->meter_ticket_number;
    }

    /**
     * @param mixed $meter_ticket_number
     */
    public function setMeterTicketNumber($meter_ticket_number)
    {
        $this->meter_ticket_number = $meter_ticket_number;
    }

    /**
     * @return mixed
     */
    public function getWaybillNo()
    {
        return $this->waybill_no;
    }

    /**
     * @param mixed $waybill_no
     */
    public function setWaybillNo($waybill_no)
    {
        $this->waybill_no = $waybill_no;
    }

    /**
     * @return mixed
     */
    public function getProductQty()
    {
        return $this->productQty;
    }

    /**
     * @param mixed $productQty
     */
    public function setProductQty($productQty)
    {
        $this->productQty = $productQty;
    }

    /**
     * @return mixed
     */
    public function getDateOfArrival()
    {
        return $this->date_of_arrival;
    }

    /**
     * @param mixed $date_of_arrival
     */
    public function setDateOfArrival($date_of_arrival)
    {
        $this->date_of_arrival = $date_of_arrival;
    }





    public function morphToJSON(){
        $var = get_object_vars($this);
        foreach($var as &$value){
            if(is_object($value) && method_exists($value,'morphToJSON')){
                $value = $value->morphToJSON();
            }
        }
        return $var;
    }




    function __toString()
    {
        return $this->jsonfy(get_object_vars($this));
    }
}