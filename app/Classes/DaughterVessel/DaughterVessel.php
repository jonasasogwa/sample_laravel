<?php

namespace App\Classes\DaughterVessel;

use App\Classes\Engine;


class DaughterVessel Extends Engine
{
    private $id;
    private $name;
    private $dispatchDate;
    private $billOfLadenNo;
    private $terminal;
    private $product;
    private $receiveQty;
    private $dispatchQty;

    /**
     * @return mixed
     */
    public function getReceiveQty()
    {
        return $this->receiveQty;
    }

    /**
     * @param mixed $receiveQty
     */
    public function setReceiveQty($receiveQty)
    {
        $this->receiveQty = $receiveQty;
    }

    /**
     * @return mixed
     */
    public function getDispatchQty()
    {
        return $this->dispatchQty;
    }

    /**
     * @param mixed $dispatchQty
     */
    public function setDispatchQty($dispatchQty)
    {
        $this->dispatchQty = $dispatchQty;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDispatchDate()
    {
        return $this->dispatchDate;
    }

    /**
     * @param mixed $dispatchDate
     */
    public function setDispatchDate($dispatchDate)
    {
        $this->dispatchDate = $dispatchDate;
    }

    /**
     * @return mixed
     */
    public function getBillOfLadenNo()
    {
        return $this->billOfLadenNo;
    }

    /**
     * @param mixed $billOfLadenNo
     */
    public function setBillOfLadenNo($billOfLadenNo)
    {
        $this->billOfLadenNo = $billOfLadenNo;
    }

    /**
     * @return mixed
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param mixed $terminal
     */
    public function setTerminal($terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    public function morphToJSON(){
        $var = get_object_vars($this);
        foreach($var as &$value){
            if(is_object($value) && method_exists($value,'morphToJSON')){
                $value = $value->morphToJSON();
            }
        }
        return $var;
    }




    function __toString()
    {
        return $this->jsonfy(get_object_vars($this));
    }

}