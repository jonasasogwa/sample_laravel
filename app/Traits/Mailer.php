<?php

namespace App\Traits;

use Mail;

trait Mailer
{
    protected function notifyAdmins($params)
    {
        try {
            $this->sendMail('emails.notification', $params, 'Error Notification');
        } catch (\Exception $e) {
        }
    }

    protected function sendWelcomeEmail($params)
    {
        try {
            $this->sendMail('emails.welcome', $params, 'Welcome to  NHCMB');
        } catch (\Exception $e) {
            $this->notifyAdmins([
                'last_name' => 'Admin',
                'first_name' => 'NNPC ',
                'body' => $e->getMessage(),
                'email' => 'info@ikooba.com'
            ]);
        }
    }


    protected function sendMail($template, $params, $subject)
    {
        $params['subject'] = $subject;
        Mail::send($template, $params, function ($m) use ($params, $template) {
            $m->from(config('mail.from.address'),config('mail.from.name'));
            $m->to($params['email'], $params['first_name'] . ' ' . $params['last_name'])->subject($params['subject']);

            try {
                if ($template == 'emails.welcome') {
                    if (!empty(config('constants.welcomeBcc'))) {
                        $welcomeEmail = config('constants.welcomeBcc');
                        foreach ($welcomeEmail as $eachOne) {
                            $m->bcc($eachOne);
                        }

                    }
                }
            } catch (\Exception $ex) {

            }
        });
    }



}

