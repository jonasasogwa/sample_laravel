<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Terminal extends Model implements Auditable
{

    protected $table = 'terminals';

    use \OwenIt\Auditing\Auditable;

    protected $fillable = ['code','name','status_id','capacity', 'date_installed'];

//    public function status(){
//        return $this->belongsTo('App\Status', 'status_id');
//    }

    public function terminal_receive(){
        return $this->hasMany('App\Models\Terminal_receive');
    }

//    public function depot_receives(){
//        return $this->belongsToMany('App\Models\DepotReceives');
//    }


}
