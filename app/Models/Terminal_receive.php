<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Terminal_receive extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'terminal_receives';

    protected $fillable = [
        'type_id',
        'terminal_id',
        'qty_dispatch',
        'daughter_vessel',
        'user_id',
        'bill_date',
        'updated_at'
    ];

    public function type(){
        return $this->belongsTo('App\Models\Type');
    }

    public function terminal(){
        return $this->belongsTo('App\Models\Terminal');
    }

    public function daughter_vessel(){
        return $this->belongsTo('App\Models\Daughter_vessel');
    }
}
