<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class DaughterVessel extends Model
{

    public $table = 'daughter_vessels';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name'
    ];

    public function mother_dispatched(){
        return $this->hasMany('App\Models\MotherVesselDispatch','daughter_vessel_id');
    }
    
}
