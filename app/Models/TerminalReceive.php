<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
/**
 * Class TerminalReceive
 * @package App\Models
 * @version February 16, 2018, 10:58 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property integer type_id
 * @property integer terminal_id
 * @property date bill_date
 * @property integer qty_dispatch
 * @property string daughter_vessel
 */
class TerminalReceive extends Model
{

    public $table = 'terminal_receives';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'type_id',
        'terminal_id',
        'bill_date',
        'qty_dispatch',
        'daughter_vessel'
    ];

    public function terminal()
    {
        return $this->belongsTo('App\Models\Terminal', 'terminal_id');
    }
    
}
