<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class MotherVesselReceive extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = ['type_id','bill_of_laden_no','bill_of_laden_qty','out_turn_qty','daughter_vessel_id','arrival_date'];
    protected $primaryKey = 'id';
    protected $table = 'mother_vessel_receives';

    public function type(){
       return $this->belongsTo('App\Models\Type');
    }

}
