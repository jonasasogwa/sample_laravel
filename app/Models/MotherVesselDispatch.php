<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MotherVesselDispatch
 * @package App\Models
 * @version February 23, 2018, 2:36 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property integer daughter_vessel_id
 * @property integer mother_vessel_id
 * @property integer bill_of_laden_qty
 * @property integer type_id
 * @property integer dispatch_date
 */
class MotherVesselDispatch extends Model
{

    public $table = 'mother_vessel_dispatches';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'daughter_vessel_id',
        'mother_vessel_id',
        'bill_of_laden_qty',
        'type_id',
        'dispatch_date',
        'user_id',
        'status_id',
        'bill_of_laden_no'
    ];

    public function mother_vessel()
    {
        return $this->belongsTo('App\Models\MotherVessel');
    }


    public function daughter_vessel()
    {
        return $this->belongsTo('App\Models\DaughterVessel');
    }

    public function type(){
        return $this->belongsTo('App\Models\Type');
    }


}
