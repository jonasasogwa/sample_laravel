<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Outlet_hcmb extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'outlet_hcmbs';

    protected $fillable = ['date','time','outlet_tank_id','vol','user_id'];


}
