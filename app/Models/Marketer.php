<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Marketer extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = ['code', 'address', 'status_id', 'user_id', 'type_id', 'name'];
    protected $primaryKey = 'id';
    protected $table = 'marketers';


    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }


}
