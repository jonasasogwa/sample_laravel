<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Retail_outlet extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    protected $fillable = ['code','name','address','city_id', 'marketer_id', 'user_id',
    'nearest_to_depot','distant_to_depot','tank_no','capacity','pump_no',
    'date_of_purchase','low_demand_season','high_demand_season','status_id'];
    protected $primaryKey = 'id';
    protected $table = 'retail_outlets';
}
