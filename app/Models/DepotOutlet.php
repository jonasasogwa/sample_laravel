<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
/**
 * Class DepotOutlet
 * @package App\Models
 * @version February 17, 2018, 1:38 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property integer depot_id
 * @property integer retail_outlet_id
 * @property date loading_date
 * @property string marketer_id
 * @property string transporter_name
 * @property string truck_number
 * @property string meter_ticket_number
 * @property string product_qty
 * @property string waybill_no
 * @property string waybill_qty
 * @property string driver_name
 * @property string driver_number
 * @property date date_of_arrival
 */
class DepotOutlet extends Model
{

    public $table = 'depot_outlets';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'depot_id',
        'retail_outlet_id',
        'loading_date',
        'marketer_id',
        'transporter_name',
        'truck_number',
        'meter_ticket_number',
        'product_qty',
        'waybill_no',
        'waybill_qty',
        'driver_name',
        'driver_number',
        'date_of_arrival',
        'user_id',
        'type_id'
    ];

    public function outlet()
    {
        return $this->belongsTo('App\Models\Retail_outlet', 'retail_outlet_id');
    }

    public function depot()
    {
        return $this->belongsTo('App\Models\Depot', 'depot_id');
    }

}
