<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public $table = 'statuses';

    public $fillable = [
        'name'
    ];

    public function user(){
        return $this->hasMany('App\Models\User');
    }
}
