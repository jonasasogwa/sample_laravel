<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Logs extends Model
{
    protected $table = 'audits';
    //

    public function users()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    

}
