<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MotherVesselDispatches extends Model
{

    protected $table = 'mother_vessel_dispatches';

    protected $fillable = [
        'bill_of_laden_no','status_id','bill_of_laden_qty',
        'daughter_vessel_id','mother_vessel_id','type_id',
        'dispatch_date','user_id'
    ];

    public function type(){
        return $this->belongsTo('App\Models\Type');
    }

}
