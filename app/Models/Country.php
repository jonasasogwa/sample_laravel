<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    function nigerian_state()
    {
        return $this->hasMany('App\Models\State', 'country_id')->where('id', 160);
    }
}
