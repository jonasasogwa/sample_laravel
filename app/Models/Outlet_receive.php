<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Outlet_receive extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'outlet_receives';

    protected $fillable = [
        'dispatch_no','truck_no','date_time_dispatch','product_qty','driver_name',
        'driver_phone','outlet_id','depot_id','terminal_id','retail_outlet_id',
        'estimated_arrival_date','waybill_no','user_id'
    ];

}
