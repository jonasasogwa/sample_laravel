<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use OwenIt\Auditing\Contracts\Auditable;
use App\Notifications\MyOwnResetPassword as ResetPasswordNotification;

class User extends Authenticatable implements Auditable
{
    use Notifiable, EntrustUserTrait;
    use \OwenIt\Auditing\Auditable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'name', 'email', 'password', 'verification_code', 'date_registered', 'last_login', 'type_id', 'status_id'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user_profile()
    {
        return $this->hasOne('App\Models\User_profile');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'role_user');
    }

    public function marketers()
    {
        return $this->hasOne('App\Models\Marketer', 'user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }


    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token, $this->getEmailForPasswordReset()));
    }
}
