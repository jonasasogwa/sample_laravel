<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class MotherVessel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'mother_vessels';
    protected $fillable = ['vessel_no','vessel_name'];
    protected $primaryKey = 'id';

    public function daughter_vessel_receives(){
        return $this->hasMany('App\Models\DaughterVesselReceive','mother_vessel_id');
    }

    public function mother_dispatches(){
        return $this->hasMany('App\Models\MotherVesselDispatch','mother_vessel_id');
    }

}
