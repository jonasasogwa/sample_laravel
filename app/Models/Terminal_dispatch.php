<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Terminal_dispatch extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    protected $table = 'terminal_dispatches';

    protected $fillable = [
        'dispatch_type_id','terminal_id','retail_outlet_id','loading_date',
        'driver_name','truck_no','product_qty','waybill_no','driver_no','acquila_code',
        'meter_ticket_no','est_date_of_arrival','depot_id','user_id'
    ];

    public function terminal()
    {
        return $this->belongsTo('App\Models\Terminal', 'terminal_id');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Type', 'type_id');
    }

    public function outlet()
    {
        return $this->belongsTo('App\Models\Retail_outlet', 'retail_outlet_id');
    }

    public function depot()
    {
        return $this->belongsTo('App\Models\Depot', 'depot_id');
    }

    public function dispatch_type()
    {
        return $this->belongsTo('App\Models\Dispatch_type', 'dispatch_type_id');
    }

}
