<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DaughterVesselReceive extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'daughter_vessel_receiveds';
    protected $fillable = ['mother_vessel_id','type_id','bill_of_laden_qty','loading_date'];

    protected $primaryKey = 'id';

    public function type(){
        return $this->belongsTo('App\Models\Type');
    }

    public function mother_vessel(){
        return $this->belongsTo('App\Models\MotherVessel');
    }
}
