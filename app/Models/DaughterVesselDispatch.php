<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class DaughterVesselDispatch
 * @package App\Models
 * @version February 22, 2018, 11:35 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property integer terminal_id
 * @property integer depot_id
 * @property integer product_id
 * @property integer bill_of_laden_qty
 * @property date bill_of_laden_date
 * @property date qty_dispatched
 * @property date dispatch_date
 */
class DaughterVesselDispatch extends Model
{

    public $table = 'daughter_vessel_dispatches';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'terminal_id',
        'depot_id',
        'type_id',
        'bill_of_laden_qty',
        'bill_of_laden_date',
        'qty_dispatched',
        'dispatch_date',
        'dispatch_type_id',
        'daughter_vessel_id',
        'user_id'
    ];



    public function daughter_vessel()
    {
        return $this->belongsTo('App\Models\DaughterVessel', 'daughter_vessel_id');
    }

    public function type(){
        return $this->belongsTo('App\Models\Type');
    }

    public function terminal()
    {
        return $this->belongsTo('App\Models\Terminal', 'terminal_id');
    }
    
}
