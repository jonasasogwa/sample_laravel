<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Daughter_vessel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
   protected $table = 'daughter_vessels';
   protected $fillable = ['name','number'];
   protected $primaryKey = 'id';

    public function terminal_receive(){
        return $this->hasMany('App\Models\Terminal_receive','daughter_vessel');
    }
}
