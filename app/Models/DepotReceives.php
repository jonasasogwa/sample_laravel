<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepotReceives extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $table = 'depot_receiveds';
    protected $fillable = [
        'terminal_id','depot_id','loading_date',
        'truck_no','metre_ticket_no','product_qty',
        'waybill_no','waybill_qty','driver_name',
        'driver_phone','date_of_arrival','acquila_code',
        'type_id','user_id'
    ];

    public function depot(){
        return $this->belongsTo('App\Models\Depot');
    }

//    public function terminal(){
//        return $this->belongsTo('App\Models\Terminal');
//    }
}
