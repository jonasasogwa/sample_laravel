<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Type extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $table='types';

    protected $fillable = [
        'name','description','status_id','category_id'
    ];

    public function mother(){
       return $this->hasMany('App\Models\MotherVesselReceive');
    }

    public function mother_dispatches(){
       return $this->hasMany('App\Models\MotherVesselDispatches');
    }

    public function daughter_receives(){
        return $this->hasMany('App\Models\DaughterVesselReceive');
    }

    public function terminal(){
        return $this->hasMany('App\Models\Terminal_receive');
    }
}
