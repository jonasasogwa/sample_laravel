<?php

namespace App\Models;
use App\Models\State;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    function state()
    {
        return $this->belongsTo('App\Models\State', 'city_id');
    }

    static function _find()
    {
        State::where('country_id', '160')->get();
    }
}
