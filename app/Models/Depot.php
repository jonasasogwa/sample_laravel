<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Depot extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'depots';
    protected $fillable = ['code','name','address','city_id','status_id'];

    public function depot_receive(){
        return $this->belongsToMany('App\Models\DepotReceives');
    }

}
