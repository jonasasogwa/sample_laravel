<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutletReceivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outlet_receives', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dispatch_no');
            $table->string('truck_no');
            $table->dateTime('date_time_dispatch');
            $table->string('estimated_arival_date');
            $table->integer('product_qty');
            $table->string('driver_name');
            $table->string('driver_phone');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outlet_receives');
    }
}
