<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetailOutletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retail_outlets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('dealer_name');
            $table->text('address');
            $table->integer('city_id');
            $table->integer('nearest_to_depot');
            $table->integer('distant_to_depot');
            $table->string('tank_no');
            $table->integer('capacity');
            $table->integer('pump_no');
            $table->date('date_of_purchase');
            $table->string('low_demand_season');
            $table->string('high_demand_season');
            $table->integer('status_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retail_outlets');
    }
}
