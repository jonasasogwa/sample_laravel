<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminalReceivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terminal_receives', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('daughter_vessel_id');
            $table->integer('type_id');
            $table->integer('terminal_id');
            $table->date('bill_date');
            $table->integer('qty_dispatch');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terminal_receives');
    }
}
