<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminalDispatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terminal_dispatches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dispatch_type_id');
            $table->integer('terminal_id');
            $table->integer('retail_outlet_id');
            $table->date('loading_date');
            $table->string('marketer_company');
            $table->string('driver_name');
            $table->integer('truck_no');
            $table->string('marketer_ticket_no');
            $table->integer('product_qty');
            $table->string('waybill_no');
            $table->string('driver_no');
            $table->date('est_date_of_arival');
            $table->string('acquila_code');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terminal_dispatches');
    }
}
