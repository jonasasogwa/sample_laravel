<?php

/*
n| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/marketers/login', 'AuthController@index');
Route::post('/marketers/login', 'AuthController@login');
Route::get('logs', 'LogsController@index');
Route::get('data-log/{id}','LogsController@data_log');
Route::get('/users', 'UserController@index')->name('users');
Route::get('/user_status/{id}', 'UserController@changeStatusView')->name('user_status');
Route::get('/sort_users_view', 'UserController@SortView')->name('sort_users_view');
Route::post('/sort_users_post', 'UserController@SortAll')->name('sort_users_post');
Route::post('/update_status/{id}', 'UserController@updateStatus')->name('update_status');
Route::get('/password/reset/{token?}', ['as' => 'auth.password.reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
Route::group(['prefix' => 'marketer'], function () {
    Route::get('/marketers', 'MarketersController@index')->name('marketers');
    Route::get('/add_marketer', 'MarketersController@add_marketer')->name('add_marketer');
    Route::post('/add_marketer', 'MarketersController@store_marketer')->name('store_marketer');
	Route::get('/edit_marketer/{id}', 'MarketersController@getEditableMarketerView')->name('marketer.edit_marketer');
	Route::post('/upload_excel','MarketersController@upload_excel')->name('upload_excel');
    Route::get('download-excel-file/{type}', array('as'=>'excel-file-m','uses'=>'MarketersController@downloadExcelFile'));
    Route::post('/update_marketer', 'MarketersController@UpdateMarketer')->name('marketer.update_marketer');

    Route::get('/delete/{id}', 'MarketersController@DestroyMarketer')->name('marketer.delete');
    Route::get('download-excel-file_req/{type}', array('as'=>'excel-file_req','uses'=>'MarketersController@downloadExcelFileReq'));
    Route::get('/marketers_requests', 'MarketersController@marketers_requests')->name('marketers_requests');
});

Route::group(['prefix' => 'auditors'], function () {
    Route::get('/', 'AuditorController@index');
    Route::get('/add', 'AuditorController@getAuditorView');
    Route::get('/upload', 'AuditorController@getUploadView');
    Route::post('/upload', 'AuditorController@saveUpload');
    Route::post('/create', 'AuditorController@createAuditor');
    Route::get('/edit/{id}', 'AuditorController@getEditedAuditorView');
    Route::post('/edit', 'AuditorController@updateAuditor');
    Route::get('/delete', 'AuditorController@deleteAuditor');
    Route::get('download-excel-file/{type}', array('as'=>'auditors-excel-file','uses'=>'AuditorController@downloadExcelFile'));
});

Route::get('/change-password/', 'Auth\PasswordController@getChangePasswordView');
Route::post('/change-password/','Auth\PasswordController@savePassword');

Route::group(['prefix' => 'terminals'], function () {
    Route::get('/', 'TerminalController@index');
    Route::get('/add', 'TerminalController@getTerminalView');
    Route::get('/upload', 'TerminalController@getUploadView');
    Route::post('/upload', 'TerminalController@saveUpload');
    Route::post('/create', 'TerminalController@createTerminal');
    Route::get('/edit/{id}', 'TerminalController@getEditedTerminalView');
    Route::post('/edit', 'TerminalController@updateTerminal');
    Route::get('/delete', 'TerminalController@deleteTerminal');
    Route::get('download-excel-file/{type}', array('as'=>'terminals-excel-file','uses'=>'TerminalController@downloadExcelFile'));
});

Route::group(['prefix' => 'outlet'], function () {
    Route::get('/addnew', ['uses'=>'RetailOutletController@addnew','as'=>'outlet.addnew']);
    Route::post('/create', ['uses'=>'RetailOutletController@createoutlet','as'=>'outlet.create']);
    Route::get('/manage', ['uses'=>'RetailOutletController@manage','as'=>'outlet.manage' ]);
    Route::get('/upload', ['uses'=>'RetailOutletController@upload','as'=>'outlet.upload']);
    Route::get('/edit/{id}', ['uses'=>'RetailOutletController@edit','as'=>'outlet.edit']);
    Route::get('/delete/{id}', ['uses'=>'RetailOutletController@Destroy','as'=>'outlet.delete']);
    Route::post('/update/{id}', ['uses'=>'RetailOutletController@update','as'=>'outlet.update']);
    Route::get('/view/{id}', ['uses'=>'RetailOutletController@view','as'=>'outlet.view']);
    Route::post('/upload_excel','RetailOutletController@upload_Outlet_excel')->name('upload_Outlet_excel');
    Route::get('download-excel-file/{type}', array('as'=>'outlet-excel-file','uses'=>'RetailOutletController@downloadExcelFile'));
});

Route::group(['prefix' => 'reports'], function (){
    //mother receives/dispatches
    Route::post('/post_mother_vessel_receives', 'MotherVesselReceiveController@ViewIndex')->name('post_mother_vessel_receives');
    Route::get('/mother_vessel_receives', 'MotherVesselReceiveController@index')->name('mother_vessel_receives');
    Route::get('/mother-receives-pdf-file', array('as'=>'mother-receives-pdf-file','uses'=>'MotherVesselReceiveController@downloadPdf'));
    Route::get('/download-excel-file/{type}', array('as'=>'mother-receives-excel-file','uses'=>'MotherVesselReceiveController@downloadExcelFile'));
    //dispatches
    Route::get('/mother_vessel_dispatches', 'MotherVesselReceiveController@MotherDispatchesIndex')->name('mother_vessel_dispatches');
    Route::post('/post_mother_vessel_dispatches', 'MotherVesselReceiveController@GetRangeDispatched')->name('post_mother_vessel_dispatches');
    Route::get('/mother-dispatches-pdf-file', array('as'=>'mother-dispatches-pdf-file','uses'=>'MotherVesselReceiveController@downloadDispatchPdf'));
    Route::get('/mother_dispatch-excel-file/{type}', array('as'=>'mother-dispatches-excel-file','uses'=>'MotherVesselReceiveController@downloadDispatchExcelFile'));


    //product dispatched and not received
    Route::get('/product-dispatched-not-received', 'ReportController@productDispatchedNotReceivedView');
    Route::get('/getSourceTypeNames', 'ReportController@getSourceTypeNames');
    Route::get('/getProductDispatchNotReceived', 'ReportController@getProductDispatchNotReceivedReport');
    Route::get('/outlet-with-without-product', 'ReportController@getOutletWithWithoutProductView');
    Route::get('/getOutletWithWithoutProductReport', 'ReportController@getOutletWithWithoutProduct');


    //daughter receives/dispatches
    Route::get('/daughter_vessel_receives', 'DaughterVesselReceiveController@index')->name('daughter_vessel_receives');
    Route::post('/post_daughter_vessel_receives', 'DaughterVesselReceiveController@ViewIndex')->name('post_daughter_vessel_receives');
    Route::get('/daughter_download-excel-file/{type}', array('as'=>'daughter-receives-excel-file','uses'=>'DaughterVesselReceiveController@downloadExcelFile'));
    Route::get('/daughter-receives-pdf-file', array('as'=>'daughter-receives-pdf-file','uses'=>'DaughterVesselReceiveController@downloadPdf'));
    //dispatches
    Route::get('/daughter_vessel_dispatches', 'DaughterVesselReceiveController@DaughterDispatchesIndex')->name('daughter_vessel_dispatches');
    Route::post('/post_daughter_vessel_dispatch', 'DaughterVesselReceiveController@GetRangeDispatched')->name('post_daughter_vessel_dispatch');
    Route::get('/daughter-dispatches-pdf-file', array('as'=>'daughter-dispatches-pdf-file','uses'=>'DaughterVesselReceiveController@downloadDispatchPdf'));
    Route::get('/daughter_dispatch-excel-file/{type}', array('as'=>'daughter-dispatches-excel-file','uses'=>'DaughterVesselReceiveController@downloadDispatchExcelFile'));


    //terminals receives/dispatches
    Route::get('/terminals_receives', 'TerminalReceivesController@index')->name('terminals_receives');
    Route::post('/post_terminal_receives', 'TerminalReceivesController@ViewIndex')->name('post_terminal_receives');
    Route::get('/terminal_download-excel-file/{type}', array('as'=>'terminal-receives-excel-file','uses'=>'TerminalReceivesController@downloadExcelFile'));
    Route::get('/terminal-receives-pdf-file', array('as'=>'terminal-receives-pdf-file','uses'=>'TerminalReceivesController@downloadPdf'));


    Route::get('/consolidated-report', 'ReportController@getConsolidatedReportView');
    Route::get('/getConsolidatedReport', 'ReportController@getConsolidatedReport');

    //dispatches
    Route::get('/terminals_dispatches', 'TerminalReceivesController@DispatchIndex')->name('terminals_dispatches');
    Route::post('/post_terminal_dispatch', 'TerminalReceivesController@GetRangeDispatched')->name('post_terminal_dispatch');
    Route::get('/terminals-dispatches-pdf-file', array('as'=>'terminal-dispatches-pdf-file','uses'=>'TerminalReceivesController@downloadDispatchPdf'));
    Route::get('/terminals-dispatch-excel-file/{type}', array('as'=>'terminal-dispatches-excel-file','uses'=>'TerminalReceivesController@downloadDispatchExcelFile'));


    //depot receives/dispatches
    Route::get('/depots_receives', 'DepotController@ReceiveIndex')->name('depots_receives');
    Route::post('/post_depot_receives', 'DepotController@GetRangeReceived')->name('post_depot_receives');
    Route::get('/depot_download-excel-file/{type}', array('as'=>'depot-receives-excel-file','uses'=>'DepotController@downloadExcelFile'));
    Route::get('/depot-receives-pdf-file', array('as'=>'depot-receives-pdf-file','uses'=>'DepotController@downloadPdf'));
    //dispatches
    Route::get('/depots_dispatches', 'DepotController@DispatchIndex')->name('depots_dispatches');
    Route::post('/post_depot_dispatches', 'DepotController@GetRangeDispatched')->name('post_depot_dispatches');
    Route::get('/depot_dispatches-excel-file/{type}', array('as'=>'depot-dispatches-excel-file','uses'=>'DepotController@downloadDispatchedExcelFile'));
    Route::get('/depot-dispatches-pdf-file', array('as'=>'depot-dispatches-pdf-file','uses'=>'DepotController@downloadDispatchedPdf'));


    //outlet receives/dispatches
    Route::get('/outlets_receives', 'RetailOutletReportController@ReceiveIndex')->name('outlets_receives');
    Route::post('/post_outlet_receives', 'RetailOutletReportController@GetRangeReceived')->name('post_outlet_receives');
    Route::get('/outlet_download-excel-file/{type}', array('as'=>'outlet-receives-excel-file','uses'=>'RetailOutletReportController@downloadExcelFile'));
    Route::get('/outlet-receives-pdf-file', array('as'=>'outlet-receives-pdf-file','uses'=>'RetailOutletReportController@downloadPdf'));

    //Retail-Outlet Mass Balance
    Route::get('/retail-outlet-mass-balance', 'RetailOutletReportController@OutLetMassIndex')->name('retail-outlet-mass-balance');

});

