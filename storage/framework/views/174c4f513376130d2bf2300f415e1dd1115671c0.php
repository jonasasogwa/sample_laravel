<?php $__env->startSection('content'); ?>
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-default btn-xs" href="<?php echo e(route('excel-file_req',['type'=>'xls'])); ?>">Download Excel xls</a>
                        <a class="btn btn-default btn-xs" href="<?php echo e(route('excel-file_req',['type'=>'xlsx'])); ?>">Download Excel xlsx</a>
                        <a class="btn btn-default btn-xs" href="<?php echo e(route('excel-file_req',['type'=>'csv'])); ?>">Download CSV</a>
                    </div>
                </div>

                <table class="table table-hover">
                    <thead>
                    <th>CODE</th>
                    <th>NAME</th>
                    <th>PRODUCT NAME</th>
                    <th>DESCRIPTION</th>
                    <th>SETTINGS</th>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $maketers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $maketer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td>
                                <?php echo e($maketer->code); ?>

                            </td>
                            <td>
                                <?php echo e($maketer->first_name); ?> <?php echo e($maketer->last_name); ?>

                            </td>
                            <td>
                                <?php echo e($maketer->name); ?>

                            </td>
                            <td>
                                <?php echo e($maketer->description); ?>

                            </td>
                            <td>
                                <a href="#" class="btn btn-xs btn-info "> view </a>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>

            </div>

        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('custom/js/sales.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>