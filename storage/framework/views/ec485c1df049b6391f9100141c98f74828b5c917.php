<?php $__env->startSection('content'); ?>

    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                <form class="form-inline" method="post" action="" >
                    <?php echo e(csrf_field()); ?>


                    <div class="col-md-10 row" >
                        <label for="from_date">Source Type:: </label>
                        <input type="text" class="form-control" id="source_type_id" name="source_type_id">
                        <label for="to_date">Source Name:: </label>
                        <input type="text" class="form-control" id="source_name_id" name="source_name_id">
                    </div>

                    <br/>
                    <div class="col-md-10 row" >
                        <label for="from_date">From: </label>
                        <input type="text" class="form-control" id="from_date" name="from_date">
                        <label for="to_date">To: </label>
                        <input type="text" class="form-control" id="to_date" name="to_date">
                        <button type="button" class="btn btn-primary " id="productDispatchNotReceivedBtn"> Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        $('#from_date').dcalendarpicker();
        $('#to_date').dcalendarpicker();
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>