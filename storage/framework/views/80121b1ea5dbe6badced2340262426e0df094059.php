<?php $__env->startSection('content'); ?>

        <div class="col-sm-12">

            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">

                    <form class="form-inline" method="post" action="<?php echo e(url('/')); ?>" >
                        <?php echo e(csrf_field()); ?>

                        <input type="hidden" name="getSourceTypeNames" id="getSourceTypeNames" value="<?php echo e(url('/reports/getSourceTypeNames')); ?>">
                        <input type="hidden" name="getReport" id="getReport" value="<?php echo e(url('/reports/getConsolidatedReport')); ?>">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label for="location_id" class="col-sm-2 control-label">Source Type: </label>
                                <div class="col-sm-10 ">
                                    <?php echo Form::select('source_type_id',['0'=> 'Select Source Type', 'mother_vessel'=> 'Mother Vessel', 'daughter_vessel' => 'Daughter Vessel', 'terminal'=>'Terminal', 'depot'=>'Depot'],null,['class' => 'source_type form-control m-b', "id" => "source_type"]); ?>

                                </div>
                            </div> <!-- /form-group-->
                            <div class="form-group row">
                                <label for="location_id" class="col-sm-2 control-label">Source Name: </label>
                                <div class="col-sm-10 sub_div">
                                    <?php echo Form::select('source_name_id',['0'=> 'Select Name'],null,['class' => 'source_name_id form-control m-b', "id" => "source_name_id"]); ?>

                                </div>
                            </div> <!-- /form-group-->
                        </div>

                        <div class="col-md-10 row" >
                            <label for="from_date">From: </label>
                            <input type="text" class="form-control" id="from_date" name="from_date">
                            <label for="to_date">To: </label>
                            <input type="text" class="form-control" id="to_date" name="to_date">
                            <button type="button" class="btn btn-primary " id="consolidatedReport"> Submit</button>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-lg-12" style="text-align: right;padding-right: 20px;padding-bottom: 20px">
                            <a href="#" onclick = "printDiv('printarea')" class="btn btn-primary">Print Report</a>
                        </div>
                    </div>
                    
                        
                            
                            
                            
                            
                        
                    

                    <table id="example1" class="table table-hover">


                    </table>
                </div>

                <div class="col-sm-12">

                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-md-12 reports printarea" id="printarea"></div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

    <script>
        $('#from_date').dcalendarpicker();
        $('#to_date').dcalendarpicker();
    </script>

    <script src="<?php echo e(asset('js/reports.js')); ?>"></script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>