<?php $__env->startSection('content'); ?>
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-default btn-xs" href="<?php echo e(route('excel-file-m',['type'=>'xls'])); ?>">Download Excel xls</a>
                        <a class="btn btn-default btn-xs" href="<?php echo e(route('excel-file-m',['type'=>'xlsx'])); ?>">Download Excel xlsx</a>
                        <a class="btn btn-default btn-xs" href="<?php echo e(route('excel-file-m',['type'=>'csv'])); ?>">Download CSV</a>
                    </div>
                </div>

                <table class="table table-hover">
                    <thead>
                    <th>S/N</th>
                    <th>CODE</th>
                    <th>NAME</th>
                    <th>PHONE</th>
                    <th>EMAIL</th>
                    <th>SETTINGS</th>
                    </thead>
                    <tbody>
                    <?php $sn = 1 ?>
                    <?php $__currentLoopData = $maketers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $maketer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>

                            <td>
                                <?php echo e($sn++); ?>

                            </td>

                            <td>
                                <?php ?>
                                <?php if(isset($maketer->marketers->code)): ?>
                                <?php echo e($maketer->marketers->code); ?>

                                <?php endif; ?>
                            </td>

                            <td>
                                <?php if(isset($maketer->user_profile)): ?>
                                <?php echo e($maketer->user_profile->last_name); ?> <?php echo e($maketer->user_profile->first_name); ?>

                                <?php endif; ?>
                            </td>

                            <td>
                                <?php if(isset($maketer->user_profile)): ?>
                                <?php echo e($maketer->user_profile->phone); ?>

                                    <?php endif; ?>
                            </td>

                            <td>
                                <?php echo e($maketer->email); ?>

                            </td>

                            <td>
                                
                                <a href="<?php echo e(route('marketer.edit_marketer',['id' => $maketer->id])); ?>"
                                   class="btn btn-xs btn-warning "> edit </a>

                                <a href="<?php echo e(route('marketer.delete',['id' => $maketer->id])); ?>" class="btn btn-xs btn-danger ">
                                    trash </a>
                            </td>

                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>

                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo e($maketers->links()); ?>

                    </ul>
                </div>

                
                
                
                    
                        
                            
                                
                                
                            
                        
                    
                    
                        
                    
                
                

            </div>

            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-7">
                    <div class="box box-solid">
                        <div class="box-header with-border">

                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="box-group" id="accordion">
                                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                <div class="panel box box-primary">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" >
                                                Upload Marketers Records from Excel File

                                                    <a  href="<?php echo e(asset('custom/downloadable/marketers.csv')); ?>">
                                                        <code class="btn btn-xs btn-info">Download Template</code>
                                                    </a>

                                            </a>
                                        </h4>
                                    </div>
                                    <div id="" class="panel-collapse  in">
                                        <div class="box-body">

                                            <?php echo Form::open(array('route' => 'upload_excel','method'=>'POST','files'=>'true')); ?>

                                                <br>
                                                <div class="form-group has-feedback">
                                                    <?php echo Form::file('sample_file', array('class' => '' )); ?>

                                                    <?php echo $errors->first('sample_file', '<p class="alert alert-danger">:message</p>'); ?>

                                                    <span class="fa fa-upload form-control-feedback"></span>
                                                </div>

                                                <div class="row">
                                                    <div class="col-xs-4 pull-right">
                                                        <?php echo Form::submit('Upload',['class'=>'btn btn-primary btn-block btn-flat']); ?>


                                                    </div>
                                                </div>
                                            <?php echo Form::close(); ?>


                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>


        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('custom/js/sales.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>