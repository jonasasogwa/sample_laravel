<?php $__env->startSection('content'); ?>
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <fieldset><legend>Download Range:</legend> </fieldset>
                <form class="form-inline" method="post" action="<?php echo e(route('post_depot_receives')); ?>" >
                    <?php echo e(csrf_field()); ?>

                    <label for="from_date">From: </label>
                    <input type="text" class="form-control" id="from_date" name="from_date">
                    <label for="to_date">To: </label>
                    <input type="text" class="form-control" id="to_date" name="to_date">
                    
                    
                        
                        
                        
                        
                    
                    <button type="submit" class="btn btn-primary"> Submit</button>
                </form>
                <fieldset><legend>Download Range:</legend> </fieldset>
                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-success btn-xs" href="<?php echo e(route('depot-receives-excel-file',['type'=>'xls'])); ?>">Download Excel xls</a> 
                        <a class="btn btn-success btn-xs" href="<?php echo e(route('depot-receives-excel-file',['type'=>'xlsx'])); ?>">Download Excel xlsx</a>
                        <a class="btn btn-success btn-xs" href="<?php echo e(route('depot-receives-excel-file',['type'=>'csv'])); ?>">Download CSV</a>
                        <a class="btn btn-primary btn-xs" href="<?php echo e(route('depot-receives-pdf-file')); ?>">Download PDF</a>
                    </div>
                </div>

                <table id="example1" class="table table-hover">
                    <thead>
                    <th>S/N</th>
                    <th>Terminal Code</th>
                    <th>Depot Code</th>
                    <th>Truck No.</th>
                    <th>Metre Ticket No</th>
                    <th>Product Qty</th>
                    <th>Loading Date</th>
                    <th>Arrival Date</th>
                    </thead>
                    <?php $sn=1;?>
                    <tbody>
                    <?php $__currentLoopData = $depots; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $depot): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($sn++); ?></td>
                            <td>
                                <?php echo e(App\Models\Terminal::whereRaw('id=?',[$depot['terminal_id']])->first()->code); ?>

                            </td>

                            <td>
                                <?php echo e($depot->depot->code); ?>

                            </td>

                            <td>
                                <?php echo e($depot->truck_no); ?>

                            </td>

                            <td>
                                <?php echo e($depot->metre_ticket_no); ?>

                            </td>

                            <td>
                                <?php echo e($depot->product_qty); ?>

                            </td>

                            <td>
                                <?php echo e($depot->loading_date); ?>

                            </td>

                            <td>
                                <?php echo e($depot->date_of_arrival); ?>

                            </td>

                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>

                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo e($depots->links()); ?>

                    </ul>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        $('#from_date').dcalendarpicker();
        $('#to_date').dcalendarpicker();
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>