<?php $__env->startSection('content'); ?>
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <fieldset>
                    <legend>Download Range:</legend>
                </fieldset>
                <form class="form-inline" method="post" action="<?php echo e(route('post_terminal_dispatch')); ?>">
                    <?php echo e(csrf_field()); ?>

                    <label for="from_date">From: </label>
                    <input type="text" class="form-control" id="from_date" name="from_date">
                    <label for="to_date">To: </label>
                    <input type="text" class="form-control" id="to_date" name="to_date">
                    
                    
                        
                        
                        
                        
                    
                    <button type="submit" class="btn btn-primary"> Submit</button>
                </form>
                <fieldset>
                    <legend>Download Range:</legend>
                </fieldset>
                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-success btn-xs"
                           href="<?php echo e(route('terminal-dispatches-excel-file',['type'=>'xls'])); ?>">Download Excel xls</a>
                        <a class="btn btn-success btn-xs"
                           href="<?php echo e(route('terminal-dispatches-excel-file',['type'=>'xlsx'])); ?>">Download Excel xlsx</a>
                        <a class="btn btn-success btn-xs"
                           href="<?php echo e(route('terminal-dispatches-excel-file',['type'=>'csv'])); ?>">Download CSV</a>
                        <a class="btn btn-primary btn-xs" href="<?php echo e(route('terminal-dispatches-pdf-file')); ?>">Download
                            PDF</a>
                    </div>
                </div>

                <table id="example1" class="table table-hover">
                    <thead>
                    <th>Product Type</th>
                    <th>Terminal</th>
                    <th>Quantity Dispatched</th>
                    <th>Bill Date</th>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $terminals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $terminal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>

                            <td>
                                <?php if(isset($terminal->type)): ?>
                                <?php echo e($terminal->type->name); ?>

                                <?php endif; ?>
                            </td>

                            <td>
                                <?php echo e($terminal->terminal->name); ?>

                            </td>

                            <td>
                                <?php echo e($terminal->qty_dispatch); ?>

                            </td>

                            <td>
                                <?php echo e($terminal->bill_date); ?>

                            </td>

                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        $('#from_date').dcalendarpicker();
        $('#to_date').dcalendarpicker();
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>