<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('custom/css/custom.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('custom/datepicker/dcalendar.picker.css')); ?>" rel="stylesheet" type="text/css">

</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                <?php echo e(config('app.name', 'Laravel')); ?>

            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    <?php if(auth()->guard()->guest()): ?>
                        <li><a class="nav-link" href="<?php echo e(route('login')); ?>">Login</a></li>
                        <li><a class="nav-link" href="<?php echo e(route('register')); ?>">Register</a></li>
                    <?php else: ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <?php echo e(Auth::user()->name); ?> <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST"
                                      style="display: none;">
                                    <?php echo csrf_field(); ?>
                                </form>
                            </div>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">

            <div class="col-lg-3">
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="#">Home</a>
                    </li>

                    <?php if (\Entrust::hasRole(['admin', 'outlet-admin'])) : ?>

                    <li class="list-group-item">
                        <a href="<?php echo e(url('logs')); ?>">Logs</a>
                    </li>

                    <li class="list-group">
                        <ul class="list-group-item">
                            <a href="<?php echo e(route('users')); ?>">Users</a>
                            <li class="list-group-item">
                                <a href="<?php echo e(route('users')); ?>">View Users</a>
                            </li>
                            <li class="list-group-item">
                                <a href="#">Sort Users</a>
                            </li>
                        </ul>
                    </li>
                    <?php endif; // Entrust::hasRole ?>

                    <?php if (\Entrust::hasRole(['admin', 'outlet-admin'])) : ?>
                    <li class="list-group">
                        <ul class="list-group-item">
                            <a href="<?php echo e(route('marketers')); ?>">Marketer</a>
                            <li class="list-group-item">
                                <a href="<?php echo e(route('marketers')); ?>">View Marketers</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?php echo e(route('add_marketer')); ?>">Add Marketers</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?php echo e(route('marketers_requests')); ?>">Marketers Request</a>
                            </li>
                        </ul>
                    </li>
                    <?php endif; // Entrust::hasRole ?>

                    <?php if (\Entrust::hasRole(['admin', 'outlet-admin', 'outlet-manager'])) : ?>
                    <li class="list-group-item">
                        <a href="<?php echo e(route('outlet.manage')); ?>">Outlet</a>
                        <ul class="list-group-item">
                            <li class="list-group-item">
                                <a href="<?php echo e(route('outlet.addnew')); ?>">Create Outlet</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?php echo e(route('outlet.upload')); ?>">Upload Outlets</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?php echo e(route('outlet.manage')); ?>">Manage Outlets</a>
                            </li>

                        </ul>
                    </li>
                    <?php endif; // Entrust::hasRole ?>

                    <?php if (\Entrust::hasRole(['admin', 'admin-auditor'])) : ?>
                    <li class="list-group">
                        <ul class="list-group-item">
                            <a href="#">Auditor</a>
                            <li class="list-group-item">
                                <a href="<?php echo e(url('auditors')); ?>">View Auditor</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?php echo e(url('auditors/upload')); ?>">Upload Auditors</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?php echo e(url('auditors/add')); ?>">Add Auditor</a>
                            </li>
                        </ul>
                    </li>
                    <?php endif; // Entrust::hasRole ?>

                    <?php if (\Entrust::hasRole(['admin'])) : ?>
                    <li class="list-group">
                        <ul class="list-group-item">
                            <a href="#">Terminal</a>
                            <li class="list-group-item">
                                <a href="<?php echo e(url('terminals/upload')); ?>">Upload Terminals</a>
                            </li>

                            <li class="list-group-item">
                                <a href="<?php echo e(url('terminals')); ?>">View Terminal</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?php echo e(url('terminals/add')); ?>">Add Terminal</a>
                            </li>
                        </ul>
                    </li>
                    <?php endif; // Entrust::hasRole ?>

                    <?php if (\Entrust::hasRole(['admin'])) : ?>
                    <li class="list-group">

                        <ul class="list-group-item">
                            <a href="#">Product received Report</a>
                            <li class="list-group-item">
                                <a href="<?php echo e(route('mother_vessel_receives')); ?>">Mother Vessel Receive</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?php echo e(route('daughter_vessel_receives')); ?>">Daughter Vessel Receive</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?php echo e(route('terminals_receives')); ?>">Terminal Receive</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?php echo e(route('depots_receives')); ?>">Depot Receive</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?php echo e(route('outlets_receives')); ?>">Retail Outlets Receive</a>
                            </li>
                        </ul>
                    </li>

                    <li class="list-group">

                        <ul class="list-group-item">
                            <a href="#">Product Dispatched Report</a>

                            <li class="list-group-item">
                                <a href="<?php echo e(route('mother_vessel_dispatches')); ?>">Mother Vessel Dispatched</a>
                            </li>

                            <li class="list-group-item">
                                <a href="<?php echo e(route('daughter_vessel_dispatches')); ?>">Daughter Vessel Dispatched</a>
                            </li>

                            <li class="list-group-item">
                                <a href="<?php echo e(route('terminals_dispatches')); ?>">Terminal Dispatched</a>
                            </li>

                            <li class="list-group-item">
                                <a href="<?php echo e(route('depots_dispatches')); ?>">Depot Dispatched</a>
                            </li>

                        </ul>
                    </li>

                    <li class="list-group-item">
                        <a href="<?php echo e(url('reports/product-dispatched-not-received/')); ?>">Product Dispatched & not
                            Received</a>
                    </li>
                   
                    <li class="list-group-item">
                        <a href="<?php echo e(route('retail-outlet-mass-balance')); ?>">Retail-Outlet Mass Balance</a>
                    </li>


                    <li class="list-group-item">
                        <a href="<?php echo e(url('reports/consolidated-report/')); ?>">Consolidated Report</a>
                    </li>

                    <li class="list-group-item">
                        <a href="<?php echo e(url('reports/outlet-with-without-product/')); ?>">Outlet with/without Product</a>
                    </li>
                    <?php endif; // Entrust::hasRole ?>

                    <?php if(\Illuminate\Support\Facades\Auth::user()):?>
                    <li class="list-group-item">
                        <a href="<?php echo e(url('auditors/edit/')); ?>/<?php echo e(\Illuminate\Support\Facades\Auth::user()->id); ?>">Edit
                            Info</a>
                    </li>
                    <?php endif; ?>

                    <li class="list-group-item">
                        <a href="<?php echo e(url('/change-password')); ?>">Change Password</a>
                    </li>

                </ul>
            </div>

            <div class="col-lg-9">
                <?php echo $__env->yieldContent('content'); ?>
            </div>

        </div>
    </div>

</div>

<!-- Scripts -->
<script src="<?php echo e(asset('js/app.js')); ?>"></script>
<script src="<?php echo e(asset('custom/js/jquery.js')); ?>"></script>
<script src="<?php echo e(asset('custom/datepicker/dcalendar.picker.js')); ?>"></script>

<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>

<?php echo $__env->yieldContent('js'); ?>
</body>
</html>
