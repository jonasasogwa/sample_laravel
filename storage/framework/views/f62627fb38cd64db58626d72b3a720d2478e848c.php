<?php $__env->startSection('content'); ?>
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <fieldset><legend>Download Range:</legend> </fieldset>
                <form class="form-inline" method="post" action="<?php echo e(route('post_outlet_receives')); ?>" >
                    <?php echo e(csrf_field()); ?>

                    <label for="from_date">From: </label>
                    <input type="text" class="form-control" id="from_date" name="from_date">
                    <label for="to_date">To: </label>
                    <input type="text" class="form-control" id="to_date" name="to_date">
                    
                    
                        
                        
                        
                        
                    
                    <button type="submit" class="btn btn-primary"> Submit</button>
                </form>
                <fieldset><legend>Download All:</legend> </fieldset>
                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-success btn-xs" href="<?php echo e(route('outlet-receives-excel-file',['type'=>'xls'])); ?>">Download Excel xls</a>
                        <a class="btn btn-success btn-xs" href="<?php echo e(route('outlet-receives-excel-file',['type'=>'xlsx'])); ?>">Download Excel xlsx</a>
                        <a class="btn btn-success btn-xs" href="<?php echo e(route('outlet-receives-excel-file',['type'=>'csv'])); ?>">Download CSV</a>
                        <a class="btn btn-primary btn-xs" href="<?php echo e(route('outlet-receives-pdf-file')); ?>">Download PDF</a>
                    </div>
                </div>

                <table class="table table-hover">
                    <thead>
                    <th>S/N</th>
                    <th>Dispatch No</th>
                    <th>Truck No.</th>
                    <th>Product Qty.</th>
                    <th>Driver Name</th>
                    <th>Waybill No.</th>
                    <th>Date-Time Dispatch</th>
                    <th>Estimated Arrival Date</th>
                    </thead>
                    <?php $sn=1;?>
                    <tbody>
                    <?php $__currentLoopData = $outlets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $outlet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($sn++); ?></td>
                            <td>
                                <?php echo e($outlet->dispatch_no); ?>

                            </td>

                            <td>
                                <?php echo e($outlet->truck_no); ?>

                            </td>

                            <td>
                                <?php echo e($outlet->product_qty); ?>

                            </td>

                            <td>
                                <?php echo e($outlet->driver_name); ?>

                            </td>

                            <td>
                                <?php echo e($outlet->product_qty); ?>

                            </td>

                            <td>
                                <?php echo e($outlet->waybill_no); ?>

                            </td>

                            <td>
                                <?php echo e($outlet->date_time_dispatch); ?>

                            </td>

                            <td>
                                <?php echo e($outlet->estimated_arrival_date); ?>

                            </td>

                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>

                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo e($outlets->links()); ?>

                    </ul>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        $('#from_date').dcalendarpicker();
        $('#to_date').dcalendarpicker();
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>