<?php $__env->startSection('content'); ?>
    <div class="col-sm-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">

                <div class="row pull-left">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn-default btn-xs" href="<?php echo e(route('auditors-excel-file',['type'=>'xls'])); ?>">Download Excel xls</a>
                        <a class="btn btn-default btn-xs" href="<?php echo e(route('auditors-excel-file',['type'=>'xlsx'])); ?>">Download Excel xlsx</a>
                        <a class="btn btn-default btn-xs" href="<?php echo e(route('auditors-excel-file',['type'=>'csv'])); ?>">Download CSV</a>
                    </div>
                </div>

                    <table class="table table-hover">
                        <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Verification Code</th>
                        <th>phone</th>
                        <th>Settings</th>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $auditors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $auditor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td>
                                    <?php echo e($auditor->last_name); ?>  <?php echo e($auditor->first_name); ?>

                                </td>
                                <td>
                                    <?php echo e($auditor->email); ?>

                                </td>
                                <td>
                                    <?php echo e($auditor->verification_code); ?>

                                </td>
                                <td>
                                    <?php echo e($auditor->phone); ?>

                                </td>
                                <td>
                                    <a href="<?php echo e(url('/auditors/edit/')); ?>/<?php echo e($auditor->id); ?>" class="btn btn-xs btn-warning "> edit </a>
                                    <a type="button" data-info='<?php echo e($auditor->id); ?>}' class="btn btn-xs btn-danger removeAuditor">trash</a>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>

                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <?php echo e($auditors->links()); ?>

                        </ul>
                    </div>

                </div>

            </div>

        </div>

    <script>
        $(document).on('click', '.removeAuditor', function(){
            var id = $(this).attr('data-info');
            var r =confirm('Are you sure you want to delete this  Auditor');
            //alert(id);
            if(r){
                $.get('auditors/delete', {'id':id})
                .done(function(e){
                    if(e == '200'){
                        alert('It has been deleted');
                        location.reload();
                    }
                    if(e == '403'){
                        alert('You cannot delete this Auditor because it being used by one or more process');
                    }
                })
                .fail(function(){
                    alert('error');
                });
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>