<?php $__env->startSection('content'); ?>
    <div class="col-sm-12">

        <div class="">
            <!-- /.box-header -->
            <div class="">

                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>
                </div>

                <?php if (\Entrust::hasRole(['admin'])) : ?>
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3><?php echo e($data['usersCount']); ?></h3>
                                <p>Users</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-users"></i>
                            </div>
                            <a href="<?php echo e(route('users')); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>

                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3><?php echo e($data['marketersCount']); ?>

                                    
                                </h3>

                                <p>Marketers</p>
                            </div>
                            <div class="icon">
                                <i class="fa  fa-opencart"></i>
                            </div>
                            <a href="<?php echo e(route('marketers')); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3><?php echo e($data['retailOutlet']); ?></h3>

                                <p>Retail-Outlets</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-truck"></i>
                            </div>
                            <a href="<?php echo e(route('outlet.manage')); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3><?php echo e($data['terminalCount']); ?></h3>

                                <p>Terminals</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-arrows-alt"></i>
                            </div>
                            <a href="<?php echo e(url('/terminals/')); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->



                </div>
                <?php endif; // Entrust::hasRole ?>

            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>